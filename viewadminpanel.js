/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, updateVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
updateVar = UrlVars.urlVar('update');
infoMessage = document.getElementById('p_adminpanel_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'nopwd') {
    infoMessage.innerHTML =  "Error: Please fill in the field of the password for the admin panel.";
  }
  else if (errorVar == 'wrongpwd') {
    infoMessage.innerHTML = "Error: Please note that the password for the administrator's panel was incorrect. No changes have been implemented.";
  }
  else if (errorVar == 'notadmin') {
    infoMessage.innerHTML = "Warning: Please note that only the administrator can use the admin panel.";
  }
  else if (errorVar == 'nooptionselected') {
    infoMessage.innerHTML = "You must select one of the three options in order to change the mode for sign up.";
  }
  else if (errorVar == 'invalidoption') {
    infoMessage.innerHTML = "Error: Sent data were not what was expected, in order for the mode of sign up to be changed.";
  }
  else if (errorVar == 'nosecretcode') {
    infoMessage.innerHTML = "Please note that you must fill in the field of the secret code in order to change it.";
  }
  else if (errorVar == 'secretcodelength') {
    infoMessage.innerHTML =  "Length of secret code must be between 8 and 18 characters.";
  }
}

  if (updateVar != "") {
    if (updateVar == 'changemodesuccessful') {
    infoMessage.innerHTML = "You have successfully changed the mode for signing up.";
    }
    else if (updateVar == 'secretcodesuccessful') {
    infoMessage.innerHTML = "You have successfully changed the secret code that a user must enter before signing up (if it is requested).";
    }
  }
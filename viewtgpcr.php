<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
  switch ($category) {
  	case 't':
?>
      <div id="t<?php echo $tgpcr['id']; ?>" class="class_topic">
    		<div class="id_text" onclick="selectTgpcr(event)"><?php echo 't', $tgpcr['id']; ?></div>
    		<div class="tgpcr_text"><?php echo $tgpcr['topic']; ?></div>
    		<div class="votes_text"><?php echo $tgpcr['votes_sum_result']; ?></div>
    		<div class="votes_text"><?php echo $tgpcr['number_of_votes']; ?></div>
    		<div class="user_date_text"><?php echo $tgpcr['user_name']; ?></div>
    		<div class="user_date_text"><?php echo $tgpcr['date_time']; ?></div>
        <div class="user_date_text" style="display: none;"><?php echo 'n0', '_n0', '_n0' ?></div>
    	</div>
<?php
  		break;

  	case 'g':
?>
  		<div id="g<?php echo $tgpcr['id']; ?>" class="class_group">
        <div class="id_text" onclick="selectTgpcr(event)"><?php echo 'g', $tgpcr['id']; ?>˺</div>
        <div class="tgpcr_text"><?php echo $tgpcr['group_name']; ?>˺</div>
        <div class="votes_text"><?php echo $tgpcr['votes_sum_result']; ?>˺</div>
        <div class="votes_text"><?php echo $tgpcr['number_of_votes']; ?>˺</div>
        <div class="user_date_text"><?php echo $tgpcr['user_name']; ?>˺</div>
        <div class="user_date_text"><?php echo $tgpcr['date_time']; ?>˺</div>
        <div class="user_date_text" style="display: none;"><?php echo 't', $tgpcr['topic_id'], '_n0', '_n0' ?></div>
      </div>
<?php
  		break;

  	case 'p':
      if ($tgpcr['edit_state'] >= 1) {
          $stateOfEdit = 'Editing phase';
        } else {
          $stateOfEdit = 'Voting phase';
        }
?>
      <div id="p<?php echo $tgpcr['id']; ?>" class="class_proposal">
        <div class="id_text" onclick="selectTgpcr(event)"><?php echo 'p', $tgpcr['id']; ?>˺</div>
        <div class="tgpcr_text" onclick="selectProposals(event)"><?php echo $tgpcr['proposal']; ?>˺</div>
        <div class="votes_text"><?php echo $tgpcr['votes_sum_result']; ?>˺</div>
        <div class="votes_text"><?php echo $tgpcr['number_of_votes']; ?>˺</div>
        <div class="user_date_text"><?php echo $tgpcr['user_name']; ?>˺</div>
        <div class="user_date_text"><?php echo $tgpcr['date_time']; ?>˺</div>
        <div class="user_date_text"><?php echo $stateOfEdit; ?>˺</div>
        <div class="user_date_text" style="display: none;"><?php echo 't', $tgpcr['topic_id'], '_n0', '_n0' ?></div>
      </div>
<?php
  		break;

  	case 'c':
      switch ($tgpcr['category_id']) {
        case '1':
          $categoryOfParent = 'q';
          break;

        case '2':
          $categoryOfParent = 'g';
          break;

        case '3':
          $categoryOfParent = 'p';
          break;

        case '4':
          $categoryOfParent = 'c';
          break;

        case '5':
          $categoryOfParent = 'r';
          break;

        default:
          $categoryOfParent = 'n';
          break;
      }
?>
      <div id="c<?php echo $tgpcr['id']; ?>" class="class_comment">
        <div class="id_text" onclick="selectTgpcr(event)"><?php echo 'c', $tgpcr['id']; ?>˺</div>
        <div class="tgpcr_text"><?php echo $tgpcr['comment_text']; ?>˺</div>
        <div class="votes_text">˺</div>
        <div class="votes_text">˺</div>
        <div class="user_date_text"><?php echo $tgpcr['user_name']; ?>˺</div>
        <div class="user_date_text"><?php echo $tgpcr['date_time']; ?>˺</div>
        <div class="user_date_text" style="display: none;"><?php echo $categoryOfParent, $tgpcr['in_category_id'], '_n0', '_n0' ?></div><!-- TO DO: It should be parentCategId_tId_n0 -->
      </div>
<?php
  		break;

  	case 'r':
      $categoryOfParent = 'n';
      $idOfParent = 0;
?>
      <div id="r<?php echo $tgpcr['id']; ?>" class="class_reference">
        <div class="id_text" onclick="selectTgpcr(event)"><?php echo 'r', $tgpcr['id']; ?>˺</div>
        <div class="tgpcr_text" title="<?php echo $tgpcr['reference_text']; ?>">
          <a href="<?php echo $tgpcr['reference_text']; ?>" target="_blank"><?php echo $tgpcr['ref_description']; ?></a>˺
        </div>
        <div class="votes_text"><?php echo $tgpcr['votes_sum_result']; ?>˺</div>
        <div class="votes_text"><?php echo $tgpcr['number_of_votes']; ?>˺</div>
        <div class="user_date_text"><?php echo $tgpcr['user_name']; ?>˺</div>
        <div class="user_date_text"><?php echo $tgpcr['date_time']; ?>˺</div>
        <div class="user_date_text" style="display: none;"><?php echo $categoryOfParent, $idOfParent, '_n0', '_n0' ?></div>
      </div>
<?php
  		break;

    case 'q':    //requirement.
        if ($tgpcr['approval_status'] >= 1) {
          $stateOfApproval = 'Approved';
        } elseif ($tgpcr['approval_status'] == null) {
           $stateOfApproval = 'Pending';
        } else {
          $stateOfApproval = 'Rejected';
        }
?>
      <div id="q<?php echo $tgpcr['id']; ?>" class="class_requirement">
        <div class="id_text" onclick="selectTgpcr(event)"><?php echo 'q', $tgpcr['id']; ?></div>
        <div class="tgpcr_text"><?php echo $tgpcr['requirement_text']; ?></div>
        <div class="votes_text"><?php echo $tgpcr['votes_sum_result']; ?></div>
        <div class="votes_text"><?php echo $tgpcr['number_of_votes']; ?></div>
        <div class="user_date_text"><?php echo $tgpcr['user_name']; ?></div>
        <div class="user_date_text"><?php echo $tgpcr['date_time']; ?></div>
        <div class="user_date_text"><?php echo $stateOfApproval; ?></div>
        <div class="user_date_text" style="display: none;"><?php echo 't', $tgpcr['topic_id'], '_n0', '_n0' ?></div>
      </div>
<?php
      break;

    case 's':
?>
      <div id="s<?php echo $tgpcr['id']; ?>" class="class_similarity">
        <div class="id_text" onclick="selectTgpcr(event)"><?php echo 's', $tgpcr['id']; ?></div>
        <div class="tgpcr_text">
          Similar proposals: <b><?php echo 'p', $tgpcr['proposal_a_id']; ?></b> & <?php echo 'p', $tgpcr['proposal_b_id']; ?>
        </div>
        <div class="votes_text"><?php echo $tgpcr['votes_sum_result']; ?></div>
        <div class="votes_text"><?php echo $tgpcr['number_of_votes']; ?></div>
        <div class="user_date_text"><?php echo $tgpcr['user_name']; ?></div>
        <!-- <div class="user_date_text"><?php echo $tgpcr['date_time']; ?></div> -->
        <div class="user_date_text" style="display: none;"><?php echo 't', $tgpcr['topic_id'], '_n0', '_n0' ?></div>
      </div>
<?php
      break;

    case 'u':
?>
      <div id="u<?php echo $tgpcr['id']; ?>" class="class_user">
        <div class="id_text" style="display: none;"><?php echo 'u', $tgpcr['member_id']; ?></div>
        <div class="username_text" onclick="selectTeamMembers(event)"><?php echo $tgpcr['user_name']; ?></div>
        <?php
        if (isset($tgpcr['member_votes_sum_result'])) {
          ?>
            <div class="votes_text"><?php echo $tgpcr['member_votes_sum_result']; ?></div>
          <?php
        }
        ?>
        <div class="user_date_text" style="display: none;"><?php echo 't', $topicId, '_n0', '_n0' ?></div>
      </div>
<?php
      break;

    case 'm':
?>
      <div id="m<?php echo $tgpcr['id']; ?>" class="class_message">
        <!-- <div class="id_text" onclick="selectTgpcr(event)"><?php echo 'm', $tgpcr['id']; ?></div> -->
        <div class="tgpcr_text" onclick="viewRecipients(event)"><?php echo $tgpcr['message']; ?></div>
        <div class="user_date_text"><?php echo $tgpcr['sender_name']; ?></div>
        <div class="user_date_text"><?php echo $tgpcr['date_time']; ?></div>
        <div class="user_date_text" style="display: none;"><?php echo 'n0', '_n0', '_n0' ?></div>
      </div>
<?php
      break;
  	
  	default:
  		# code...
  		break;
  }
?>
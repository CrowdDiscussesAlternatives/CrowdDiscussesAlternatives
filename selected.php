<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 $cdaContrObj = new CdaContr();
 $cdaContrObj->checkSessionExpCookie();  //Before any html.
 $cdaContrObj->setSessionExpCookie();  //Set again coocie after check.
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
		   $cdaViewObj = new CdaView();
		   $tgpcrObj = new Tgpcr();

       $selectedCateg = substr($_GET['selected'], 0, 1);
       $selectedTgpcrID = intval(substr($_GET['selected'], 1));
       $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
       $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

       $parentsOfSelected = explode('_', $_GET['parent'], 3);
       for ($i=0; $i < 3; $i++) { 
       	 $parentCateg[$i] = substr($parentsOfSelected[$i], 0, 1);
       	 $parentId[$i] =  intval(substr($parentsOfSelected[$i], 1));
       }

       require_once "selectednavbar.php";
  ?>
      <p>Selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
	<?php
	    require_once "headeroftable.php";

			$category = $selectedCateg;
			require "viewtgpcr.php";

			if ($selectedCategMsg != 'Non-existent category!') {
				switch ($selectedCateg) {
					case 'g':
					  $cdaContrObj = new CdaContr();
						$checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
						if ($checkIfMember != NULL) {
					  ?>
						  <div class="link_in_selected">
		            <p><a href="createnewcomment.php?selected=<?php echo $_GET['selected']; ?>">Comment on selected <?php echo $selectedCategMsg; ?>.</a></p>
		          </div>
		          <p><a href="vote.php?selected=<?php echo $_GET['selected']; ?>">Vote selected <?php echo $selectedCategMsg; ?>.</a></p>
		        <?php
		        }
		        unset($cdaContrObj);
						break;

					case 'p':
					  $cdaContrObj = new CdaContr();
						$checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
						if ($checkIfMember != NULL) {
						  ?>
		            <div class="link_in_selected">
		            	<p><a href="createnewcomment.php?selected=<?php echo $_GET['selected']; ?>">Comment on selected <?php echo $selectedCategMsg; ?>.</a></p>
		            </div>
						  <?php
							if ($tgpcr['edit_state'] < 1) {
								?>
				          <p><a href="vote.php?selected=<?php echo $_GET['selected']; ?>">Vote selected <?php echo $selectedCategMsg; ?>.</a></p>
								<?php
							} elseif ($tgpcr['edit_state'] >= 1) {
								if ($tgpcr['user_id'] == $_SESSION["userId"]) {
								?>
								  <p><a href="editproposal.php?selected=<?php echo $_GET['selected']; ?>">Edit selected <?php echo $selectedCategMsg; ?>.</a></p>
							  <?php
                }
							  ?>
				          <p>Note: Team-members will be able to vote for this proposal when it will be on the voting phase (after <?php echo $tgpcr['edit_closing_date'] ?>).</p>
								<?php
							}
						}
					  unset($cdaContrObj);
						break;

					case 'q':
						$cdaContrObj = new CdaContr();
						$checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
						if ($checkIfMember != NULL) {
						  ?>
		            <div class="link_in_selected">
		            	<p><a href="createnewcomment.php?selected=<?php echo $_GET['selected']; ?>">Comment on selected <?php echo $selectedCategMsg; ?>.</a></p>
		            </div>
						  <?php
							if ($tgpcr['approval_status'] === null) {
								?>
		              <p><a href="vote.php?selected=<?php echo $_GET['selected']; ?>">Vote selected <?php echo $selectedCategMsg; ?>.</a></p>
						    <?php
							}
					  }
					  unset($cdaContrObj);
						break;

					case 'c':
						# code...
						break;

					case 'r':
					  ?>
						  <div class="link_in_selected">
		            <p><a href="createnewcomment.php?selected=<?php echo $_GET['selected']; ?>">Comment on selected <?php echo $selectedCategMsg; ?>.</a></p>
		          </div>
		          <p><a href="vote.php?selected=<?php echo $_GET['selected']; ?>">Vote selected <?php echo $selectedCategMsg; ?>.</a></p>
		        <?php
						break;

					case 's':
					  $similarity = $tgpcr;
					  $proposalA = $cdaViewObj->showSelectedTgpcr('p', (int)$tgpcr['proposal_a_id']);
					  $proposalB = $cdaViewObj->showSelectedTgpcr('p', (int)$tgpcr['proposal_b_id']);
					  $tgpcr = $proposalA;
					  $category = 'p';
			      require "viewtgpcr.php";
			      $tgpcr = $proposalB;
       	    //$category = 'p';
			      require "viewtgpcr.php";
            $tgpcr = $similarity;
					  $cdaContrObj = new CdaContr();
						$checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
						if ($checkIfMember != NULL) {
					  ?>
		          <p><a href="vote.php?selected=<?php echo $_GET['selected']; ?>">Vote selected <?php echo $selectedCategMsg; ?>.</a></p>
		        <?php
		        }
		        unset($cdaContrObj);
						break;
					
					default:
						?>
              <p><a href="vote.php?selected=<?php echo $_GET['selected']; ?>">Vote selected <?php echo $selectedCategMsg; ?>.</a></p>
				    <?php
						break;
			  }
			}

				for ($i=0; $i < 3; $i++) { 
					if ($parentCateg[$i]!='n') {
						$tgpcr = $cdaViewObj->showSelectedTgpcr($parentCateg[$i], (int)$parentId[$i]);
	          $parentCategMsg = $tgpcrObj->categMsg($parentCateg[$i]);
	          $category = $parentCateg[$i];
	  ?>
	          <p>of <?php echo $parentCategMsg; ?>:</p>
	  <?php
						require "viewtgpcr.php";
					}
				}
	?>
      </div>
      <br>

      <script type="text/javascript" src="./viewtgpcr.js"></script>

	<?php
	    unset($cdaViewObj);
	    unset($tgpcrObj);
    } else {
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
     Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>
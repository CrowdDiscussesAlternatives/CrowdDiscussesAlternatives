/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let VoteUrlVars, errorVar, voteVar, voteMessage, similarityIdVar;
VoteUrlVars = new GetUrlVars(window.location.href);
errorVar = VoteUrlVars.urlVar('error');
voteVar = VoteUrlVars.urlVar('vote');
similarityIdVar = VoteUrlVars.urlVar('revS');
voteMessage = document.getElementById('p_vote_urlvar');
voteMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'novote') {
    voteMessage.innerHTML = "You must select one of the three options (+1 or 0 or -1) in order to vote!";
  }
  else if (errorVar == 'invalidvote') {
    voteMessage.innerHTML = "Error: Vote was not +1 or 0 or -1.";
  }
  else if (errorVar == 'alreadyvoted') {
    voteMessage.innerHTML =  "You have already voted.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'noballotbox') {
    voteMessage.innerHTML =  "Error: BB_ID not found.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'tofproposalmembernotmatch') {    ///
    voteMessage.innerHTML =  "You are not a member of the topic that this proposal belongs, to be eligible to vote.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'tofrequirementmembernotmatch') {    ///
    voteMessage.innerHTML =  "You are not a member of the topic that this requirement belongs, to be eligible to vote.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'tofgroupmembernotmatch') {    ///
    voteMessage.innerHTML =  "You are not a member of the topic that this group belongs, to be eligible to vote.";
    document.getElementById('form-vote').style.display = 'none';
  }
   else if (errorVar == 'tofrefmembernotmatch') {    ///
    voteMessage.innerHTML =  "You are not a member of the topic that this reference belongs, to be eligible to vote.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'editingphase') {
    voteMessage.innerHTML =  "You cannot vote for a proposal that is in editing phase.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'requirementnotpending') {
    voteMessage.innerHTML =  "You cannot vote for a requirement that is not in a pending phase.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'reqphasenotyet') {
    voteMessage.innerHTML =  "You cannot vote for a requirement <b>yet<b> (the topic is still in the \"team members invitation\" phase)." + 
    " You can view the time table for more info.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'reqphasepassed') {
    voteMessage.innerHTML =  "You cannot vote for a requirement anymore (the requirements phase has been closed)." + 
    " You can view the time table for more info.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'groupsphasepassed') {
    voteMessage.innerHTML =  "You cannot vote anymore (the \"groups\" phase (fourth phase) has been closed)." + 
    " You can view the time table for more info.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'groupsphasenotyet') {
    voteMessage.innerHTML =  "You cannot vote for a group <b>yet<b> (the topic is still in the \"proposals\" (third) phase)." + 
    " You can view the time table for more info.";
    document.getElementById('form-vote').style.display = 'none';
  }
  else if (errorVar == 'UVotedForSOfRevPs') {
    voteMessage.innerHTML =  "You have already voted for a similarity of that includes these proposals in reversed order. See similarity: " + 
    "<a href=\"selected.php?selected=s" + similarityIdVar + "&parent=n0_n0_n0\" target=\"_blank\">s"
     + similarityIdVar + "</a>.";
    document.getElementById('form-vote').style.display = 'none';
  }
} 
else if (voteVar != "") {
  if (voteVar == 'success') {
    voteMessage.innerHTML =  "You have successfully voted.";
    document.getElementById('form-vote').style.display = 'none';
  }
}
<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="codeofconduct">Code of conduct</h2>
<p>
Please respect the following rules when using this website:
<ul>
	<li>No incitement of violence or promotion of violent ideologies.
	<li>Content or interactions which harass in any way are not allowed.
	<li>No racism, homophobia, xenophobia, casteism, or violent nationalism. No Holocaust denial or Nazi symbolism.
	<li>This platform is not for pornography.
	<li>Do not share intentionally false or misleading information.
  <li>Respect the licences of the creators when you share content.
</ul>
</p>
<br>
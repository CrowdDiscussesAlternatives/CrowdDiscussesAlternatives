<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2>Contents</h2>
<ul>
	<li><a href="#introduction">Introduction</a></li>
	<li><a href="#purpose">Purpose</a></li>
	<li><a href="#coreconcepts">Core concepts</a></li>
	<li><a href="#faq">Frequently asked questions</a></li>
    <li><a href="#sourcecode">Source code</a></li>
    <li><a href="#codeofconduct">Code of conduct</a></li>
    <li><a href="#moderationinformation">Moderation information</a></li>
    <li><a href="#usefulguidelines">Useful guidelines</a></li>
    <li><a href="#helpfullinks">Helpful links</a></li>
</ul>
<br>
<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="usefulguidelines">Useful guidelines</h2>
<p>
Please keep in mind the guidelines bellow, when you participate in discussions:
<br>
<ul>
    <li>Always be polite.
    <li>Do not characterize other people. Just write down the pros and cons of their suggestions (from your point of view). 
    <li>If the conversation gets hot, try to be calmer when you respond, than the rest of the participants, in order to reduce the heat.
    <li>Try to be on point of what you want to say. Stay focused on the subject of the discussion.
</ul>
</p>
<br>
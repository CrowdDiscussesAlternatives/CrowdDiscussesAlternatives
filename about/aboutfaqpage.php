<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="faq">Frequently asked questions</h2>
<p>
  <i>1. What is the difference between Crowd Discusses Alternatives application and other voting systems?</i>
</p>
<p>
	Crowd Discusses Alternatives does not try to distinguish the best solution of a problem (that is represented in a topic) from all the other solutions. On the contrary, it helps the users to find a collection of alternative solutions, and it clearly represents their advantages/disadvantages and their popularity.
</p>
<p>
	The users do not just vote for or against a solution. They participate on the construction of the solutions. They insert proposals based on the sources/information that they have gathered. These proposals are the “bricks” for the collective construction of each solution.
</p>
<p>
	Each solution might resemble of a bulleted list of the most fundamental concepts, or the most important decisions that will solve the problem. This might be necessary in order <!--thousands of-->many people to <!--consent-->be able to cooperate. If more details are needed, they can be placed as a question in another topic. <!--The detailed solution might have the structure of a tree.-->
</p>
<br>
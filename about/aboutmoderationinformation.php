<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="moderationinformation">Moderation information</h2>
<p>
If you become aware of some content in this website (text or link) that violates the code of conduct, you can send a notification via a private message to the admin of Crowd Discusses Alternatives<!-- or via email at ???@a2hosting.com-->. In the future, I am tending to add some features in this website in order the whole community of its users, to be able to help moderating its content, by reporting inappropriate content, and maybe by voting if it should become temporarily hidden from the search results due to violations of the code of conduct. The aim is, this community to be involved in the moderation of the content in order sounder decisions to be taken for this matter via creating an open and safe environment for discussions<!-- that encourage people to read and write their thoughts-->. The administrator can still temporarily hide or permanently delete content that violates the above code of conduct.
<br>
<br>
Please also note that this website is intended for public content only. There are no privacy guarantees (not even in private messages).
</p>
<br>
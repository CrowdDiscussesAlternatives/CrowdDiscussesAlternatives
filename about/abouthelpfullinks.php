<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="helpfullinks">Helpful links</h2>
<p>
You can read about how you can use this web application in the following pdf (there are plenty of snapshots that demonstrate its user interface):
<br>
<a href="https://git.disroot.org/CrowdDiscussesAlternatives/CrowdDiscussesAlternatives/src/branch/master/pdfs/Crowd%20Discusses%20Alternatives%20V2_1_2.pdf" target="_blank">Crowd Discusses Alternatives V2_1_2.pdf.</a>
</p>
<p>
Alternatively, you can watch the following video:
<br>
<a href="https://diode.zone/w/q7cApNwUjUZa6iS4eRygeY" target="_blank">"Crowd Discusses Alternatives" open-source web-app . Explanation of its basic features.</a>
</p>
<p>
An enhanced voting system for groups was implemented in 2024. For more information you can read:
<br>
<a href="https://git.disroot.org/CrowdDiscussesAlternatives/CrowdDiscussesAlternatives/src/branch/master/pdfs/Proposal%20of%20an%20adequately%20secure%20low-stakes%20voting%20system.pdf" target="_blank">Proposal of an adequately secure low-stakes voting system.pdf.</a>
</p>
<br>
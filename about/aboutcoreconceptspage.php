<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="coreconcepts">Core concepts</h2>
<p>
Crowd Discusses Alternatives is an open-source online application that helps people discuss with each other, construct alternative solutions and distinguish the most popular. The core concepts (goals) of this application are the following:
</p>
<ul>
	<li>The application helps the team-members that are involved in a topic to well-define the subject of the discussion. The goals of the subject are clear and time-framed.</li>
	<li>The proposals are clearly distinguished from the comments.</li>
	<li>Team-members are able to group proposals in order to form alternative solutions.</li>
	<li>Team-members are able to insert references and evaluate them concerning their accuracy and importance.</li>
	<li>The application has powerful search tools that give users the ability to find specific comments, proposals or groups.</li>
	<li>The application has the tools that help team-members evaluate proposals and groups of proposals.</li>
	<li>Proposals and groups of proposals can be ranked by popularity.</li>
</ul>
<br>
<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="purpose">Purpose</h2>
<p>
Discussions in forums usually tend to have the following drawbacks:
</p>
<ul>
	<li>The discussions often have no conclusion (occasionally tend to become off topic).</li>
	<li>The few proposed solutions are buried inside the comments and cannot be easily found.</li>
	<li>Each proposed solution is usually written in detail by only one person and the users of the forum have the only option to agree or disagree with it (i.e. typically forums do not have the tools to help the users cooperate in order to improve the solutions).</li>
</ul>
<p>
Crowd Discusses Alternatives aims to solve these problems by focusing on the concepts below:
</p>
<ul>
	<li>Each proposed solution is decomposed in a set of “proposals” (<!--i.e. sub-proposals, -->each one of the width of roughly a sentence). The composition of another alternative solution can now be formed by grouping another set of proposals (from this pool of the proposals). This can help people who disagree, to find alternative solutions, evaluate them and finally consent.</li>
	<li>Each new proposal is easily distinguished from its comments (i.e. from the discussion that follows the proposal).</li>
	<li>The users can evaluate the quality of the references/sources that follow each proposal (i.e. validate the available information that the alternative solutions are based on).</li>
</ul>
<!--<p>
In other words, the main goals are the users to be able to collect and evaluate all the available information and then produce and evaluate all the available proposals which will be based on this information. There are tools that help users compose alternative solutions for a problem/topic by using these proposals.
</p>-->
<br>
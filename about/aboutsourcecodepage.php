<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<h2 id="sourcecode">Source code</h2>
<p>
You can find the source code of "Crowd Discusses Alternatives" in the following repository:
<br>
<a href="https://git.disroot.org/CrowdDiscussesAlternatives/CrowdDiscussesAlternatives" target="_blank">Repository of source code.</a>
</p>
<p>
"Crowd Discusses Alternatives" is a free software under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
<br>
<a href="https://www.gnu.org/licenses/" target="_blank">GNU licenses.</a>
</p>
<br>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
      if (!isset($_REQUEST['selected'])) {
        exit("<br><br><b>-- Error: Topic ID is not found!<b>");
      }

      $selectedCateg = 'n';
      require_once "selectednavbar.php";

      $cdaViewObj = new CdaView();
      $tgpcrObj = new Tgpcr();

      $selectedCateg = substr($_REQUEST['selected'], 0, 1);  //Must be topic.
      $selectedTgpcrID = intval(substr($_REQUEST['selected'], 1)); // ID of the selected topic.

      if ($selectedCateg == '') {
        exit('-- Error: Category sent to server does not exist!');
      } elseif ($selectedCateg != 't') {
        exit('-- Error: Category of topic was expected to be sent to server (for displaying its time-table)!');
      }

      $tgpcr = $cdaViewObj->showSelectedTopicWithTimeTableInfo((int) $selectedTgpcrID);

      $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

       $topicInfo = array('inv_state' => $tgpcr['inv_state'], 'req_state' => $tgpcr['req_state'], 'prop_state' => $tgpcr['prop_state'], 'groups_state' => $tgpcr['groups_state'], 'invitation_phase_closing_date' => $tgpcr['invitation_phase_closing_date'], 'requirements_phase_closing_date' => $tgpcr['requirements_phase_closing_date'], 'proposals_phase_closing_date' => $tgpcr['proposals_phase_closing_date'], 'groups_phase_closing_date' => $tgpcr['groups_phase_closing_date'], 'timetable_changed' => $tgpcr['timetable_changed']);

      if (!isset($tgpcr['invitation_phase_closing_date'], $tgpcr['requirements_phase_closing_date'], $tgpcr['proposals_phase_closing_date'], $tgpcr['groups_phase_closing_date'], $tgpcr['timetable_changed']) || empty($tgpcr['invitation_phase_closing_date'])) {
        exit('-- Note: The time-table of this topic has not been created yet.');
      } elseif ($topicInfo["timetable_changed"] == -1) {
        exit('-- Note: The time-table of this topic has not been created yet.');
      }
  ?>

      <p>Selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
  <?php
      require_once "headeroftable.php";

      $category = $selectedCateg;
      require "viewtgpcr.php";
  ?>
      
      <br><br>
      <div class="div_as_label">Time-table of selected topic</div>
        <div class="div_view_timetable">
          <div class="div_inputlabel_timetable">
            <p>Closing date of <b>team members invitation and references not in proposals yet</b> phase:</p>
            <div class="div_show_closing_date">
              <?php 
                $date = new DateTime($topicInfo["invitation_phase_closing_date"]);
                echo $date->format('Y-m-d');
              ?>
            </div>
          </div>
          <div class="div_inputlabel_timetable">
            <p>Closing date of <b>requirements and references not in proposals yet</b> phase:</p>
            <div class="div_show_closing_date">
              <?php 
                $date = new DateTime($topicInfo["requirements_phase_closing_date"]);
                echo $date->format('Y-m-d');
              ?>
            </div>
          </div>
          <div class="div_inputlabel_timetable">
            <p>Closing date of <b>proposals and references of proposals</b> phase:</p>
            <div class="div_show_closing_date">
              <?php 
                $date = new DateTime($topicInfo["proposals_phase_closing_date"]);
                echo $date->format('Y-m-d');
              ?>
            </div>
          </div>
          <div class="div_inputlabel_timetable">
            <p>Closing date of <b>proposals, references and groups of proposals</b> phase:</p>
            <div class="div_show_closing_date">
              <?php 
                $date = new DateTime($topicInfo["groups_phase_closing_date"]);
                echo $date->format('Y-m-d');
              ?>
            </div>
          </div>
      </div>

      <p>
        Dates are in (in year-month-day). Time-table has been updated <?php echo $topicInfo["timetable_changed"] ?> times.
      </p>
      
      <?php
      require "timetableinfotext.php";
      ?>

      <script type="text/javascript" src="./viewtgpcr.js"></script>

	<?php
      unset($cdaViewObj);
      unset($tgpcrObj);
    } else {
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
      Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

	<?php

    $cdaViewObj = new CdaView();
    $cdaContrObj = new CdaContr();

    $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

    if (!isset($_REQUEST['selected'])) {
      exit("<br><br><b>-- Error: ID is not found!<b>");
    }

    $groupId = intval(substr($_REQUEST['selected'], 1));

    if ($groupId == 0 || $groupId == null) {
      exit("<br><br>-- Error: Topic ID is not found!");
    }

    $selectedgroup = $cdaViewObj->showSelectedTgpcr('g', $groupId);
    $topicId = $selectedgroup['topic_id'];

    $selectedCateg = 'g';
     require_once "selectednavbar.php";

    $orderBy = $_SESSION['orderby'];
    $ascDesc = $_SESSION['ascdesc'];
  ?>

    <p class="class_tip">Selected group:</p>

		<div class="tgpcrAll">
		<?php
      require "headeroftable.php";

      $category = 'g';
      $tgpcr = $selectedgroup;
      require "viewtgpcr.php";

      $proposals = $cdaViewObj->showProposalsOfGroup((int)$selectedgroup['id']);

      if (isset($proposals) || $proposals!=[]) {
        foreach ($proposals as $proposalsKey => $proposalValue) {
          $category = 'p';
          $tgpcr = $proposalValue;
          require "viewtgpcr.php";
        }
      }

      $totalnumOfQ = $cdaViewObj->totalNumberOfApprovedRequirements((int)$topicId);
      $requirements = $cdaViewObj->showApprovedRequirements((int)$topicId, 0, (int)$totalnumOfQ, $orderBy, $ascDesc);
    
      if ($requirements == []) {
      ?>
        <p>There are not approved requirements in the topic yet!</p>
      <?php
        exit();
      }

      ?>
        <br><br>
        <label>Evaluate group</label>

        <p class="class_tip">Total number of approved requirements in topic: <?php echo $totalnumOfQ; ?></p>

        <form action="" method="post" name="form_evaluate" id="form_evaluate">
      <?php

      foreach ($requirements as $QKey => $RequirementValue) {
        $category = 'q';
        $tgpcr = $RequirementValue;
        require "viewtgpcr.php";
        ?>
        <input type="number" name="weightfactor_<?php echo $QKey; ?>" id="weightfactor_<?php echo $QKey; ?>" class="input_weightfactor" min="0" max="1" step="0.1" placeholder="Weight factor..." title="Weight factor concerning above requirement." required></input>
        <div class="div_score">X</div>
        <input type="number" name="score_<?php echo $QKey; ?>" id="score_<?php echo $QKey; ?>" class="input_score" min="0" max="10" placeholder="Score..." title="Score of group concerning above requirement." required></input>
      <?php
        }
      ?>
        
        </form>
        <br><br>
        <button type="button" onclick="calculateGroupEvaluation()">Calculate total score</button>
        <div class="div_totalscore">Total score of Group: </div><div id="div_totalscoredisplay" class="div_totalscore"></div>
    </div>
    <br>

    <script type="text/javascript" src="./viewtgpcr.js"></script>
    <script type="text/javascript" src="./evaluategroup.js"></script>
</main>

<?php
  unset($cdaViewObj);  
  unset($cdaContrObj);  
  require "footer.php";
?>
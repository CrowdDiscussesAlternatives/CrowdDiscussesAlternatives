<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<?php
  require_once 'includes/autoloader-inc.php';
  //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
  require "header.php";

  $adminPanelContrObj = new AdminPanelContr();

  $modeForSigningUp = $adminPanelContrObj->checkModeForSigningUp();

  if (empty($modeForSigningUp) || $modeForSigningUp == null) {
      exit("<p><br><br><b>-- Error: mode for signing up is not set.</b></p>");
  }

  if ($modeForSigningUp == "disable_sign_up") {
      exit("<p><br><br><b>-- Please note that currently, the ability of signing up is active only for people that I know and friends of mine. You can still login as a guest, if you want to see the content of this platform.</b></p>");    //Signup is temporarily unavailable.
  }

  /*exit("<p><br><br><b>-- Please note </b>that currently, the ability of signing up is active only for people that I know and friends of mine.</p>
  <p>You can still login as a guest, if you want to see the content of this platform.</p>");    //Signup is temporarily unavailable.*/
?>

<main>
	<div>
		<section>
      <p>Please, take a few minutes to read the <a href="aboutmainpage.php#codeofconduct" target="_blank">code of conduct</a> before you sign-up.</p>
      <br>

      <label form="form-signup">Signup</label>

			<form action="includes/signup-inc.php" method="post" name="form-signup" id="form-signup">
				<input type="text" name="uid" placeholder="Username..." maxlength="20" required="required" title="Use UPPER-case/lower-case letters and numbers for username">
				<input type="Password" name="pwd" placeholder="Password..." required="required" minlength="6" maxlength="20" title="Password must be at least 6 characters long and no more than 20.">
				<input type="Password" name="pwd-repeat" placeholder="Repeat password..." required="required" minlength="6">
        <?php
        if ($modeForSigningUp == "enable_sign_up_with_secret_code") {
          ?>
          <input type="Password" id="secretCodeForSignup" name="secretCodeForSignup" placeholder="Secret code for signing up..." required="required" minlength="8" maxlength="18">
          <?php
          }
        ?>
				<button type="submit" name="signup-submit">Signup</button>
			</form>

      <p id="p_signup_urlvar"></p>

      <script type="module" src="./signup.js">
        
      </script>

		</section>
	</div>
</main>

<?php
  require "footer.php";
?>
<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Crowd Discusses Alternatives">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Crowd Discusses Alternatives</title>
	<link rel="stylesheet" type="text/css" href="style_cda.css">
</head>
<body>
	<header>
		<ul class="header_ul">
			<li><a href="index.php" id="logo" target="_blank">Crowd Discusses Alternatives</a></li>
			<li><a href="signup.php" id="signup">Signup</a></li>
			<li><a href="login.php" id="login">Login</a></li>
			<li><a href="logout.php" id="logout">Logout</a></li>
			<li><a href="aboutmainpage.php" id="about" target="_blank">About</a></li>
			
		</ul>

		<?php
      if (isset($_SESSION['userId'])) {
    ?>

        <script type="text/javascript" src="./headerloggedin.js"></script>
		  	
    <?php
      } else { 
    ?>

        <script type="text/javascript" src="./headerloggedout.js"></script>
 
    <?php
      } 
    ?>	
	</header>
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2023 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
infoMessage = document.getElementById('p_errorpage_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'noinitiator') {
    infoMessage.innerHTML =  "Error: Initiator not found, or topic not found.";
  }
  else if (errorVar == 'notopic') {
    infoMessage.innerHTML =  "Error: T_ID not found.";
  }
  else if (errorVar == 'nottopic') {
    infoMessage.innerHTML =  "Error: Topic category was expected to be sent to server.";
  }
  else if (errorVar == 'notmember') {
    infoMessage.innerHTML =  "You are not a member of this topic.<br><br>You can send a message to the initiator of the topic, if you want to become a member.";
  }
  else if (errorVar == 'notimetableyet') {
    infoMessage.innerHTML = "The time table of the topic has not been created yet." + 
    " You can contact with the initiator of the topic via a personal message.";
  }
  else if (errorVar == 'textmaxlength') {
    infoMessage.innerHTML = "Error: Number of characters in text exceeds limit.<br><br> Please note that each one of the < and > characters takes the space of three characters (except in &ltbr&gt which is the only HTML tag that is allowed).";
  }
  else if (errorVar == 'wrongcateg') {
    infoMessage.innerHTML = "Error: Category sent to server is not what was expected.";
  }
  else if (errorVar == 'wrongcategname') {
    infoMessage.innerHTML = "Error: Category name sent to server is not what was expected.";
  }
  else if (errorVar == 'noid') {
    infoMessage.innerHTML = "Error: ID cannot be zero neither null.";
  }
  else if (errorVar == 'notgpcr') {
    infoMessage.innerHTML = "Error: Zero tgpcr.";
  }
  else if (errorVar == 'emptyfields') {
    infoMessage.innerHTML = "Please fill in all the fields of the form.";
  }
  else if (errorVar == 'bbidnull') {
    infoMessage.innerHTML = "Error: BB_ID not found.";
  }
  //ID must be a positive integer.
  else if (errorVar == 'novalidid') {
    infoMessage.innerHTML = "Error: Not valid Id.";
  }
  else if (errorVar == 'entryrejected') {
    infoMessage.innerHTML = "Error: Currently, the limit of the table in the database (for this action) has been reached. Please, try again after 1 day.";
  }
  else if (errorVar == 'usernameisguest0001') {
    infoMessage.innerHTML = "Error: Please note that the account with username guest0001 is not allowed to create content or become a team-member. This account can only be used for viewing content.";
  }
}
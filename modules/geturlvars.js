/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
class GetUrlVars {
    constructor(url) {
        this.urlVars = [""];
        this.urlVarValues = [""];
        this.url = url;
    }

    findUrlVars() {
        var strPos, urlVars, urlVarValues;
        strPos = this.url.indexOf("?");    //indexOf() returns the position of the first occurrence of a specified value in a string (the first letter is position 0). It  returns -1 if the value to search for never occurs.
        if (strPos != -1) {
            var slicedstr, nextStrPos, urlVarPairs, i;
            urlVarPairs = [""];
            i = 0;
            slicedstr = this.url.slice(strPos+1);   //slice() selects the elements in an array starting at the given start argument, and ends at, but does not include, the given end argument (the first element has an index of 0).
            urlVarPairs[i] = slicedstr;    //urlVarPairs declared if there is no & in slicedstr.
            nextStrPos = slicedstr.indexOf("&");
            while (nextStrPos != -1) {
                nextStrPos = slicedstr.indexOf("&");
                if (nextStrPos != -1) {
                    urlVarPairs[i] = slicedstr.slice(0, nextStrPos);
                    slicedstr = slicedstr.slice(nextStrPos+1, slicedstr.length);
                } else {
                    urlVarPairs[i] = slicedstr;
                }
                i++;
            }
          
            let arrayLength = urlVarPairs.length;
            for (i = 0; i < arrayLength; i++) {
                nextStrPos = urlVarPairs[i].indexOf("=");
                this.urlVars[i] = urlVarPairs[i].slice(0, nextStrPos);
                this.urlVarValues[i] = urlVarPairs[i].slice(nextStrPos+1, urlVarPairs[i].length);
            }
        }
    }

    urlVar(name) {
        this.findUrlVars();
        let arrayIndex = this.urlVars.indexOf(name);
        if (arrayIndex != -1) {
          return this.urlVarValues[arrayIndex];
        } else {
            return "";
        }
    }
}

export { GetUrlVars };
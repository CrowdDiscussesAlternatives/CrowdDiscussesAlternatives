<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<div class="info_text" id="div_time_table_info">
  <p>
    Note that:
    <br><br>
    During the first phase: The initiator of the topic can invite other users of Crowd Discusses Alternatives as team members, by sending personal messages with the id or the link of the topic in them. Consider an adequate duration of this phase in order the people that you invite to be able to respond back [1].
    <br><br>
    During the second phase: The team can make the topic more specific by attaching requirements to it. In this phase goals can become quantified and time-framed, necessary resources can be specified. The team can search for references/sources early in this phase in order to better understand the topic. These references can be later added to proposals.
    <br><br>
    During the third phase: Each member can submit his/her proposals in the topic, attach references to them, and specify also the period that each proposal can be editable (changed). This period can be used for, not only taking feedback from other members concerning your proposals, but also for commenting on others proposals in order to help them inprove their proposals and in order to be better informed about the available proposals of the topic. When the editing phase of a proposal ends, members can vote for/against it, or comment about it, specifying the pros and cons of the proposal.
    <br><br>
    During the fourth phase: The team can start forming groups of proposals that will represent the alternative solutions of the topic. Members can comment on a group, mostly for subjects that concern a group (alternative solution) as a whole. They can also vote for/against a group, in order to become clear which alternatives are considered the best. 
    <br><br>
    Members can also comment on or submit proposals during this phase, however before they can group these new proposals, they must wait for a period of 15 days after their editing periods. During these 15 days they can mainly comment on or vote for/against these new proposals. After this period, they can also use these new proposals in groups.
    <br><br>
    Please note that the initiator of the topic can only postpone a phase (he/she cannot terminate a phase earlier).
    <br><br>
    Update [1]: Currently, the initiator of the topic can add new members until the end of the third phase. However, after the end of the first phase, it is better for the initiator of the topic to seek advice from those that are early members, when inviting new members to the topic.
    <br><br>
    <!--Please note that the initiator can create the time-table of the topic only once. if a member wants to update the time-table or to close a phase in order to start the next one, he/she can propose it via a poll. The results of the poll will change automatically the time table.-->
  </p>
</div>
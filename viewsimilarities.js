/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var offsetOfS; // they are on global scope and accessed easily. it is better to use closure.
offsetOfS = 0;

function btnShow()
{
    let numOfS, numOfComments, cbShowComments, orderBy, ascDesc, selectedT;    //G: Group.
    numOfS =  Number(document.getElementById('div_numofsimilarities').innerHTML);
    orderBy = document.getElementById('div_orderby').innerHTML;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    offsetOfS += numOfS;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreS, newDivS, buttonShowMoreS;
            newDivS = document.createElement("div");
            newDivS.setAttribute('id', ('div_groups_' + offsetOfS.toString()));
            divShowMoreS = document.getElementById('shows');
            buttonShowMoreS = document.getElementById('btn_shows');
            divShowMoreS.insertBefore(newDivS, buttonShowMoreS);
            newDivS.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4, hrefString5, hrefString6;
    hrefString1 = "offset=" + offsetOfS.toString();
    hrefString2 = "&numofsimilarities=" + numOfS.toString();
    hrefString5 = "&orderby=" + orderBy;
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString7 = "&selectedt=" + selectedT;
    hrefString = hrefString1 + hrefString2 + hrefString5 + hrefString6 + hrefString7;
    xhttp.open("post", "viewsimilarities_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}
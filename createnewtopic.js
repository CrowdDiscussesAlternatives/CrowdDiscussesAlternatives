/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, createVar, newTopicVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
createVar = UrlVars.urlVar('create');
newTopicVar = UrlVars.urlVar('newtopic');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'bbidnull') {
    infoMessage.innerHTML = "Error: BB_ID not found.";
    document.getElementById('form_create_topic').style.display = 'none';
  }
  else if (errorVar == 'notopic') {
    infoMessage.innerHTML = "You cannot create an empty topic!";
  }
  else if (errorVar == 'topicexists') {
    infoMessage.innerHTML =  "This topic already exists!";
    document.getElementById('form_create_topic').style.display = 'none';
  }
} 
else if (createVar != "") {
  if (createVar == 'success') {
    infoMessage.innerHTML =  "You have successfully created the topic <a href=\"selected.php?selected=" + newTopicVar.toString() 
    + "&parent=n0_n0_n0\" target=\"_blank\">" + newTopicVar.toString() + "</a>." 
    + "<br><br>Please, specify firstly the time-table of the created topic. You will find it in the navigation bar (menu) of the above link." 
    + "<br><br>You can also send personal messages to invite team members (remember to copy-paste the link of the topic in the messages)." 
    + " If they respond by accepting the invitation, you can add them as team members of the topic.";
    document.getElementById('form_create_topic').style.display = 'none';
  }
}
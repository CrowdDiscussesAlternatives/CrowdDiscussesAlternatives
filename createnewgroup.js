/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, createVar,existingGroupIdVar, topicIdVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
createVar = UrlVars.urlVar('create');
existingGroupIdVar = UrlVars.urlVar('groupid');
topicIdVar = UrlVars.urlVar('topicid');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'notitle') {
    infoMessage.innerHTML = "Please enter a title for the group.";
  } 
  else if (errorVar == 'noproposals') {
    infoMessage.innerHTML = "You cannot create a group with no proposals!";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_selected_proposals').style.display = 'none';
  }
  else if (errorVar == 'groupsphasenotyet') {
    infoMessage.innerHTML = "\"Groups\" phase (fourth phase) has not been started yet." + 
    " You can view the time table for more info.";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_selected_proposals').style.display = 'none';
  }
  else if (errorVar == 'groupsstop') {
    infoMessage.innerHTML = "\"Groups\" phase (fourth phase) has been completed." + 
    " You can view the time table for more info.";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_selected_proposals').style.display = 'none';
  }
  else if (errorVar == 'groupexists') {
    infoMessage.innerHTML = "This group of proposals already exists with id <a href=\"selected.php?selected=g" + 
    existingGroupIdVar + "&parent=t" + topicIdVar + "_n0_n0\" target=\"_blank\">g" + existingGroupIdVar + "</a>.";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_selected_proposals').style.display = 'none';
  }
  else if (errorVar == 'titleexists') {
    infoMessage.innerHTML = "This title has already been used. Please select another title for the group!";
  }
} 
else if (createVar != "") {
  if (createVar == 'success') {
    infoMessage.innerHTML =  "You have successfully created the group.";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_selected_proposals').style.display = 'none';
  }
}
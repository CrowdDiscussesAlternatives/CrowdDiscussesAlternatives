<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require "header.php";
?>

<main>

  <?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
      if (!isset($_REQUEST['selected'])) {
        exit("<br><br><b>-- Error: ID is not found!<b>");
      }

      $selectedCateg = 'n';
      require_once "selectednavbar.php";

      $cdaViewObj = new CdaView();
      $tgpcrObj = new Tgpcr();

      $selectedCateg = substr($_REQUEST['selected'], 0, 1);
      $selectedTgpcrID = intval(substr($_REQUEST['selected'], 1));
      $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
      $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

      if ($selectedCategMsg == 'Non-existent category!') {
        exit('-- Error: Category sent to server does not exist!');
      }
  ?>

      <br>
      <label form="form_create">Create a new comment</label>

      <form action="includes/createnewcomment-inc.php" method="post" name="form_create" id="form_create">
        <textarea name="textarea_name" placeholder="Comment..." maxlength="4000" cols="30" required></textarea> <!-- wrap="hard". 1334 for chinese characters. It should become 4000 i.e. VARCHAR(11986) in database!-->
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedcategory" name="selectedcategory" value="<?php echo $selectedCateg; ?>">
          <input type="text" id="selectedid" name="selectedid" value="<?php echo $selectedTgpcrID; ?>">
        </div>
        <button type="submit" name="create_submit">OK</button>
        <p>Tip: You can write &ltbr&gt if you want to insert a line break (please note that no other html tag is allowed).</p>
      </form>
      <br>

      <p>of selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
  <?php
      require_once "headeroftable.php";

      $category = $selectedCateg;
      require "viewtgpcr.php";
  ?>

      <p id="p_create_urlvar"></p>

      <script type="module" src="./createnewcomment.js"></script>
      <script type="text/javascript" src="./viewtgpcr.js"></script>
  <?php
      unset($cdaViewObj);
      unset($tgpcrObj);
    } else {
      //redirect to login
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
      Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
    }
  ?>

</main>

<?php
  require "footer.php";
?>
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, createVar, existingSimilaritiesIdsVar, sumOfNewSimilarities, infoMessage, arrayOfIdsOfExistingSimilarities;
let displayedTextOfExistingSimilarities, textS, messageText, usersExistingReversedSimilaritiesIdsVar, arrayOfIdsOfUsersExistingReversedSimilarities;
let displayedTextOfUsersExistingReversedSimilarities;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
createVar = UrlVars.urlVar('create');
existingSimilaritiesIdsVar = UrlVars.urlVar('existings');
usersExistingReversedSimilaritiesIdsVar = UrlVars.urlVar('existingsrev');
sumOfNewSimilarities = UrlVars.urlVar('newSNum');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

arrayOfIdsOfExistingSimilarities = existingSimilaritiesIdsVar.split(",");
arrayOfIdsOfUsersExistingReversedSimilarities = usersExistingReversedSimilaritiesIdsVar.split(",");

displayedTextOfExistingSimilarities = prepareTextForExistingS(arrayOfIdsOfExistingSimilarities);
displayedTextOfUsersExistingReversedSimilarities = prepareTextForExistingS(arrayOfIdsOfUsersExistingReversedSimilarities);

if (sumOfNewSimilarities == 1) {
  textS = "similarity";
} else {
  textS = "similarities";
}

if (errorVar != "") {
  if (errorVar == 'fewproposals') {
    infoMessage.innerHTML = "You must select at least two proposals in order to report them as similar!";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_selected_proposals').style.display = 'none';
  }
  else if (errorVar == 'nopaid') {
    infoMessage.innerHTML = "<b>Note: You must select one proposal as the most well-written from the similar proposals below!</b>"
     + " You can click on its text (content) to select it!";
} 
  }
else if (createVar != "") {
  if (createVar == 'success') {
    if (arrayOfIdsOfExistingSimilarities[0] == "" || arrayOfIdsOfExistingSimilarities[0] == "0") {
      messageText = "You have created " + sumOfNewSimilarities + " new " + textS + ".<br><br>";
    } else {
      messageText = "You have created " + sumOfNewSimilarities + " new " + textS + ".<br><br>" + 
    "The similarities with the following ids already exist: " + displayedTextOfExistingSimilarities + ".";
    }

    if (arrayOfIdsOfUsersExistingReversedSimilarities[0] != "" && arrayOfIdsOfUsersExistingReversedSimilarities[0] != "0") {
      messageText += "<br>Some similarities could not be created because you have already reported" + 
      " them with the reversed order of proposals. See similarities:" + displayedTextOfUsersExistingReversedSimilarities + ".";
    }

    infoMessage.innerHTML = messageText;
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_selected_proposals').style.display = 'none';
    document.getElementById('tip_select').style.display = 'none';
  }
}

function prepareTextForExistingS(arrayOfIdsOfExistingS)
{
  let displayedTextOfExistingS, lengthOfArray, prepareText;

  prepareText = "";
  if (arrayOfIdsOfExistingS[0] == "" || arrayOfIdsOfExistingS[0] == "0") {
    displayedTextOfExistingS = "-";
    return displayedTextOfExistingS;
  }

  lengthOfArray = arrayOfIdsOfExistingS.length;
  for (var i = 0; i < lengthOfArray; i++) {
    prepareText += "<a href=\"selected.php?selected=s" + 
    arrayOfIdsOfExistingS[i] + "&parent=n0_n0_n0\" target=\"_blank\">s" + arrayOfIdsOfExistingS[i] + "</a>, "; 
  }

  displayedTextOfExistingS = prepareText.substr(0, prepareText.length-2);

  return displayedTextOfExistingS;
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 require "header.php";
?>

<main>

	<?php
	  $cdaContrObj = new CdaContr();
	  $cdaContrObj->checkIfLoggedIn(null);

	  $cdaViewObj = new CdaView();
	  $tgpcrObj = new Tgpcr();

	  $selectedCateg = 'n';
	  require_once "selectednavbar.php";

	  $selectedCateg = substr($_REQUEST['selected'], 0, 1);
	  $selectedTgpcrID = intval(substr($_REQUEST['selected'], 1));
	  $tgpcr = $cdaViewObj->showSelectedTopicWithTimeTableInfo((int) $selectedTgpcrID);
	  $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

	  if ($selectedCategMsg == 'Non-existent category!') {
	  	require "closehtmltag.php";
	    exit('-- Error: Category sent to server does not exist!');
	  } elseif ($selectedCategMsg != 'topic') {
	  	require "closehtmltag.php";
	    exit('-- Error: Category of topic (for time-table update) was expected to be sent to server!');
	  }

	  $topicInfo = array('inv_state' => $tgpcr['inv_state'], 'req_state' => $tgpcr['req_state'], 'prop_state' => $tgpcr['prop_state'], 'groups_state' => $tgpcr['groups_state'], 'invitation_phase_closing_date' => $tgpcr['invitation_phase_closing_date'], 'requirements_phase_closing_date' => $tgpcr['requirements_phase_closing_date'], 'proposals_phase_closing_date' => $tgpcr['proposals_phase_closing_date'], 'groups_phase_closing_date' => $tgpcr['groups_phase_closing_date'], 'timetable_changed' => $tgpcr['timetable_changed']);

    if ($topicInfo['timetable_changed'] == -1) {
    	require "closehtmltag.php";
      exit('-- Please note that the time-table of this topic has not been specified yet! You need to firstly specify the time-table in order to be able to postpone the end of a phase.');
    }

	  if ($topicInfo['inv_state'] > 0) {
	  	$currentPhase = 'inv_state';
	  	$currentPhaseMsg = 'team members invitation and references not in proposals yet';
	  	$currentEndOfPhase = $topicInfo['invitation_phase_closing_date'];
	  } elseif ($topicInfo['req_state'] > 0) {
	  	$currentPhase = 'req_state';
	  	$currentPhaseMsg = 'requirements and references not in proposals yet';
	  	$currentEndOfPhase = $topicInfo['requirements_phase_closing_date'];
	  } elseif ($topicInfo['prop_state'] > 0) {
	  	$currentPhase = 'prop_state';
	  	$currentPhaseMsg = 'proposals and references of proposals';
	  	$currentEndOfPhase = $topicInfo['proposals_phase_closing_date'];
	  } elseif ($topicInfo['groups_state'] > 0) {
	  	$currentPhase = 'groups_state';
	  	$currentPhaseMsg = 'proposals, references and groups of proposals';
	  	$currentEndOfPhase = $topicInfo['groups_phase_closing_date'];
	  } else {
	  	require "closehtmltag.php";
	  	exit('-- Please note that all the phases of this topic have been closed. You cannot postpone the end of a phase any more!');
	  }
	?>

  <p>Selected <?php echo $selectedCategMsg; ?>:</p>

  <div class="tgpcrAll">
  <?php
    require_once "headeroftable.php";

    $category = $selectedCateg;
    require "viewtgpcr.php";
  ?>

  <p class="class_tip">The current phase of the topic is the <b>"<?php echo $currentPhaseMsg; ?>"</b> phase. It ends on <?php 
    $date = new DateTime($currentEndOfPhase);
    echo $date->format('Y-m-d'); ?>.</p>

    <p class="class_tip" id="noteforlaterphases">
    	<b>Please note that all of the phases that are after the current phase will also been postponed by the same amount of days.</b>
    </p>

  <br>
  <label form="form_create">Postpone the end of the current phase</label>

  <form action="includes/postponecurrentphase-inc.php" method="post" name="form_create" id="form_create">
    <div class="div_inputlabel_timetable">
    <label class="label_for" for="postponeendofcurrentphaseindays">Number of days that the current phase and every subsequent phase will be postponed:</label>
    <input class="input_time_table" type="number" id="postponeendofcurrentphaseindays" name="postponeendofcurrentphaseindays" title="Minimum 3 days, maximum 15 days." placeholder="3 to 15 days..." required min="3" max="15">
    </div>
    <div id="selecteddata" style="display: none;">
    	<input type="text" id="currentphase" name="currentphase" value="<?php echo $currentPhase; ?>">
      <input type="text" id="topicid" name="topicid" value="<?php echo $selectedTgpcrID; ?>">
    </div>
    <button type="submit" name="create_submit">OK</button>
  </form>
  <br>

  <p id="p_create_urlvar"></p>

  <br>
  <div class="div_as_label">Time-table of selected topic</div>
    <div class="div_view_timetable">
      <div class="div_inputlabel_timetable">
        <p>Closing date of <b>team members invitation and references not in proposals yet</b> phase:</p>
        <div class="div_show_closing_date">
          <?php 
            $date = new DateTime($topicInfo["invitation_phase_closing_date"]);
            echo $date->format('Y-m-d');
          ?>
        </div>
      </div>
      <div class="div_inputlabel_timetable">
        <p>Closing date of <b>requirements and references not in proposals yet</b> phase:</p>
        <div class="div_show_closing_date">
          <?php 
            $date = new DateTime($topicInfo["requirements_phase_closing_date"]);
            echo $date->format('Y-m-d');
          ?>
        </div>
      </div>
      <div class="div_inputlabel_timetable">
        <p>Closing date of <b>proposals and references of proposals</b> phase:</p>
        <div class="div_show_closing_date">
          <?php 
            $date = new DateTime($topicInfo["proposals_phase_closing_date"]);
            echo $date->format('Y-m-d');
          ?>
        </div>
      </div>
      <div class="div_inputlabel_timetable">
        <p>Closing date of <b>proposals, references and groups of proposals</b> phase:</p>
        <div class="div_show_closing_date">
          <?php 
            $date = new DateTime($topicInfo["groups_phase_closing_date"]);
            echo $date->format('Y-m-d');
          ?>
        </div>
      </div>
  </div>

  <p>
    Dates are in (in year-month-day). Time-table has been updated <?php echo $topicInfo["timetable_changed"] ?> times.
  </p>

  <?php
    require "timetableinfotext.php";
  ?>

  <script type="module" src="./postponecurrentphase.js"></script>
  <script type="text/javascript" src="./viewtgpcr.js"></script>

	<?php
	  unset($cdaContrObj);
	  unset($cdaViewObj);
	  unset($tgpcrObj);
	?>

</main>

<?php
  require "footer.php";
?>
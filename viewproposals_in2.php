<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

$totalnumOfP = 0;

if (isset($_REQUEST['offset'], $_REQUEST["cbshowcomments"], $_REQUEST["numofcomments"], $_REQUEST["cbshowreferences"], $_REQUEST["numofreferences"], $_REQUEST['orderby'], $_REQUEST['ascdesc'])) {
  if (!isset($_REQUEST['selectedt'])) {
   exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }
  $offset = intval($_REQUEST['offset']);
  $numOfP = intval($_REQUEST['numofproposals']);
  $cbShowComments = $_REQUEST['cbshowcomments'];
  $numOfComments = intval($_REQUEST['numofcomments']);
  $cbShowReferences = $_REQUEST['cbshowreferences'];
  $numOfReferences = intval($_REQUEST['numofreferences']);
  $orderBy = mb_strtoupper($_REQUEST['orderby']);
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfP = intval($_SESSION['numofproposals']);
  $cbShowComments = $_SESSION['cbshowcomments'];
  $numOfComments = intval($_SESSION['numofcomments']);
  $cbShowReferences = $_SESSION['cbshowreferences'];
  $numOfReferences = intval($_SESSION['numofreferences']);
  $orderBy = $_SESSION['orderby'];
  $ascDesc = $_SESSION['ascdesc'];
}

$cdaViewObj = new CdaView();
//TO DO: find topicId and check if member!!!
$topicId = intval(substr($_REQUEST['selectedt'], 1));
if ($topicId == 0 || $topicId == null) {
  exit("<br><br>-- Error: Topic ID is not found!");
}

if (isset($_REQUEST['lcp'])) {
  $proposals = $cdaViewObj->showLatestCommentedProposals((int)$topicId, (int)$numOfP);
  $totalnumOfP = $cdaViewObj->totalNumberOfProposals((int)$topicId);
} elseif (isset($_REQUEST['selectedm'])) {
  $membersIds = explode(",", $_REQUEST['selectedm']);
  $proposals = $cdaViewObj->showProposalsOfSelectedMembers((int)$topicId, $membersIds, $orderBy, $ascDesc);
  $totalnumOfP = count($proposals);
} elseif (isset($_REQUEST['scoreincateg'])) {
  $proposals = $cdaViewObj->showProposalsOfMembersWithHighScoreInSelectedCateg((int)$topicId, $_REQUEST['scoreincateg'], $_REQUEST['numofmemb'], $orderBy, $ascDesc);
  $totalnumOfP = count($proposals);
} elseif (isset($_REQUEST['selectedp'])) {
  $proposals = $cdaViewObj->showProposalsWithSelectedIds((int)$topicId, $_REQUEST['selectedp']);
  $totalnumOfP = count($proposals);
} elseif (isset($_REQUEST['viewp'], $_REQUEST['strs'], $_REQUEST['m'])) {
  // $_REQUEST['m'] is either 'currentmember' or 'allmembers'.
  $proposals = $cdaViewObj->showProposalsThatTheirCommentsContainStr((int)$topicId, $_REQUEST['strs'], $_REQUEST['m'], $orderBy, $ascDesc);
  $totalnumOfP = count($proposals);
} elseif (isset($_REQUEST['v1by'])) {
  $proposals = $cdaViewObj->showProposalsThatUserVoted1((int)$topicId);
  $totalnumOfP = count($proposals);
} elseif (isset($_REQUEST['viewp_excls_v1by'])) {
  $proposals = $cdaViewObj->ShowProposalsExcluding2ndPsFromSimilaritiesThatUserVoted1((int)$topicId, $orderBy, $ascDesc);
  $totalnumOfP = count($proposals);
} else {
  $proposals = $cdaViewObj->showProposals((int)$topicId, (int)$offset, (int)$numOfP, $orderBy, $ascDesc);
  $totalnumOfP = $cdaViewObj->totalNumberOfProposals((int)$topicId);
}

if ($proposals == []) {
?>
  <p>All of <?php echo $totalnumOfP; ?> proposals are displayed!</p>
<?php
  exit();
}

foreach ($proposals as $PKey => $proposalValue) {
  $category = 'p';
  $tgpcr = $proposalValue;
  require "viewtgpcr.php";

  if ($cbShowReferences == 'true') {
    $references = $cdaViewObj->showReferencesInProposal($proposalValue['id'], 0, (int)$numOfReferences, $orderBy, $ascDesc);
    if (isset($references) || $references!=[]) {
      foreach ($references as $referenceKey => $referenceValue) {
          $category = 'r';
          $tgpcr = $referenceValue;
          require "viewtgpcr.php";
      }
    }
  }

  if ($cbShowComments == 'true') {
    $comments = $cdaViewObj->showComments('3' , $proposalValue['id'], 0, (int)$numOfComments, $orderBy, $ascDesc); //! $orderBy will be used in the future if there will be voting for comments. Now it is set to comments_in_cda.id. !
    if (isset($comments) || $comments!=[]) {
      foreach ($comments as $commentKey => $commentValue) {
          $category = 'c';
          $tgpcr = $commentValue;
          require "viewtgpcr.php";
      }
    }
  }
}
?>
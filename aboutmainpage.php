<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 $cdaContrObj = new CdaContr();
 $cdaContrObj->setSessionExpCookie();
 require "header.php";
?>

<main>
 <div class="aboutpage">
	<?php
	  include 'about/aboutcontentspage.php';
	  include 'about/aboutintroductionpage.php';
	  include 'about/aboutpurposepage.php';
    include 'about/aboutcoreconceptspage.php';
    include 'about/aboutfaqpage.php';
    include 'about/aboutsourcecodepage.php';
    include 'about/aboutcodeofconduct.php';
    include 'about/aboutmoderationinformation.php';
    include 'about/aboutusefulguidelines.php';
    include 'about/abouthelpfullinks.php';
	?>
 </div>
</main>

<?php
  require "footer.php";
?>
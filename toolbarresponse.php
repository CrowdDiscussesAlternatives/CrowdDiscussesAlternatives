<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
  session_start();    //session_start(); on the top of the code.
  require "header.php";
?>

<main>

	<?php

	  if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
	 	  if (isset($_POST["numoftopics"], $_POST["numofgroups"], $_POST["numofproposals"], $_POST["numofcomments"], $_POST["numofreferences"])) {
	 	 	  $_SESSION["numoftopics"] = intval($_POST["numoftopics"]);
	 	 	  $_SESSION["numofgroups"] = intval($_POST["numofgroups"]);
	 	 	  $_SESSION["numofproposals"] = intval($_POST["numofproposals"]);
	 	 	  $_SESSION["numofcomments"] = intval($_POST["numofcomments"]);
	 	 	  $_SESSION["numofreferences"] = intval($_POST["numofreferences"]);

	 	 	  if (isset($_POST["cb_showproposals"]) && $_POST["cb_showproposals"] == 'true') {
	 	 	  	$_SESSION["cbshowproposals"] = 'true';
	 	 	  } else {
	 	 	  	$_SESSION["cbshowproposals"] = 'false';
	 	 	  }

	 	 	  if (isset($_POST["cb_showcomments"]) && $_POST["cb_showcomments"] == 'true') {
	 	 	  	$_SESSION["cbshowcomments"] = 'true';
	 	 	  } else {
	 	 	  	$_SESSION["cbshowcomments"] = 'false';
	 	 	  }

	 	 	  if (isset($_POST["cb_showreferences"]) && $_POST["cb_showreferences"] == 'true') {
	 	 	  	$_SESSION["cbshowreferences"] = 'true';
	 	 	  } else {
	 	 	  	$_SESSION["cbshowreferences"] = 'false';
	 	 	  }

	 	 	  if (isset($_POST["orderby"])) {
	 	 	  	$_SESSION["orderby"] = mb_strtoupper($_POST["orderby"]);
	 	 	  } else {
	 	 	  	$_SESSION["orderby"] = 'DATE';
	 	 	  }

	 	 	  if (isset($_POST["ascdesc"])) {
	 	 	  	$_SESSION["ascdesc"] = mb_strtoupper($_POST["ascdesc"]);
	 	 	  } else {
	 	 	  	$_SESSION["ascdesc"] = 'DESC';
	 	 	  }
	?>
	 	    <p>Toolbar data are successfully stored.</p>
	 	    <!--<p><a href="toolbar.php">Return to Toolbar.</a></p>-->
	<?php
	 	  }
	  } else {
	?>

	  <p>You are not logged in yet (or you are logged out). Toolbar changes are not stored.<br><br>
	 Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

	<?php
	  }
  ?>

</main>

<?php
  require "footer.php";
?>
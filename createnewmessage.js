/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, createVar, nameVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
createVar = UrlVars.urlVar('create');
nameVar = UrlVars.urlVar('name');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'noid') {
    infoMessage.innerHTML = "<b>Please notice that the user name \"" + nameVar + "\" does not exists. The message has not been sent.</b>";
  } 
} 
else if (createVar != "") {
  if (createVar == 'success') {
    infoMessage.innerHTML =  "You have successfully sent the message.";
    document.getElementById('form_create').style.display = 'none';
  }
}
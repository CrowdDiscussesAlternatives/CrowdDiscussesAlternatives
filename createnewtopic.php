<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
      require_once "viewtopicsnavbar.php";
  ?>

      <br>
      <label form="form_create_topic">Create a new topic</label>

      <form action="includes/createnewtopic-inc.php" method="post" name="form_create_topic" id="form_create_topic">
        <textarea name="topic_textarea" placeholder="Topic..." maxlength="300" cols="30" required></textarea> <!-- wrap="hard" -->
        <button type="submit" name="create_topic_submit">OK</button>
      </form>
      <br>

      <p id="p_create_urlvar"></p>

      <script type="module" src="./createnewtopic.js"></script>

	<?php
    } else {
      //redirect to login
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
      Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>

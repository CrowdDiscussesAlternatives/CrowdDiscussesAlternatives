/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, createVar, refIdVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
createVar = UrlVars.urlVar('create');
refIdVar = UrlVars.urlVar('refid');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'emptyurlinput') {
    infoMessage.innerHTML = "Please note that reference link (URL) field cannot be empty.";
  } 
  else if (errorVar == 'nowriterofp') {
    infoMessage.innerHTML = "You are not the writer of this proposal in order to be able to add references to it!";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'reqhasenotyet') {
    infoMessage.innerHTML = "The \"requirements\" phase (second phase) has not been started yet." + 
    " You can view the time table for more info.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'groupsphasestop') {
    infoMessage.innerHTML = "You cannot add references anymore since the \"groups\" phase (fourth phase) of the topic has been closed." + 
    " You can view the time table for more info.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'alreadyinp') {
    infoMessage.innerHTML = "The reference is already added to this proposal!";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'alreadyint') {
    infoMessage.innerHTML = "The reference is already in this topic! You can add this reference to any of your proposals.";
    document.getElementById('form_create').style.display = 'none';
  }
} 
else if (createVar != "") {
  if (createVar == 'successinp') {
    infoMessage.innerHTML =  "The reference has been created and successfully added to the proposal.";
    document.getElementById('form_create').style.display = 'none';
  } else if (createVar == 'successint') {
    infoMessage.innerHTML =  "The reference has been created. You can add this reference to any of your proposals of this topic.";
    document.getElementById('form_create').style.display = 'none';
  } else if (createVar == 'refaddedtop') {
    infoMessage.innerHTML =  "The reference already exists with" + 
    " id <a href=\"selected.php?selected=r" + refIdVar + "&parent=n0_n0_n0\" target=\"_blank\">r" + refIdVar + "</a> " + 
    "and is succesfully added to this proposal.";
    document.getElementById('form_create').style.display = 'none';
  } else if (createVar == 'refaddedtot') {
    infoMessage.innerHTML =  "The reference already exists with" + 
    " id <a href=\"selected.php?selected=r" + refIdVar + "&parent=n0_n0_n0\" target=\"_blank\">r" + refIdVar + "</a> " + 
    "and is incorporated to the topic. You can add this reference to any of your proposals of this topic.";
    document.getElementById('form_create').style.display = 'none';
  }
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

$totalnumOfTM = 0;
  
if (!isset($_REQUEST['selectedt'])) {
 exit("<br><br><b>-- Error: Topic ID is not found!<b>");
}

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

$topicId = intval(substr($_REQUEST['selectedt'], 1));
if ($topicId == 0 || $topicId == null) {
  exit("<br><br>-- Error: Topic ID is not found!");
}

if (isset($_REQUEST['rank_m_by_u_vs'])) {
  $TeamMembers = $cdaViewObj->rankMembersByUsersVoteOfRefs((int)$topicId, 'DESC');
} else {
  $TeamMembers = $cdaViewObj->showAllTeamMembersOfTopic((int)$topicId);
}

foreach ($TeamMembers as $TeamMembersKey => $TeamMembersValue) {
  $category = 'u';
  $tgpcr = $TeamMembersValue;
  require "viewtgpcr.php";
}
?>
<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<nav id="nav">
<?php 
  require_once 'includes/autoloader-inc.php';
  
  if (!isset($selectedCateg)) {
  	$selectedCateg = 'n';
  }

  switch ($selectedCateg) {
  	case 't':
?>
			  <ul>Topic: menu 1
			    <div class="dropdown_content">
			    	<li><a class="nav_a" href="viewtopics.php">View topics</a></li>
			    	<li><a class="nav_a" href="timetableinput.php?selected=<?php echo $_GET['selected'] ?>">Specify time-table</a></li>
			    	<li><a class="nav_a" href="viewtimetable.php?selected=<?php echo $_GET['selected'] ?>">View time-table</a></li>
			    	<li><a class="nav_a" href="postponecurrentphase.php?selected=<?php echo $_GET['selected'] ?>">Postpone current phase</a></li>
			      <li><a class="nav_a" href="addnewmembers.php?selected=<?php echo $_GET['selected'] ?>">Add new members</a></li>
			      <li><a class="nav_a" href="viewteammembers.php?selectedt=<?php echo $_GET['selected'] ?>">View team-members</a></li>
			      <li><a class="nav_a" href="viewteammembers.php?selectedt=<?php echo $_GET['selected'] ?>&rank_m_by_u_vs=true">View members ranked by your votes in ref</a></li>
			      <li><a class="nav_a" href="viewrequirements.php?selectedt=<?php echo $_GET['selected'] ?>">View requirements</a></li>
			      <li><a class="nav_a" href="viewrequirements.php?selectedt=<?php echo $_GET['selected'] ?>&var=true">View approved requirements</a></li>
			      <li><a class="nav_a" href="createnewrequirement.php?selectedt=<?php echo $_GET['selected'] ?>">New requirement</a></li>
			    </div>
			  </ul>
			  <ul>Topic: menu 2
			    <div class="dropdown_content">
			    	<li><a class="nav_a" href="viewtopics.php">View topics</a></li>
			      <li><a class="nav_a" href="viewproposals.php?selectedt=<?php echo $_GET['selected'] ?>">View proposals</a></li>
			      <li><a class="nav_a" href="viewproposals.php?selectedt=<?php echo $_GET['selected'] ?>&lcp=true">View latest commented proposals</a></li>
			      <li><a class="nav_a" href="viewsimilarities.php?selectedt=<?php echo $_GET['selected'] ?>">View similarities of proposals</a></li>
			      <li><a class="nav_a" href="createnewproposal.php?selectedt=<?php echo $_GET['selected'] ?>">New proposal</a></li>
			      <li><a class="nav_a" href="viewreferences.php?selectedtid=<?php echo substr($_GET['selected'], 1) ?>">View references</a></li>
			      <li><a class="nav_a" href="createaddreference.php?selectedtid=<?php echo substr($_GET['selected'], 1) ?>">Add reference</a></li>
			      <li><a class="nav_a" href="viewgroups.php?selectedt=<?php echo $_GET['selected'] ?>">View groups</a></li>
			      <li><a class="nav_a" href="viewcomments.php?selectedt=<?php echo $_GET['selected'] ?>">Search in comments</a></li>
			    </div>
			  </ul>
<?php
  		break;

  	case 'g':
	  	if (!isset($cdaViewObj)) {
	  	  $cdaViewObj = new CdaView();
	  	}
	  	  $groupId = substr($_GET['selected'], 1);
	  	  $tgpcr = $cdaViewObj->showSelectedTgpcr('g', (int)$groupId);
?>
        <ul>Group
	        <div class="dropdown_content">
  		      <li><a class="nav_a" href="viewgroups.php?selectedt=t<?php echo $tgpcr['topic_id'] ?>&selectedgid=<?php echo $groupId ?>">View proposals of group</a></li>
  		      <li><a class="nav_a" href="viewcommentsofselected.php?selected=<?php echo $_GET['selected']; ?>">View comments</a></li>
  		      <li><a class="nav_a" href="evaluategroup.php?selected=<?php echo $_GET['selected']; ?>">Evaluate group</a></li>
  		      <li><a class="nav_a" href="votingeventreport.php?selectedgid=<?php echo $groupId; ?>">Print report of results</a></li>
  		    </div>
			  </ul>
<?php
  		break;

  	case 'p':
?>
        <ul>Proposal
	        <div class="dropdown_content">
  		      <li><a class="nav_a" href="viewreferences.php?selectedpid=<?php echo substr($_GET['selected'], 1) ?>">View references</a></li>
  		      <li><a class="nav_a" href="createaddreference.php?selectedpid=<?php echo substr($_GET['selected'], 1) ?>">Add reference</a></li>
  		      <li><a class="nav_a" href="viewcommentsofselected.php?selected=<?php echo $_GET['selected']; ?>">View comments</a></li>
  		    </div>
			  </ul>
<?php
  		break;

  	case 'r':
?>
        <ul>Reference
	        <div class="dropdown_content">
  		      <li><a class="nav_a" href="viewcommentsofselected.php?selected=<?php echo $_GET['selected']; ?>">View comments</a></li>
  		    </div>
			  </ul>
<?php
  		break;

  	case 'q':
  	if (!isset($cdaViewObj)) {
  	  $cdaViewObj = new CdaView();
  	}
  	  $requirementId = substr($_GET['selected'], 1);
  	  $tgpcr = $cdaViewObj->showSelectedTgpcr('q', (int)$requirementId);
?>
			  <ul>Requirements
			    <div class="dropdown_content">
			      <li><a class="nav_a" href="selected.php?selected=t<?php echo $tgpcr['topic_id'] ?>&parent=n0_n0_n0">Show parent topic</a></li>
			      <li><a class="nav_a" href="viewrequirements.php?selected=<?php echo $_GET['selected'] ?>&selectedt=t<?php echo $tgpcr['topic_id'] ?>">View requirements</a></li>
			      <li><a class="nav_a" href="approverequirement.php?selected=<?php echo $_GET['selected'] ?>&selectedt=t<?php echo $tgpcr['topic_id'] ?>">Review requirement</a></li>
			      <li><a class="nav_a" href="viewcommentsofselected.php?selected=<?php echo $_GET['selected']; ?>">View comments</a></li>
			    </div>
			  </ul>
<?php
  		break;

  	default:

  		break;
  }
?>
		  <ul>Personal-messages
		    <div class="dropdown_content">
		      <li><a class="nav_a" href="viewsentmessages.php" target="_blank">View sent messages</a></li>
		      <li><a class="nav_a" href="viewreceivedmessages.php" target="_blank">View received messages</a></li>
		      <li><a class="nav_a" href="createnewmessage.php" target="_blank">Write a new message</a></li>
		    </div>
		  </ul>
		  <ul>Options
		    <div class="dropdown_content">
		      <li><a class="nav_a" href="toolbar.php" target="_blank">Toolbar</a></li>
		      <li><a class="nav_a" href="changepassword.php" target="_blank">Change password</a></li>
		      <li><a class="nav_a" href="aboutmainpage.php" target="_blank">About</a></li>
		    </div>
		  </ul>
		</nav>
		<br>
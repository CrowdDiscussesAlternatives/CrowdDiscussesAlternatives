<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 $cdaViewObj = new CdaView();
 $cdaContrObj = new CdaContr();

 $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

 require "header.php";
?>

<main>

<?php
  if (!isset($_REQUEST['selectedt'])) {
    exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }

  require_once "viewsimilaritiesnavbar.php";
?>

  <div id="div_toolbardata" style="display: none;">
    <div id="div_numofsimilarities"><?php echo $_SESSION["numofsimilarities"] ?></div>
    <div id="div_orderby"><?php echo $_SESSION["orderby"] ?></div>
    <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
    <div id="div_selectedt"><?php echo $_REQUEST['selectedt'] ?></div>
  </div>
  
  <?php
  if (isset($_REQUEST['c_m'])) {
    ?>
      <p class="class_tip"><b>Below are displayed the similarities (of proposals) that are reported by you.</b></p>
    <?php
  }

  if (isset($_REQUEST['v1by'])) {
    ?>
      <p class="class_tip"><b>Below are displayed the similarities (of proposals) that you have voted +1.</b></p>
    <?php
  }
  ?>

  <p class="class_tip">To select a similarity you can click on its id. Note that when a similarity is reported, the first proposal of the two, is the one that is considered the most well written. <b>When you agree that a pair of proposals are similar, please consider to vote the same for these proposals.</b></p>

	<div class="tgpcrAll">
	<?php
    require "headeroftable.php";
  
    require "viewsimilarities_in2.php";

    if (!isset($_REQUEST['selectedsid']) && !isset($_REQUEST['c_m']) && !isset($_REQUEST['v1by'])) {
      $checkRequestVars = true;
    } else {
      $checkRequestVars = false;
    }

    //When isset($_REQUEST['selectedsid']) == true, it shows only selected group.
    if ($orderBy == 'DATE' && $checkRequestVars == true) {
      $categToShow = 's';
      $tgpcrObj = new Tgpcr();
      $categMsgToShow = $tgpcrObj->categMsg($categToShow);
      $tgpcrInTotalToShow = $totalnumOfS;    //declared in viewsimilarities_in2.php
      unset($tgpcrObj);
      require 'btnshowmoretgpcr.php';
    } elseif ($orderBy != 'DATE' && $checkRequestVars == true) {
      ?>
        <p class="class_tip">Total number of similarities in topic: <?php echo $totalnumOfS; ?></p>
      <?php
    }
  ?>
  </div>
  <br>

  <script type="text/javascript" src="./viewtgpcr.js"></script>
  <script type="text/javascript" src="./viewsimilarities.js"></script>

<?php
  unset($cdaViewObj);
  unset($cdaContrObj);
?>

</main>

<?php
  require "footer.php";
?>
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var offsetOfG; // they are on global scope and accessed easily. it is better to use closure.
offsetOfG = 0;

function btnShow()
{
    let numOfG, numOfComments, cbShowComments, orderBy, ascDesc, selectedT;    //G: Group.
    numOfG =  Number(document.getElementById('div_numofgroups').innerHTML);
    numOfComments = Number(document.getElementById('div_numofcomments').innerHTML);
    cbShowComments = document.getElementById('div_cbshowcomments').innerHTML;
    orderBy = document.getElementById('div_orderby').innerHTML;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    offsetOfG += numOfG;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreG, newDivG, buttonShowMoreG;
            newDivG = document.createElement("div");
            newDivG.setAttribute('id', ('div_groups_' + offsetOfG.toString()));
            divShowMoreG = document.getElementById('showg');
            buttonShowMoreG = document.getElementById('btn_showg');
            divShowMoreG.insertBefore(newDivG, buttonShowMoreG);
            newDivG.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4, hrefString5, hrefString6;
    hrefString1 = "offset=" + offsetOfG.toString();
    hrefString2 = "&numofgroups=" + numOfG.toString();
    hrefString3 = "&numofcomments=" + numOfComments.toString();
    hrefString4 = "&cbshowcomments=" + cbShowComments;
    hrefString5 = "&orderby=" + orderBy;
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString7 = "&selectedt=" + selectedT;
    hrefString = hrefString1 + hrefString2 + hrefString3 + hrefString4 + hrefString5 + hrefString6 + hrefString7;
    xhttp.open("post", "viewgroups_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
  //session_start(): A session had already been started in viewtopics.php.
  require_once 'includes/autoloader-inc.php';
  /* The variables $value['id'], $categSelected, $categToShow, $categMsgSelected, $categMsgToShow, 
     $tgpcrInTotalToShow are declared in vietopics.php */

  if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
	  	$divId = 'show' . $categToShow;
	  	$btnId = 'btn_show' . $categToShow;
	  	$msg1 = ' more ' . $categMsgToShow . 's';
	  	$msg2 = ', (' . $categMsgToShow . 's in total to be shown: ' . $tgpcrInTotalToShow .').';
	  	if ($categToShow == 's') {
  	  	$msg1 = ' more similarities';
  	    $msg2 = ', (similarities in total to be shown: ' . $tgpcrInTotalToShow .').';
	  	}
?>

		<div id="<?php echo $divId; ?>" class="div_showtgpcr">
		  <button type="button" id="<?php echo $btnId; ?>" onclick="btnShow()">Show </button>
		  <p><?php echo $msg1, $msg2 ?></p>
		</div>

<?php
  }
?>
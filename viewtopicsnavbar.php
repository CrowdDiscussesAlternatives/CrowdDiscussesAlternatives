<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<nav id="nav">
  <ul>Topics
    <div class="dropdown_content">
      <li><a class="nav_a" href="viewtopics.php">View topics</a></li>
      <li><a class="nav_a" href="createnewtopic.php">Create new topic</a></li>
      <li><a class="nav_a" href="viewreferences.php">View references</a></li>
    </div>
  </ul>
  <ul>Personal-messages
    <div class="dropdown_content">
      <li><a class="nav_a" href="viewsentmessages.php" target="_blank">View sent messages</a></li>
      <li><a class="nav_a" href="viewreceivedmessages.php" target="_blank">View received messages</a></li>
      <li><a class="nav_a" href="createnewmessage.php" target="_blank">Write a new message</a></li>
    </div>
  </ul>
  <ul>Options
    <div class="dropdown_content">
      <li><a class="nav_a" href="toolbar.php" target="_blank">Toolbar</a></li>
      <li><a class="nav_a" href="changepassword.php" target="_blank">Change password</a></li>
      <li><a class="nav_a" href="aboutmainpage.php" target="_blank">About</a></li>
      <?php
        if (isset($_SESSION['userUid']) && $_SESSION['userUid'] == "admin") {
      ?>
      <li><a class="nav_a" href="viewadminpanel.php" target="_blank">Admin panel</a></li>
      <?php
        }
      ?>
    </div>
  </ul>
</nav>
<br>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

if (!isset($cdaContrObj)) {
  $cdaContrObj = new CdaContr();
}

$totalnumOfC = 0;

if (isset($_REQUEST['ascdesc'])) {
  if (!isset($_REQUEST['selectedt'])) {
   exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $ascDesc = $_SESSION['ascdesc'];
}

//TO DO: find topicId and check if member!!!
$topicId = intval(substr($_REQUEST['selectedt'], 1));
if ($topicId == 0 || $topicId == null) {
  exit("<br><br>-- Error: Topic ID is not found!");
}

if (isset($_REQUEST['strs'], $_REQUEST['c_in_categ'], $_REQUEST['selectedmember'])) {
  $comments = $cdaViewObj->showCommentsOfSelectedCategThatContainStr((int)$topicId, $_REQUEST['strs'], $_REQUEST['c_in_categ'], $_REQUEST['selectedmember'], $ascDesc);
  $totalnumOfC = count($comments);
} else {
  $comments = [];
}

if ($comments == []) {
?>
  <p>All of <?php echo $totalnumOfC; ?> comments are displayed!</p>
<?php
  exit();
}

foreach ($comments as $PKey => $commentsValue) {
  $category = 'c';
  $tgpcr = $commentsValue;
  require "viewtgpcr.php";
}
?>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

if (!isset($cdaContrObj)) {
  $cdaContrObj = new CdaContr();
}

$totalnumOfG = 0;

if (isset($_REQUEST['offset'], $_REQUEST["numofgroups"], $_REQUEST["cbshowcomments"], $_REQUEST["numofcomments"], $_REQUEST['orderby'], $_REQUEST['ascdesc'])) {
  if (!isset($_REQUEST['selectedt'])) {
   exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }
  $offset = intval($_REQUEST['offset']);
  $numOfG = intval($_REQUEST['numofgroups']);
  $cbShowComments = $_REQUEST['cbshowcomments'];
  $numOfComments = intval($_REQUEST['numofcomments']);
  $orderBy = mb_strtoupper($_REQUEST['orderby']);
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfG = intval($_SESSION['numofgroups']);
  $cbShowComments = $_SESSION['cbshowcomments'];
  $numOfComments = intval($_SESSION['numofcomments']);
  $orderBy = $_SESSION['orderby'];
  $ascDesc = $_SESSION['ascdesc'];
}

$topicId = intval(substr($_REQUEST['selectedt'], 1));
if ($topicId == 0 || $topicId == null) {
  exit("<br><br>-- Error: Topic ID is not found!");
}

if (isset($_REQUEST['selectedgid'])) {  //When isset($_REQUEST['selectedgid']), it shows only selected group.
  $group = $cdaViewObj->showSelectedTgpcr('g', (int)$_REQUEST['selectedgid']);   //(int)"text" returns 0.
  $groups = [$group];
  $totalnumOfG = 1;
} elseif (isset($_REQUEST['viewGofselectedP'])) {
  $groups = $cdaViewObj->showGroupsOfSelectedProposals($_REQUEST['viewGofselectedP'], $orderBy, $ascDesc);
  $totalnumOfG = count($groups);
} elseif (isset($_REQUEST['viewg'], $_REQUEST['strs'], $_REQUEST['m'])) {
  // $_REQUEST['m'] is either 'currentmember' or 'allmembers'.
  $groups = $cdaViewObj->showGroupsThatTheirCommentsContainStr((int)$topicId, $_REQUEST['strs'], $_REQUEST['m'], $orderBy, $ascDesc);
  $totalnumOfG = count($groups);
} else {
  $groups = $cdaViewObj->showGroups((int)$topicId, (int)$offset, (int)$numOfG, $orderBy, $ascDesc);
  $totalnumOfG = $cdaViewObj->totalNumberOfGroups((int)$topicId);
}

if ($groups == []) {
?>
  <p>All of <?php echo $totalnumOfG; ?> groups are displayed!</p>
<?php
  exit();
}

foreach ($groups as $groupKey => $groupValue) {
  $category = 'g';
  $tgpcr = $groupValue;
  require "viewtgpcr.php";

  $proposals = $cdaViewObj->showProposalsOfGroup((int)$groupValue['id']);

  if (isset($proposals) || $proposals!=[]) {
    foreach ($proposals as $proposalsKey => $proposalValue) {
      $category = 'p';
      $tgpcr = $proposalValue;
      require "viewtgpcr.php";
      if ($cbShowComments == 'true' && isset($_REQUEST['selectedgid'])) {
        $comments = $cdaViewObj->showComments('3' , $proposalValue['id'], 0, (int)$numOfComments, $orderBy, $ascDesc); //! $orderBy will be used in the future if there will be voting for comments. Now it is set to comments_in_cda.id. !
        if (isset($comments) || $comments!=[]) {
          foreach ($comments as $commentKey => $commentValue) {
              $category = 'c';
              $tgpcr = $commentValue;
              require "viewtgpcr.php";
          }
        }
      }
    }
  }

  if ($cbShowComments == 'true') {
    ?>
    <p class="commentsofg_p">Comments of group g<?php echo $groupValue['id']; ?>:</p>
    <?php

    $comments = $cdaViewObj->showComments('2' , $groupValue['id'], 0, (int)$numOfComments, $orderBy, $ascDesc); //! $orderBy will be used in the future if there will be voting for comments. Now it is set to comments_in_cda.id. !
    if (isset($comments) || $comments!=[]) {
      foreach ($comments as $commentKey => $commentValue) {
          $category = 'c';
          $tgpcr = $commentValue;
          require "viewtgpcr.php";
      }
    }
  }
}
?>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

if (!isset($cdaContrObj)) {
  $cdaContrObj = new CdaContr();
}

if (!isset($tgpcrObj)) {
  $tgpcrObj = new Tgpcr();
}

$totalnumOfC = 0;

if (isset($_REQUEST['offset'], $_REQUEST["numofcomments"], $_REQUEST['orderby'], $_REQUEST['ascdesc'])) {
  if (!isset($_REQUEST['selected'])) {
   exit("<br><br><b>-- Error: ID is not found!<b>");
  }
  $offset = intval($_REQUEST['offset']);
  $numOfComments = intval($_REQUEST['numofcomments']);
  $orderBy = mb_strtoupper($_REQUEST['orderby']);
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfComments = intval($_SESSION['numofcomments']);
  $orderBy = $_SESSION['orderby'];
  $ascDesc = $_SESSION['ascdesc'];
}

$idOfSelected = intval(substr($_REQUEST['selected'], 1));
$categOfSelected = substr($_REQUEST['selected'], 0, 1);
if ($idOfSelected == 0 || $idOfSelected == null) {
  exit("<br><br>-- Error: ID is not found!");
}

$categoryNumber = $tgpcrObj->categNumInDb($categOfSelected);

$comments = $cdaViewObj->showComments($categoryNumber, $idOfSelected, $offset, (int)$numOfComments, $orderBy, $ascDesc);
//$orderBy for comments might be used in the future, if there will be voting system for comments.
$totalnumOfC = $cdaViewObj->totalNumberOfcommentsOfselectedCategId((int)$categoryNumber, (int)$idOfSelected);

$categMsgToShow = $tgpcrObj->categMsg($categOfSelected);

if ($comments == []) {
?>
  <p>All of <?php echo $totalnumOfC; ?> comments of <?php echo $categMsgToShow; ?> are displayed!</p>
<?php
  exit();
}

foreach ($comments as $commentsKey => $commentsValue) {
  $category = 'c';
  $tgpcr = $commentsValue;
  require "viewtgpcr.php";
}
?>
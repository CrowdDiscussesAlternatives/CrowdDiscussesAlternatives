<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
class Dbh
{
	private $host = "localhost";
	private $user = "root";
	private $pwd = "";
	private $dbName = "cda_v0_5";
	private $dbPdo;
	protected $lastId = 0;
  
  //For encryption/decryption.
	private $key = '50d11619c23b14011fb543111bca1178d112f52a5b422c23ae1958a7c6909327';
	private $cipher = "aes-256-cbc";  //aes-256-gcm

	protected function connect()
	{
		$sdn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbName;
		try {
		  $pdo = new PDO($sdn, $this->user, $this->pwd);
		} catch (Exception $e) {
			echo "Database connection error.";    //$e->getMessage();
			die();
		}
		$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		//$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 

		return $pdo;
	}

	protected function executeStmt($sql, $params)
	{ 
		$stmt =  $this->prepareStmt($sql);
		$checkExec = $stmt->execute($params);

		if (!$checkExec) {
			$this->forcedLogout();
			$usrlRedirect = "login.php?error=stmt-exec-error";
    	$this->checkFolderAndRedirect($usrlRedirect);
  	}

  	$this->lastId = $this->dbPdo->lastInsertId();

  	return $stmt;
	}

	protected function execStmtWithBindParam($sql, $params)
	{
		$stmt =  $this->prepareStmt($sql);
		$paramsArrayLength = count($params);
		for ($i = 0; $i < $paramsArrayLength; $i++) { 
			$stmt->bindParam(($i + 1) , $params[$i], PDO::PARAM_INT);
		}
		$checkExec = $stmt->execute();

		if (!$checkExec) {
			$this->forcedLogout();
			$usrlRedirect = "login.php?error=stmt-exec-error";
    	$this->checkFolderAndRedirect($usrlRedirect);
  	}

  	$this->lastId = $this->dbPdo->lastInsertId();

  	return $stmt;
	}

	protected function prepareStmt($sql)
	{
		$this->dbPdo = $this->connect();
		$stmt = $this->dbPdo->prepare($sql);
		if (!$stmt) {
    	$sqlerrorSelect = $this->dbPdo->errorInfo();
    	$this->forcedLogout();
    	$usrlRedirect = "login.php?error=stmt-prepare".$sqlerrorSelect[2];
    	$this->checkFolderAndRedirect($usrlRedirect);
  	}

  	return $stmt;
	}

	public function checkFolderAndRedirect($usrlRedirect)
	{
		if (strpos($_SERVER['PHP_SELF'], 'inc') !== false) {
    		header("Location: ../".$usrlRedirect);
  	    exit();
    } else {
    		header("Location: ".$usrlRedirect);
  	    exit();
    	}
	}

	public function forcedLogout() 
	{
		session_start();
    session_unset();
    session_destroy();
    setcookie("auth", "", (time()-3600), '/', "localhost", false, true);
	}

	protected function encryptData($data)
	{
		$ivlen = openssl_cipher_iv_length($this->cipher);
	  $iv = openssl_random_pseudo_bytes($ivlen);
	  $encryptedData = openssl_encrypt($data, $this->cipher, $this->key, 0, $iv) . '::' . $iv;
	  $encryptedData = base64_encode($encryptedData);

	  return $encryptedData;
	}

	protected function decryptData($encrypredData)
	{
		$encrypredData = base64_decode($encrypredData);
		$ivlen = openssl_cipher_iv_length($this->cipher);
		$iv = substr($encrypredData, -$ivlen, $ivlen);
		$lengthOfData = strlen($encrypredData);
		$encrypredData = substr($encrypredData, 0, $lengthOfData-$ivlen-2);
	  $decryptedData = openssl_decrypt($encrypredData, $this->cipher, $this->key, 0, $iv);

	  return $decryptedData;
	}
}

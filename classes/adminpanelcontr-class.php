<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
class AdminPanelContr extends AdministrationPanel
{
  public function checkPasswordForAdministratorPanel($passwordForAdministratorPanel)
  {
    $checkAdminPanelPassword = $this->checkAdminPanelPwd($passwordForAdministratorPanel);
    if (!$checkAdminPanelPassword) {
      header("Location: ../viewadminpanel.php?error=wrongpwd");
      exit();
    }
  }

  public function checkSecretCodeForSigningUp($secretCodeForSigningUp)
  {
    $checkSecretCodeForSigningUp = $this->checkSecretCodeForSignUp($secretCodeForSigningUp);
    if (!$checkSecretCodeForSigningUp) {
      header("Location: ../signup.php?error=wrongsecretcode");
      exit();
    }

    $limitAndCounter = $this->getLimitAndCounterOfSignupsPerSecretCode();
    if ($limitAndCounter["counter_of_signups_per_code"] >= $limitAndCounter["limit_of_signups_per_code"]) {
      header("Location: ../signup.php?error=limit");
      exit();
    }
  }

  public function changeModeForSigningUp($modeForSigningUp)
  {
    if (empty($modeForSigningUp) || $modeForSigningUp == null || $modeForSigningUp == "") {
      header("Location: ../viewadminpanel.php?error=nooptionselected");
      exit();
    }

    if ($modeForSigningUp != "disable_sign_up" && $modeForSigningUp != "enable_sign_up" && $modeForSigningUp != "enable_sign_up_with_secret_code") {
      header("Location: ../viewadminpanel.php?error=invalidoption");
      exit();
    }

    $this->setModeForSigningUp($modeForSigningUp);
    header("Location: ../viewadminpanel.php?update=changemodesuccessful");
    exit();
  }

  public function changeSecretCodeForSigningUp($secretCodeForSigningUp)
  {
    $lenghtOfSecretCodeForSigningUp = mb_strlen($secretCodeForSigningUp);

    if ($lenghtOfSecretCodeForSigningUp < 8 || $lenghtOfSecretCodeForSigningUp > 18) {
      header("Location: ../viewadminpanel.php?error=secretcodelength");
      exit();
    }

    $this->setSecretCodeForSigningUp($secretCodeForSigningUp);
    header("Location: ../viewadminpanel.php?update=secretcodesuccessful");
    exit();
  }

  public function checkModeForSigningUp()
  {
    $modeForSigningUp = $this->getModeForSigningUp();

    return $modeForSigningUp;
  }
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 class Tgpcr
 {
 	//Return the whole word of the selected category.
 	public function categMsg($category)
 	{
 		switch ($category) {
 			case 't':
 				$msgText = 'topic';
 				break;

 			case 'g':
 				$msgText = 'group';
 				break;

 			case 'p':
 				$msgText = 'proposal';
 				break;

 			case 'c':
 				$msgText = 'comment';
 				break;

 			case 'r':
 				$msgText = 'reference';
 				break;

 		  case 'q':
 				$msgText = 'requirement';
 				break;

 				case 's':
 				$msgText = 'similarity';
 				break;

 				case 'u':
 				$msgText = 'team-member';
 				break;

 				case 'm':
 				$msgText = 'message';
 				break;
 			
 			default:
 				$msgText = 'Non-existent category!';    //if changed, change it also in selected.php
 				break;
 		}

 		return $msgText;
 }

//Return the number that corresponds to name of the selected category.
 public function categNumInDb($category)
 {
    switch ($category) {
 			case 't':
 				$categoryNumInDb = 6;
 				break;

 			case 'g':
 				$categoryNumInDb = 2;
 				break;

 			case 'p':
 				$categoryNumInDb = 3;
 				break;

 			case 'c':
 				$categoryNumInDb = 4;
 				break;

 			case 'r':
 				$categoryNumInDb = 5;
 				break;

 			case 's':
 				$categoryNumInDb = 7;
 				break;

 		  case 'q':
 				$categoryNumInDb = 1;
 				break;
 			
 			default:
 				$categoryNumInDb = 0;
 				break;
 		}

 		return $categoryNumInDb;
 }
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 class AliasAuth extends Cda
 {
 	protected function getBallotBoxIdOfGroup($groupId)
  {
  	$sql = 'SELECT ballot_box_id
			        FROM groups_of_p
			        WHERE id = :groupid;';

		$params = array(':groupid' => $groupId);

		$stmt = $this->executeStmt($sql, $params);
		$result = $stmt->fetch();

		return $result;
  }

 	protected function checkIfAliasExists($alias, $ballotBoxId)
  {
  	$sql = 'SELECT id
			        FROM aliases_of_users
			        WHERE alias = :alias AND ballot_box_id = :ballotboxid;';

		$params = array(':ballotboxid' => $ballotBoxId, ':alias' => $alias);

		$stmt = $this->executeStmt($sql, $params);
		$result = $stmt->fetch();

	if (!($result === false || $result === null || $result['id'] === null)) {    //If there is no id, it returns false.
			return $result['id'];
		} else {
		  return null;
		}
  }

  protected function setAliasVoteAndHashNumber(int $ballotBoxId, int $userId, $vote, int $groupId, $alias, $hashNumber)
  {
  	$sql = 'INSERT INTO aliases_of_users (id, ballot_box_id, user_id, vote, group_id, alias, hash_number) 
		        VALUES (LAST_INSERT_ID(), :ballotBoxId, :userId, :vote, :groupId, :alias, :hashNumber);';
		
		$params = array(':ballotBoxId' => $ballotBoxId, ':userId' => $userId, ':vote' => $vote, ':groupId' => $groupId, ':alias' => $alias, ':hashNumber' => $hashNumber);

		$stmt = $this->executeStmt($sql, $params);
  }

  protected function getAliasVoteAndHashNumber(int $userId, int $groupId)
  {
  	$sql = 'SELECT id, ballot_box_id, vote, group_id, alias, hash_number
			        FROM aliases_of_users
			        WHERE user_id = :userId AND group_id = :groupId;';

		$params = array(':userId' => $userId, ':groupId' => $groupId);

		$stmt = $this->executeStmt($sql, $params);
		$results = $stmt->fetch();

	if (!($results === false || $results === null || $results['id'] === null)) {    //If there is no id, it returns false.
			return $results;
		} else {
		  return null;
		}
  }

  protected function getAllAliasesVotesAndHashNumbersForGroup(int $groupId)
  {
  	$sql = 'SELECT aliases_of_users.id, ballot_box_id, vote, group_id, alias, hash_number, user_name
			        FROM aliases_of_users
			        JOIN users_of_cda ON aliases_of_users.user_id = users_of_cda.id
			        WHERE group_id = ?
			        ORDER BY alias ASC;';

		$params = [$groupId];
		$stmt = $this->execStmtWithBindParam($sql, $params);
		$results = $stmt->fetchAll();
		return $results;
  }
/*
  protected function getAllUsersThatVotedForGroup(int $groupId)
  {
  	$sql = 'SELECT user_name
			        FROM aliases_of_users
			        JOIN users_of_cda ON aliases_of_users.user_id = users_of_cda.id
			        WHERE group_id = ?;';

		$params = [$groupId];
		$stmt = $this->execStmtWithBindParam($sql, $params);
		$results = $stmt->fetchAll();
		return $results;
  }
*/
}
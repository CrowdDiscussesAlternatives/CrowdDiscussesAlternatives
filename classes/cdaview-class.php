<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
class CdaView extends Cda
{
	public function rankMembersByUsersVoteOfRefs($topicId, $ascDesc)
	{
    $results = $this->rankMembersByUsersVoteOfReferences($_SESSION['userId'], (int)$topicId, $ascDesc);

    return $results; 
	}

	public function showAllRecipientsOfMessage($messageId, $ascDesc)
	{
    $results = $this->getAllRecipientsOfMessage($messageId, $ascDesc);

    return $results;
	}

	//Displays all topics ordered by date or by votes, ascending or descending.
  public function showAllTopics($orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc)
	{
		$results = $this->getAllTopics($orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc);

		return $results; //generator???
	}

	//Displays all groups of a topic, ascending or descending..
  public function showAllGroupsOfTopic($topicId, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc)
	{
		$results = $this->getAllGroupsOfTopic((int)$topicId, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc);

		return $results;
	}

	//Displays all team-members of a topic, in the order that they were inserted.
  public function showAllTeamMembersOfTopic(int $topicId)
	{
		$results = $this->getAllTeamMembersNames((int)$topicId, 'ASC');

		return $results;
	}

	//Displays selected number of topics (ordered by id or by votes, ascending or descending). When ordered by id, 
	//topics are displayed starting from the topic that is "offset" rows below the latest topic (when descending),
	//or the first topic (when ascending). Do not use offset other than zero when ordering by votes.
  public function showTopics($offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc)
	{
		/*
		if ($orderByDateOrVotesSumOrVotesNum == 'VOTES_SUM' || $orderByDateOrVotesSumOrVotesNum == 'VOTES_NUM') {
			$offset = 0;
		}
    */

    if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getTopics($offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc);

		return $results; //generator???
	}
  
  //Returns the total number of topics.
	public function totalNumberOfTopics()
	{
		$results = $this->countAllTopics();

		return $results;
	}

  //Displays selected number of groups (ordered by id or by votes, ascending or descending). When ordered by id, 
	//groups are displayed starting from the groups that is "offset" rows below the latest group (when descending),
	//or the first group (when ascending). Do not use offset other than zero when ordering by votes.
  public function showGroups($topicId, $offset, $numOfRows, $orderBy, $ascDesc)
	{
    if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getGroups((int)$topicId, (int)$offset, (int)$numOfRows, $orderBy, $ascDesc);

		return $results;
	}

	public function showGroupsOfSelectedProposals($selectedProposalsIdsAsString, $orderBy, $ascDesc)
	{
		if ($selectedProposalsIdsAsString == '') {
    	exit("<br><br>-- Error: ID not found. <b>Please select the proposals that the displayed groups must contain!</b>");
		}
    
		$groupIds = $this->findGroupsThatContainSelectedProposals($selectedProposalsIdsAsString);

		if ($groupIds == []) {
			return [];
		}

		$groupsIdsAsStr = $this->convertArrayOfIdsToString($groupIds, 'group_id', ',');
		$results = $this->getGroupsByTheirIds($groupsIdsAsStr, $orderBy, $ascDesc);
    
    return $results;
	}

	public function showGroupsThatTheirCommentsContainStr($topicId, $stringsToSearchFor, $Ofmembers, $orderBy, $ascDesc)
	{
		if ($stringsToSearchFor == '') {
    	exit("<br><br><b>-- Error: Cannot search for an empty string!</b>");
		}

		$groupsIds = $this->getCategIdsThatTheirCommentsContainStr((int)$topicId, $stringsToSearchFor, 'g', $Ofmembers, 'ASC');

		$groupsIdsAsStr = $this->convertArrayOfIdsToString($groupsIds, 'in_category_id', ',');

		$groups = $this->getGroupsByTheirIds($groupsIdsAsStr, $orderBy, $ascDesc);

		return $groups;
	}
  
  //Displays proposals. Proposals that are most recently commented, appear firstly.
	public function showLatestCommentedProposals($topicId, $numOfRows)
	{
		 if ($numOfRows == 0) {
    	return [];
    }
    
    $i = 0;
    $arrayOfProposalsIds = [];
    $arrayOfProposals = $this->findIdsOfLatestCommentedProposals((int)$topicId);
    foreach ($arrayOfProposals as $arrayOfProposalsKey => $arrayOfProposalsValue) {
    	$arrayOfProposalsIds[$i] = $arrayOfProposalsValue['in_category_id'];
    	++$i;
    }
    $arrayOfPIds = array_unique($arrayOfProposalsIds, SORT_NUMERIC);

    $countArrayOfPIds = count($arrayOfPIds);
    if ($countArrayOfPIds == 0) {
    	return [];
    }
      
    $i = 0;
    $results = [];  
    foreach ($arrayOfPIds as $arrayOfPIdsKey => $arrayOfPIdsValue) {
    	if ($i <= ($numOfRows - 1)) {
    		$results[$i] = $this->getSelectedTgpcr('p', $arrayOfPIdsValue);  //$arrayOfPIds[$i]: ID of proposal.
    	}
    	++$i;
    }

		return $results;
	}

	public function ShowProposalsExcluding2ndPsFromSimilaritiesThatUserVoted1($topicId, $orderBy, $ascDesc)
	{
    $results = $this->getProposalsExcluding2ndPsFromSimilaritiesThatUserVoted1((int)$topicId, $_SESSION['userId'], $orderBy, $ascDesc);

    return $results;
	}

  //Displays all proposals of selected team-members.
	public function showProposalsOfSelectedMembers(int $topicId, $membersIds, $orderBy, $ascDesc)
	{
		$membersIdsAsStr = '';

		foreach ($membersIds as $membersIdsKkey => $membersIdsValue) {
			$memberId = intval($membersIdsValue);
			if ($memberId >= 1) {
				$membersIdsAsStr .= ',' . $memberId;
			}
		}

		if ($membersIdsAsStr == '') {
			return [];
		}

		$membersIdsAsString = substr($membersIdsAsStr, 1);
		
		$results = $this->getProposalsOfSelectedMembers((int) $topicId, $membersIdsAsString, $orderBy, $ascDesc);

		return $results;
	}

	public function showProposalsOfMembersWithHighScoreInSelectedCateg(int $topicId, $selectedCategForScore, $numberOfMembers, $orderBy, $ascDesc)
	{
    $membersWithHighScore = $this->getMembersWithHighScoreInSelectedCateg((int)$topicId, $selectedCategForScore, $numberOfMembers);

    if ($membersWithHighScore == []) {
    	return [];
    }
    
    $i = 0;
    $membersIds = [];
    foreach ($membersWithHighScore as $membersWithHighScoreKey => $membersWithHighScoreValue) {
    	$membersIds[$i] = $membersWithHighScoreValue['member_id'];
    	++$i;
    }

    $proposals = $this->showProposalsOfSelectedMembers((int)$topicId, $membersIds, $orderBy, $ascDesc);
    
    return $proposals;
	}

	public function showProposalsThatTheirCommentsContainStr($topicId, $stringsToSearchFor, $Ofmembers, $orderBy, $ascDesc)
	{
		if ($stringsToSearchFor == '') {
    	exit("<br><br><b>-- Error: Cannot search for an empty string!</b>");
		}

		$proposalsIds = $this->getCategIdsThatTheirCommentsContainStr((int)$topicId, $stringsToSearchFor, 'p', $Ofmembers, 'ASC');

		$proposalsIdsAsStr = $this->convertArrayOfIdsToString($proposalsIds, 'in_category_id', ',');

		$proposals = $this->getProposalsByTheirIds($proposalsIdsAsStr, $orderBy, $ascDesc);

		return $proposals;
	}

	public function showProposalsWithSelectedIds($topicId, $selectedProposalsIdsAsString)
	{
    $selectedProposalsIds = explode(",", $selectedProposalsIdsAsString);

    $i = 0;
    $results = [];
    foreach ($selectedProposalsIds as $selectedProposalsIdsKey => $selectedProposalsIdsValue) {
    	$proposalId = intval($selectedProposalsIdsValue);
    	if ($proposalId >= 1) {
    		$temp = $this->getSelectedTgpcr('p', $proposalId);  //$results[$i]: ID of proposal.
    		if ($temp['topic_id'] == $topicId) {
    			$results[$i] = $temp;
    			++$i;
    		}
    	}
    }

    return $results;
	}

	public function showProposalsThatUserVoted1(int $topicId)
	{
		$results = $this->getProposalsThatUserVoted1((int)$topicId, $_SESSION['userId']);

		return $results;
	}

	public function showReceivedMessagesFromUser($offset, $numOfM, $ascDesc)
	{
		$results = $this->getReceivedMessagesFromUser((int)$offset, (int)$numOfM, $ascDesc);

		foreach ($results as $resultsKey => &$resultsValue) {
    	$resultsValue['message'] = $this->decryptData($resultsValue['message']);
    }

    unset($resultsValue);

		return $results;
	}

	//Displays selected number of references (ordered by id or by votes, ascending or descending). When ordered by id, 
	//references are displayed starting from the references that is "offset" rows below the latest references (when descending),
	//or the first references (when ascending). Do not use offset other than zero when ordering by votes.
  public function showReferences($offset, $numOfRows, $orderBy, $ascDesc)
	{
    if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getReferences(null, 0, (int)$offset, (int)$numOfRows, $orderBy, $ascDesc);

		return $results;
	}

	//Displays selected number of references in proposal (ordered by id or by votes, ascending or descending). When ordered by id, 
	//references are displayed starting from the references that is "offset" rows below the latest references (when descending),
	//or the first references (when ascending). Do not use offset other than zero when ordering by votes.
  public function showReferencesInProposal($proposalId, $offset, $numOfRows, $orderBy, $ascDesc)
	{
    if ($numOfRows == 0) {
    	return [];
    }
    
		$results = $this->getReferences('proposal', (int)$proposalId, (int)$offset,(int)$numOfRows, $orderBy, $ascDesc);

		return $results;
	}

	//Displays selected number of references of a topic (ordered by id or by votes, ascending or descending). When ordered by id, 
	//references are displayed starting from the references that is "offset" rows below the latest references (when descending),
	//or the first references (when ascending). Do not use offset other than zero when ordering by votes.
  public function showReferencesInTopic($topicId, $offset, $numOfRows, $orderBy, $ascDesc)
	{
    if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getReferences('topic', (int)$topicId, (int)$offset, (int)$numOfRows, $orderBy, $ascDesc);

		return $results;
	}

//Displays selected number of approved requirements (ordered by id or by votes, ascending or descending). When ordered by id, 
	//requirements are displayed starting from the topic that is "offset" rows below the latest topic (when descending),
	//or the first topic (when ascending). Do not use offset other than zero when ordering by votes.
  public function showApprovedRequirements($topicId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc)
	{
		/*
		if ($orderByDateOrVotesSumOrVotesNum == 'VOTES_SUM' || $orderByDateOrVotesSumOrVotesNum == 'VOTES_NUM') {
			$offset = 0;
		}
    */

    if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getApprovedRequirements($topicId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc);

		return $results; //generator???
	}

	//Displays selected number of requirements (ordered by id or by votes, ascending or descending). When ordered by id, 
	//requirements are displayed starting from the topic that is "offset" rows below the latest topic (when descending),
	//or the first topic (when ascending). Do not use offset other than zero when ordering by votes.
  public function showRequirements($topicId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc)
	{
		/*
		if ($orderByDateOrVotesSumOrVotesNum == 'VOTES_SUM' || $orderByDateOrVotesSumOrVotesNum == 'VOTES_NUM') {
			$offset = 0;
		}
    */

    if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getRequirements($topicId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc);

		return $results; //generator???
	}

	public function showSentMessagesFromUser($offset, $numOfM, $ascDesc)
	{
    $results = $this->getSentMessagesFromUser((int) $offset,(int) $numOfM, $ascDesc);

    foreach ($results as $resultsKey => &$resultsValue) {
    	$resultsValue['message'] = $this->decryptData($resultsValue['message']);
    }

    unset($resultsValue);

    return $results;
	}

	public function showSimilarities($topicId, $offset, $numOfRows, $orderBy, $ascDesc)
	{
		if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getSimilarities((int)$topicId, (int)$offset, (int)$numOfRows, $orderBy, $ascDesc);

		return $results;
	}

	public function showSimilaritiesReportedByCurrentMember($topicId, $orderBy, $ascDesc)
	{
    $results = $this->getSimilaritiesReportedByCurrentMember((int)$topicId, $orderBy, $ascDesc);

    return $results;
	}

	public function showSimilaritiesThatUserVoted1(int $topicId)
	{
		$results = $this->getSimilaritiesThatUserVoted1((int)$topicId, $_SESSION['userId']);

		return $results;
	}

	public function totalNumberOfcommentsOfselectedCategId(int $categOfParent, int $parentId)
	{
		$results = $this->countCommentsOfSelectedCategoryId((int)$categOfParent, (int)$parentId);

		return $results;
	}

	//Returns the total number of requirements in topic.
	public function totalNumberOfGroups($topicId)
	{
		$results = $this->countAllGroups($topicId);

		return $results;
	}
  
  //Returns the total number of references in CDA.
	public function totalNumberOfReferences()
	{
		$results = $this->countReferences(null, 0);

		return $results;
	}

	//Returns the total number of references of proposal.
	public function totalNumberOfReferencesInProposal($proposalId)
	{
		$results = $this->countReferences('proposal', (int)$proposalId);

		return $results;
	}

	//Returns the total number of references in topic.
	public function totalNumberOfReferencesInTopic($topicId)
	{
		$results = $this->countReferences('topic', (int)$topicId);

		return $results;
	}

  //Returns the total number of approved requirements in topic.
	public function totalNumberOfApprovedRequirements($topicId)
	{
		$results = $this->countAllApprovedRequirements($topicId);

		return $results;
	}

	//Returns the total number of pending requirements in topic.
	public function totalNumberOfPendingRequirements($topicId)
	{
		$results = $this->countAllPendingRequirements($topicId);

		return $results;
	}

  //Returns the total number of requirements in topic.
	public function totalNumberOfRequirements($topicId)
	{
		$results = $this->countAllRequirements($topicId);

		return $results;
	}

	//Returns the total number of similarities of proposals in topic.
	public function totalNumberOfSimilarities($topicId)
	{
		$results = $this->countAllSimilarities((int)$topicId);

		return $results;
	}

  //Returns the total number of team-members of a topic.
	public function totalNumberOfTeamMembers($topicId)
	{
		$results = $this->countAllTeamMembers((int)$topicId);

		return $results;
	}

  //Returns selected topic or group or proposal or comment or referece.
	public function showSelectedTgpcr($selectedCateg, int $selectedTgpcrID)
	{
		$results = $this->getSelectedTgpcr($selectedCateg, $selectedTgpcrID);

		return $results;
	}

	public function showSelectedTopicWithTimeTableInfo(int $topicId)
	{
		$results = $this->getSelectedTopicWithTimeTableInfo($topicId);

		return $results;
	}

	//TO DO: To be replaced by showSelectedTgpcr().topics and topics_info tables to be merged.
	public function showSelectedTopicInfo(int $topicId)
	{
		$results = $this->getSelectedTopicInfo($topicId);

		return $results;
	}

	//Displays selected number of proposals of selected topic (ordered by date or by votes, ascending or descending).
	// When ordered by date, proposals are displayed starting from the proposal that is "offset" rows below the latest 
	//proposal (when descending), or the first proposal (when ascending). Do not use offset other than zero when ordering 
	//by votes.
  public function showProposals($topicId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc)
	{
		 if ($numOfRows == 0) {
    	return [];
    }

		$results = $this->getProposals($topicId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc);

		return $results; //generator???
	}

  //Displays all the proposals of a group, in the order of the proposals id.
  public function showProposalsOfGroup($groupId)
	{
		$results = $this->getProposalsOfGroup((int)$groupId);

		return $results;
	}

	public function totalNumberOfReceivedMessagesFromUser()
	{
		$results = $this->countAllReceivedMessagesFromUser();

		return $results;
	}

	public function totalNumberOfSentMessagesFromUser()
	{
		$results = $this->countAllSentMessagesFromUser();

		return $results;
	}

	//Returns the total number of proposals in topic.
	public function totalNumberOfProposals($topicId)
	{
		$results = $this->countAllProposals($topicId);

		return $results;
	}

	public function showComments($categOfParent, $parentId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc)
	{
		if ($numOfRows == 0) {
    	return [];
    }
    
		$results = $this->getComments($categOfParent, $parentId, $offset, $numOfRows, $orderByDateOrVotesSumOrVotesNum, $sortByAscOrDesc);

		return $results; //generator???
	}

	public function showCommentsOfSelectedCategThatContainStr($topicId, $stringsToSearchFor, $searchInCateg, $Ofmembers, $ascDesc)
	{ 
		if ($stringsToSearchFor == '') {
    	exit("<br><br><b>-- Error: Cannot search for an empty string!</b>");
		}

		$results = $this->getCommentsOfSelectedCategThatContainStr((int)$topicId, $stringsToSearchFor, $searchInCateg, $Ofmembers, $ascDesc);

		if ($results == false || $results == null) {
			return [];
		}

		return $results;
	}

    //
	public function showNumberOfUnreadMessages()
	{
		$results = $this->getNumberOfUnreadMsgs($_SESSION['userId']);

		return $results;
	}
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 class AliasAuthContr extends AliasAuth
 {
 	protected function generateAliasAndSecretCode(/*$groupId, */$BallotBoxId)
  {
  	$numberOfCharForAlias = 8;
  	$numberOfCharForSecretCode = 10;
  /*
  	$ballotBoxId = $this->getBallotBoxIdOfGroup($groupId);

  	if ($ballotBoxId == false || $ballotBoxId == null) {
  		exit("--Error: Ballot Box ID does not exist.");
  	}
  */
  	for ($i=0; $i < 100; $i++) {
  		//This only creates characters of [a-f][0-9]. Number of characters = 2 * $bytes.
  		//$newAlias = bin2hex(random_bytes(4));
  	  $newAlias = bin2hex(openssl_random_pseudo_bytes($numberOfCharForAlias/2));
  	  $aliasId = $this->checkIfAliasExists($newAlias, $BallotBoxId);
  	  if ($aliasId === null) {
  	  	break;
  	  }
  	}
  	
    if ($aliasId !== null) {
  	  	exit("--Error: There was an error when generating the alias.");
  	  }

  	$newSecretCode = bin2hex(openssl_random_pseudo_bytes($numberOfCharForSecretCode/2));
  	
  	return ["alias"=>$newAlias, "secretCode"=>$newSecretCode];
  }

  Public function createVoteTableInfo($vote, $group)
  {
  	if ($group == 0) {
  		exit("--Error: Group cannot be found.");
  	}
    
  	$selectedCategory = 'g';    //Implemented only for groups at the moment.
  	//$group = $this->getSelectedTgpcr($selectedCategory, (int)$group['id']);
  	$aliasAndSecretCode = $this->generateAliasAndSecretCode(/*(int)$group['id'], */(int)$group['ballot_box_id']);
  	$hashNumber = hash("sha256", $aliasAndSecretCode['alias'] . $aliasAndSecretCode['secretCode']);
  	$this->setAliasVoteAndHashNumber($group['ballot_box_id'], $_SESSION["userId"], $vote, $group['id'], $aliasAndSecretCode['alias'], $hashNumber);

  	$sessionVarName = "voteforg" . $group['id'];
  	//$_SESSION[$sessionVarName] = ['topicid' => 't' . $group['topic_id'], 'groupname' => $group['group_name'], 'alias' => $aliasAndSecretCode['alias'], 'vote' => $vote, 'secretCode' => $aliasAndSecretCode['secretCode']];

  	$_SESSION[$sessionVarName] = ['topicid' => 't' . $group['topic_id'], 'groupname' => $group['group_name'], 'alias' => $aliasAndSecretCode['alias'], 'secretCode' => $aliasAndSecretCode['secretCode']];

  	//return ['t' . $group['topic_id'], $selectedCategory . $group['id'], $group['group_name'], $aliasAndSecretCode['alias'], $vote, $aliasAndSecretCode['secretCode']];
  }

  Public function checkHashNumber($alias, $secretCode, $providedHashNumber)
  {
  	$alias = trim($alias);
  	$secretCode = trim($secretCode);
  	$realHashNumber = hash("sha256", $alias . $secretCode);

  	if ($realHashNumber == $providedHashNumber) {
  		$result = "Correct: Hash number is correct.";
  	} else {
  		$result = "Error: Hash number is wrong!";
  	}

  	return [$alias, $secretCode, $providedHashNumber, $realHashNumber, $result];
  }
}
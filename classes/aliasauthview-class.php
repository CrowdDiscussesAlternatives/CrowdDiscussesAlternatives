<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
  class AliasAuthView extends AliasAuth
  {
  	public function showAllAliasesVotesAndHashNumbersForGroup($groupId)
  	{
  		$results = $this->getAllAliasesVotesAndHashNumbersForGroup((int)$groupId);

      return $results;
  	}
  /*
  	public function showAllUsersThatVotedForGroup($groupId)
  	{
  		$results = $this->getAllUsersThatVotedForGroup((int)$groupId);

      return $results;
  	}
  */
  	public function suffleResults($results)
  	{
  		$i = 0;
  		foreach ($results as $key => $value) {
  			$MembersThatVoted[$i] = $value['user_name'];
  			$i++;
  		}

      shuffle($MembersThatVoted);

    /*
  		$rand = new \Random\Randomizer();
  		$SuffledResults = $rand->shuffleArray($MembersThatVoted);
  	
      return $SuffledResults;
    */

      return $MembersThatVoted;
  	}

  	Public function checkIfEligibleForTempResults($topicId)
	  {
	  	$tgpcr = $this->getSelectedTopicWithTimeTableInfo((int)$topicId);
			
			$topicInitiatorName = $tgpcr['user_name'];

			if ($topicInitiatorName === null) {
		    exit("<br><br><b>--Error: Initiator not found, or topic not found.<b>");
		  } elseif ($topicInitiatorName !== $_SESSION["userUid"]) {
				exit("<br><br><b>--Please note that you are not the initiator of the topic in order to perform this action.");
		  }

		  if ($tgpcr["groups_state"] > 0) {
		  	exit("<br><br><b>--Temporary results of this voting event will be available when the fourth phase is closed.");
		  }
	  }
  }
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
class AdministrationPanel extends Dbh
{
	protected function checkAdminPanelPwd($pswForAdminPanel)
	{
		$sql = 'SELECT psw_of_panel FROM admin_panel;';
    
        $stmt = $this->executeStmt($sql, null);
        $results = $stmt->fetch();
        $pwdCheck = false;
        $pwdCheck = password_verify($pswForAdminPanel, $results["psw_of_panel"]);

        return $pwdCheck;
	}

    protected function checkSecretCodeForSignUp($secretCodeForSigningUp)
    {
        $sql = 'SELECT secret_code_for_sign_up FROM admin_panel;';
    
        $stmt = $this->executeStmt($sql, null);
        $results = $stmt->fetch();
        $pwdCheck = false;
        $pwdCheck = password_verify($secretCodeForSigningUp, $results["secret_code_for_sign_up"]);

        return $pwdCheck;
    }

    protected function getLimitAndCounterOfSignupsPerSecretCode()
    {
        $sql = 'SELECT limit_of_signups_per_code, counter_of_signups_per_code FROM admin_panel;';
    
        $stmt = $this->executeStmt($sql, null);
        $results = $stmt->fetch();
        
        return $results;
    }

    protected function getModeForSigningUp()
    {
        $sql = 'SELECT mode_of_sign_up FROM admin_panel;';
    
        $stmt = $this->executeStmt($sql, null);
        $results = $stmt->fetch();

        return $results["mode_of_sign_up"];
    }

    protected function setModeForSigningUp($modeForSigningUp)
    {
        $sql = 'UPDATE admin_panel 
                SET mode_of_sign_up = :modeForSigningUp
                WHERE id = 1;';
        
        $params = array(':modeForSigningUp' => $modeForSigningUp);

        $stmt = $this->executeStmt($sql, $params);
    }

    protected function setSecretCodeForSigningUp($secretCodeForSigningUp)
    {
        $hashedSecretCodeForSigningUp = password_hash($secretCodeForSigningUp, PASSWORD_DEFAULT);

        $sql = 'UPDATE admin_panel 
                SET secret_code_for_sign_up = :hashedSecretCodeForSigningUp
                WHERE id = 1;';
        
        $params = array(':hashedSecretCodeForSigningUp' => $hashedSecretCodeForSigningUp);

        $stmt = $this->executeStmt($sql, $params);
    }

    public function updateCounterOfSignupsPerSecretCode()
    {
        $sql = 'UPDATE admin_panel 
                SET counter_of_signups_per_code = counter_of_signups_per_code + 1 
                WHERE id = 1;';
        
        $stmt = $this->executeStmt($sql, null);
    }
}
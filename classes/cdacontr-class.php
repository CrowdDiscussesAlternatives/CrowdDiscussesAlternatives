<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
class CdaContr extends Cda 
{
	// Adds a references to a proposal or to a topic.
	public function addRef($referenceId, $proposalId, $topicId)
	{
		if (!isset($topicId)) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=noid");
	    exit();
	  }

	  $this->addReference((int)$referenceId, $proposalId, (int)$topicId);
	}

	//Checks if user is logged in (it checks session and authentication cookie).
	public function checkIfLoggedIn($urlRedirect)
	{
		if (!(isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth'])) {

			if ($urlRedirect == null) {
			  exit("<p><br><br><b>-- You are not logged in yet (or you are logged out).<br><br>" . 
            "-- Please notice that cookies must be allowed in order to login" . 
            " (only essential cookies for funcionality of the site are used).</b></p>");
			}

      header($urlRedirect);
			exit();
		}
	}

  //Checks if in a specified period of time the entered rows in the tables of the database are less than the limit.
	public function checkLimitsOfEnteredData($categoryName)
	{
		$SpecifiedDurationOfPeriod = 24*60*60;    //in seconds. If value changed, change it also in errorpage.js and signup.js (errorVar=entryrejected).
		//The limits apply for the specified period. They refer to the number of rows entered to each table (e.g. how many topics are created and entered as rows in the topics table).
		$limitOfEnteredUsers = 10;
		$limitOfEnteredTopics = 1;
		$limitOfEnteredRequirements = 50;
		$limitOfEnteredProposals = 50;
		$limitOfEnteredReferences = 100;
		$limitOfEnteredSimilarities = 50;
		$limitOfEnteredGroups = 50;
		$limitOfEnteredComments = 500;
		$limitOfenteredMessages = 50;

		$limitOfEnteredData = array('users' => $limitOfEnteredUsers, 'topics' => $limitOfEnteredTopics, 'requirements' => $limitOfEnteredRequirements, 'proposals' => $limitOfEnteredProposals, 'references' => $limitOfEnteredReferences, 'similarities' => $limitOfEnteredSimilarities, 'groups' => $limitOfEnteredGroups, 'comments' => $limitOfEnteredComments, 'messages' => $limitOfenteredMessages);

		if ($categoryName != 'users' && $categoryName != 'topics' && $categoryName != 'requirements' && $categoryName != 'proposals' && $categoryName != 'references' && $categoryName != 'similarities' && $categoryName != 'groups' && $categoryName != 'comments' && $categoryName != 'messages') {
  		header("Location: ../errorpage.php?error=wrongcategname");
	  	exit();
  	}

		$ratesOfEnteredData =  $this->ratesOfEnteredData();
		
		if ($ratesOfEnteredData['period_in_seconds'] > $SpecifiedDurationOfPeriod) {
			$idOfLastRow = $this->insertRowForEntryRatesOfData();
			$ratesOfEnteredData =  $this->ratesOfEnteredData();
		} else {
			$idOfLastRow = $ratesOfEnteredData['id'];
		}

		$namedKey = 'entries_of_' . $categoryName . '_allowed';

		if ($ratesOfEnteredData[$namedKey] < $limitOfEnteredData[$categoryName]) {
			$allowedOrRejected = 'allowed';
		} else {
			$allowedOrRejected = 'rejected';
		}

		$result = $this->updateRatesOfData($idOfLastRow, $allowedOrRejected, $categoryName);
    
    if ($result == 'error') {
    	header("Location: ../errorpage.php?error=wrongcategname");
	  	exit();
    }

    if ($allowedOrRejected == 'rejected') {
    	if ($categoryName == 'users') {
    		header("Location: ../signup.php?error=entryrejected");
	  	  exit();
    	} else {
    		header("Location: ../errorpage.php?error=entryrejected");
	  	  exit();
    	}
    }
	}
/*
	//Checks if username has been taken. If not signs up.
	public function checkUsernameSignup($username, $pwd)
	{
		$results = $this->getUsername($username);

		if ($results['user_name']) {
			header("Location: ../signup.php?error=usertaken");
  	  exit();
		} elseif (!$results['user_name']) {
			  $this->checkLimitsOfEnteredData('users');
			  $this->setUser($username, $pwd);
				header("Location: ../signup.php?signup=success");
				exit();
		}
	}
*/
	//Checks if username has been taken.
	public function checkUsername($username)
	{
		$results = $this->getUsername($username);

		if ($results['user_name']) {
			header("Location: ../signup.php?error=usertaken");
  	        exit();
		} 
	}

	public function signup($username, $pwd)
	{
	  $this->setUser($username, $pwd);
		header("Location: ../signup.php?signup=success");
		exit();
	}

  //Checks if inputs in form are empty or username has invalid characters/length or if repeat password and password do not match.
	public function chechSignupData($username, $password, $passwordRepeat)
	{
		$lenghtOfPassword = mb_strlen($password);

		if (empty($username) || empty($password) || empty($passwordRepeat)) {    //if variables are not empty text.
	  	header("Location: ../signup.php?error=emptyfields&uid=".$username);    //no html before header();.
	  	//mysqli_close($conn); ???
	  	exit();
	  } elseif (mb_strlen($username) > 20) {
	  	header("Location: ../signup.php?error=uidmaxlength");
	  	exit();
	  } elseif (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
	  	header("Location: ../signup.php?error=invaliduid");
	  	exit();
	  } elseif ($lenghtOfPassword < 6) {
	  	header("Location: ../signup.php?error=passwordminlength&uid=".$username);
	  	exit();
	  } elseif ($lenghtOfPassword > 20) {
	  	header("Location: ../signup.php?error=passwordmaxlength&uid=".$username);
	  	exit();
	  } elseif ($password!==$passwordRepeat) {
	  	header("Location: ../signup.php?error=passwordcheck&uid=".$username);
	  	exit();
	  }
	}

  //Checks if username and password in form are correct. If yes, it starts session and logins.
	public function checkUsernamePwdLogin($username, $password)
	{
		if (empty($username) || empty($password)) {
		header("Location: ../login.php?error=emptyfields");
	  exit();
	  }

	  $results = $this->getUserCheckPwd($username, $password);

	  if ($results['username'] == null) {
			header("Location: ../login.php?error=nouser");
      exit();
		}
    elseif ($results['pwdCheck'] == false) {
			header("Location: ../login.php?error=wrongpwd&uid=".$username);
      exit();
		}
		elseif ($results['pwdCheck'] == true) {
			session_start();
			$_SESSION["userId"] = $results['id'];
			$_SESSION["userUid"] = $results['username'];
			$CookieValue = $results['id'].time().random_int(1, 1000);
			$_SESSION["auth"] = $CookieValue;
			setcookie("auth", $CookieValue, 0, '/', "localhost", false, true);    //Set cookie BEFORE the <html> tag.
			header("Set-Cookie: auth=".$CookieValue."; path=/; domain=localhost; HttpOnly; SameSite=Strict");   //For SameSite=Strict.
			$_SESSION["numoftopics"] = 10;    //Toolbar.
			$_SESSION["numofgroups"] = 10;    //Toolbar.
			$_SESSION["numofproposals"] = 10;    //Toolbar.
			$_SESSION["numofreferences"] = 10;    //Toolbar.
			$_SESSION["numofcomments"] = 10;    //Toolbar.
			$_SESSION["numofsimilarities"] = 10;    //Toolbar.
			$_SESSION["cbshowproposals"] = 'true';    //Toolbar.
			$_SESSION["cbshowcomments"] = 'true';    //Toolbar.
			$_SESSION["cbshowreferences"] = 'true';    //Toolbar.
			$_SESSION["orderby"] = 'DATE';    //Toolbar.
			$_SESSION["ascdesc"] = 'DESC';    //Toolbar.
			header("Location: ../index.php?login=success");
      exit();
    }
    else {
    	header("Location: ../login.php?error=unknown");
      exit();
    }
	}

	Public function checkUsernamesOfRecipientsOfMsg($usernamesOfRecipients)
	{
		$i = 0;
		$arrayOfUsersIds = [];
    	$arrayOfUsernames = explode(',', $usernamesOfRecipients);

    	foreach ($arrayOfUsernames as $arrayOfUsernamesKey => $arrayOfUsernamesValue) {
	    	$usernameStr = $arrayOfUsernamesValue;
	    	$userNameAndId = $this->getUsername(trim($usernameStr));

	    	//Check if username is guest0001. This account is not allowed to create content or become a team-member. It is also not allowed to receive messages.
	    	if ($userNameAndId['user_name'] == "guest0001") {
	      	continue;
	    	}

	    	if ($userNameAndId['id'] == null || $userNameAndId['id'] == false) {
	    		header("Location: ../createnewmessage.php?error=noid&name=" . $usernameStr);
	        exit();
	    	}

	  		$arrayOfUsersIds[$i] = $userNameAndId['id'];
	  		++$i;
    	}

    	return $arrayOfUsersIds;
	}

  //Creates a ballot box for the new topic. Then it creates the new topic.
	Public function createTopic($topic, $userId)
	{
		/*
		if (mb_strlen($topic, 'utf8') > 300) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
      */
	  $topic = $this->replaceCharacters($topic);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 300;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($topic, $limitOfNumberOfChar);

	  $this->checkLimitsOfEnteredData('topics');

	    $ballotBoxId = $this->setBallotBox();
	    if ($ballotBoxId == null) {
	    	header("Location: ../createnewtopic.php?error=bbidnull");
	    	exit();
	    } elseif ($topic == "") {
	    	header("Location: ../createnewtopic.php?error=notopic");
	    	exit();
	    } else {
	        return $this->setTopic($topic, $userId, $ballotBoxId);
	    }
	}

	public function createGroup($selectedProposals, $userId, $groupTitle, $topicId, $selectedProposalsAsString)
	{
	/*
      if (mb_strlen($groupTitle, 'utf8') > 200) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
    */
	  $groupTitle = $this->replaceCharacters($groupTitle);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 200;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($groupTitle, $limitOfNumberOfChar);

		if ($groupTitle == "") {
		  header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=notitle");
		  exit();
		}

		if ($selectedProposalsAsString == "") {
		  header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=noproposals");
		  exit();
		}
    
    //Also checked in creategroup-inc.php! TO DO: erase one of the two checks.
    $checkIfGroupExists = $this->checkIfGroupExists($selectedProposalsAsString);
    if ($checkIfGroupExists != null) {
    	 header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=groupexists" . 
    	 	"&groupid=" . $checkIfGroupExists . "&topicid=" . $topicId);
		  exit();
    }

    $checkIfTitleExists = $this->checkIfGroupTitleExists($groupTitle);
    if ($checkIfTitleExists != null) {
    	 header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=titleexists");
		  exit();
    }

    $this->checkLimitsOfEnteredData('groups');

    $ballotBoxId = $this->setBallotBox();
    if ($ballotBoxId == null) {
    	header("Location: ../errorpage.php?error=bbidnull");
    	exit();
    }

    $this->setGroup((int)$userId, $groupTitle, (int)$ballotBoxId, (int)$topicId, $selectedProposalsAsString);
    $groupId = $this->checkIfGroupTitleExists($groupTitle);
    foreach ($selectedProposals as $selectedProposalsKey => $selectedProposalsValue) {
      $this->addProposalToGroup((int)$groupId, $selectedProposalsValue); //selectedProposalsValue: proposalId.
    }
	}

	public function createMessage($senderId, $newMessage, $replyToMessage)
	{
		/*
		if (mb_strlen($newMessage, 'utf8') > 300) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
	  */

	  $newMessage = $this->replaceCharacters($newMessage);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 300;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($newMessage, $limitOfNumberOfChar);

	  if ($newMessage == "") {
    		header("Location: ../errorpage.php?error=emptyfields");
    	  exit();
    }

    $this->checkLimitsOfEnteredData('messages');

		$messageId = $this->setMessage((int)$senderId, $newMessage, $replyToMessage);

		return $messageId;
	}

	//public function createRef($newReferenceDescription, $newReferenceUrl, $userId, $proposalId)
	public function createRef($newReferenceDescription, $newReferenceUrl, $userId, $proposalId, $topicId)
	{
		/*
		if (mb_strlen($newReferenceDescription, 'utf8') > 300) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
	  */

	  $newReferenceDescription = $this->replaceCharacters($newReferenceDescription);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 300;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($newReferenceDescription, $limitOfNumberOfChar);

	  if (mb_strlen($newReferenceUrl, 'utf8') > 2000) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }

	  $this->checkLimitsOfEnteredData('references');

	  $ballotBoxId = $this->setBallotBox();
    if ($ballotBoxId == null) {
    	header("Location: ../errorpage.php?error=bbidnull");
    	exit();
    } elseif ($newReferenceDescription == "") {
    	  header("Location: ../errorpage.php?error=emptyfields");
    	  exit();
    } elseif ($newReferenceUrl == "") {
    		header("Location: ../errorpage.php?error=emptyfields");
    	  exit();
    } else {
        $referenceId = $this->setReference($newReferenceDescription, $newReferenceUrl, $userId, $proposalId, $topicId, $ballotBoxId);
        return $referenceId;
    }
	}

	Public function createRequirement($requirement, $userId, $topicId)
	{
		/*
		if (mb_strlen($requirement, 'utf8') > 700) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
	  */

	  $requirement = $this->replaceCharacters($requirement);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 700;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($requirement, $limitOfNumberOfChar);

	  $this->checkLimitsOfEnteredData('requirements');

    $ballotBoxId = $this->setBallotBox();
    if ($ballotBoxId == null) {
    	header("Location: ../errorpage.php?error=bbidnull");
    	exit();
    } elseif ($requirement == "") {
    	header("Location: ../createnewrequirement.php?error=noq");
    	exit();
    } else {
        $this->setRequirement($requirement, $userId, $topicId, $ballotBoxId);
    }
	}

	Public function createProposal($proposal, $userId, $editingPhaseDuration, $topicId)
	{
		/*
		if (mb_strlen($proposal, 'utf8') > 700) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
	  */

	  $proposal = $this->replaceCharacters($proposal);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 700;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($proposal, $limitOfNumberOfChar);

	  $this->checkLimitsOfEnteredData('proposals');

    $ballotBoxId = $this->setBallotBox();
    if ($ballotBoxId == null) {
    	header("Location: ../createnewrequirement.php?error=bbidnull");
    	exit();
    } elseif ($proposal == "") {
    	header("Location: ../createnewproposal.php?error=nop");
    	exit();
    } else {
        $this->setProposal($proposal, $userId, $editingPhaseDuration, $topicId, $ballotBoxId);
    }
	}

	Public function createComment($comment, $userId, $categOfParent, $parentId, $topicId)
	{ 
		/*
		if (mb_strlen($comment, 'utf8') > 4000) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
	  */

	  $comment = $this->replaceCharacters($comment);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 4000;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($comment, $limitOfNumberOfChar);

    if ($comment == "") {
    	header("Location: ../createnewcomment.php?error=noc");
    	exit();
    } else {
    	  $this->checkLimitsOfEnteredData('comments');
        $this->setComment($comment, (int)$userId, (int)$categOfParent, (int)$parentId, (int)$topicId);
    }
	}

	Public function createSimilarity($userId, $proposalAId, $proposalBId, $topicId)
	{
		$this->checkLimitsOfEnteredData('similarities');

		$ballotBoxId = $this->setBallotBox();
    if ($ballotBoxId == null) {
    	header("Location: ../errorpage.php?error=bbidnull");
    	exit();
    } else {
        $this->setSimilarity((int)$userId, (int)$proposalAId, (int)$proposalBId, (int)$topicId, (int)$ballotBoxId);
    }
	}

	Public function editProposal($proposal, $editingPhaseDuration, $proposalId)
	{
		/*
		if (mb_strlen($proposal, 'utf8') > 700) {
	  	header("Location: ../errorpage.php?error=textmaxlength");
	  	exit();
	  }
	  */

	  $proposal = $this->replaceCharacters($proposal);    //Replaces < and > characters with &lt and &gt respectively, except in <br> HTML tag.
	  $limitOfNumberOfChar = 700;    //Due to the replacements the text might become bigger.
	  $this->checkNumberOfEnteredCharacters($proposal, $limitOfNumberOfChar);

    if ($proposal == "") {
      header("Location: ../editproposal.php?selected=p" . $proposalId . "&error=nop");
    	exit();
    } else {
        $this->updateProposal($proposal, $editingPhaseDuration, (int)$proposalId);
    }
	}

	Public function vote($ballotBoxId, $vote, $topicId, $categ)
	{
		if (!isset($ballotBoxId) || $ballotBoxId == 0) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=noballotbox");
	    exit();
	  }

		$userId = $_SESSION['userId'];

		$userVote = $this->checkIfUserHasVotedReturnVote($ballotBoxId, $userId);

		if ($userVote === null) {
			if (is_int($vote) && ($vote === 1 || $vote === 0 || $vote === -1)) {
			  $this->voteToBallotBoxId($ballotBoxId, $userId, $vote, $topicId, (int)$categ);
			} else {
        header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=invalidvote");
			  exit();
			}
		} else {
        header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=alreadyvoted");
			  exit();
		}
	}

	public function addNewMember($topicId, $memberName)
	{
		if (!isset($topicId) || $topicId == 0) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=notopic");
	    exit();
	  }

		$tgpcr = $this->getSelectedTgpcr('t', (int)$topicId);
		$topicInitiatorName = $tgpcr['user_name'];

		if ($topicInitiatorName === null) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=noinitiator");
	    exit();
	  } elseif ($topicInitiatorName !== $_SESSION["userUid"]) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../addnewmembers.php?selected=" . "t" . $topicId . "&error=initnotmatch");
	    exit();
	  }
    
    $newMember = $this->getUsername($memberName);

    if ($newMember === false || $newMember['id'] === null) {
    	//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../addnewmembers.php?selected=" . "t" . $topicId . "&error=invalidname");
	    exit();
    }

    $checkIfMember = $this->checkIfAlreadyMember((int)$topicId, (int)$newMember['id']);

    if ($checkIfMember != null) {
    	//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../addnewmembers.php?selected=" . "t" . $topicId . "&error=alreadymember&oldmember=" . $memberName);
	    exit();
    } else {
    	$this->addMember((int)$topicId, (int)$newMember['id']);
    }
	}

	public function checkIfMemberInTopic($topicId, $memberId)
	{
		if (isset($topicId) && $topicId != 0) {
      return $this->checkIfAlreadyMember((int)$topicId, $memberId);
		} else {
			return 0;  //It should redirect to errorpage.php error=notopic instead of returning 0.
		}
	}

	public function checkIfTExists($topic)
	{
		if (isset($topic)) {
      return $this->checkIfTopicExists($topic);
		} else {
			return null;
		}
	}

	public function checkIfQExists($requirement, $topicId)
	{
		if (isset($requirement, $topicId) && $topicId != 0) {
      return $this->checkIfRequirementExists($requirement, (int)$topicId);
		} else {
			return 0;
		}
	}

  public function checkIfPExists($proposal, $topicId)
	{
		if (isset($proposal, $topicId) && $topicId != 0) {
      return $this->checkIfProposalExists($proposal, (int)$topicId);
		} else {
			return 0;
		}
	}

	public function checkIfUsersReversedPairOfSimilarPsExists($topicId, $proposalAid, $proposalBid, $userId)
	{
		if (isset($topicId) && $topicId != 0) {
      return $this->checkIfUsersReversedPairOfSimilarProposalsExists((int)$topicId, (int)$proposalAid, (int)$proposalBid, (int)$userId);
		} else {
			exit("<br><br><b>-- Error: Topic ID not found!</b>");
		}
	}

	public function checkIfUserHasVotedForAReversedPairOfSimilarProposals($topicId, $proposalAid, $proposalBid)
	{
		if (isset($topicId) && $topicId != 0) {
      $checkIfRevPsExists = $this->checkIfReversedPairOfSimilarProposalsExists((int)$topicId, (int)$proposalAid, (int)$proposalBid);
		} else {
			exit("<br><br><b>-- Error: Topic ID not found!</b>");
		}

		if ($checkIfRevPsExists == null) {
			return null;
		}

		$vote = $this->checkIfUserHasVotedReturnVote($checkIfRevPsExists['ballot_box_id'], $_SESSION['userId']);

		if ($vote == 1) {
			return $checkIfRevPsExists['id'];
		} else {
      return null;
		}
	}

	public function checkIfRExists($newReferenceUrl)
	{
		if (isset($newReferenceUrl)) {
      return $this->checkIfReferenceExists($newReferenceUrl);
		} else {
			return 0;
		}
	}

	public function checkIfRExistsInP($newReferenceUrl, $proposalID)
	{
		if (isset($newReferenceUrl, $proposalID)) {
      return $this->checkIfReferenceExistsInProposal($newReferenceUrl, (int)$proposalID);
		} else {
			return 0;
		}
	}

	public function checkIfRExistsInT($newReferenceUrl, $topicID)
	{
		if (isset($newReferenceUrl, $topicID)) {
      return $this->checkIfReferenceExistsInTopic($newReferenceUrl, (int)$topicID);
		} else {
			return 0;
		}
	}

	public function checkIfSExists($proposalAid, $proposalBid)
	{
		if (isset($proposalAid, $proposalBid) && $proposalAid != 0 && $proposalBid != 0) {
      return $this->checkIfSimilarityExists((int)$proposalAid, (int)$proposalBid);
		} else {
			return 0;
		}
	}

	public function createUpdateTimeTableOfTopic($topicId, $membersInvitationPhaseDuration, $requirementsPhaseDuration, $proposalsPhaseDuration, $proposalsGroupsPhaseDuration)
	{
    if (!isset($topicId) || $topicId == 0) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=notopic");
	    exit();
	  } elseif ($membersInvitationPhaseDuration < 3 || $membersInvitationPhaseDuration > 30) { // 30 to become const $maxMembersInvitationPhaseDuration = 30, etc!
	  	 header("Location: ../timetableinput.php?selected=" . "t" . $topicId . "&error=invitationphaseoutoflimit");
	     exit();
	  } elseif ($requirementsPhaseDuration < 15 || $requirementsPhaseDuration > 200) {
	  	 header("Location: ../timetableinput.php?selected=" . "t" . $topicId . "&error=requirementsphaseoutoflimit");
	     exit();
	  } elseif ($proposalsPhaseDuration < 30 || $proposalsPhaseDuration > 500) {
	  	 header("Location: ../timetableinput.php?selected=" . "t" . $topicId . "&error=proposalsphaseoutoflimit");
	     exit();
	  } elseif ($proposalsGroupsPhaseDuration < 30 || $proposalsGroupsPhaseDuration > 500) {
	  	 header("Location: ../timetableinput.php?selected=" . "t" . $topicId . "&error=groupsphaseoutoflimit");
	     exit();
	  }

		$tgpcr = $this->getSelectedTopicWithTimeTableInfo((int)$topicId);
		
		$topicInitiatorName = $tgpcr['user_name'];

		if ($topicInitiatorName === null) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=noinitiator");
	    exit();
	  } elseif ($topicInitiatorName !== $_SESSION["userUid"]) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../timetableinput.php?selected=" . "t" . $topicId . "&error=initnotmatch");
	    exit();
	  }

	  if ($tgpcr["timetable_changed"] != '-1') {
	  	header("Location: ../timetableinput.php?selected=" . "t" . $topicId . "&error=update");
	    exit();
	  } //TO DO: timetable will be updated via polls.

	  $this->updateTimeTableOfTopic((int)$topicId, $membersInvitationPhaseDuration, $requirementsPhaseDuration, $proposalsPhaseDuration, $proposalsGroupsPhaseDuration, (intval($tgpcr["timetable_changed"]) + 1));
	}

	public function postponeCurrentPhase($topicId, $postoneEndOfCurrentPhaseInDays, $declaredPhase)
	{
		if (!isset($topicId) || $topicId == 0) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=notopic");
	    exit();
	  }

		if ($postoneEndOfCurrentPhaseInDays < 3) {
			header("Location: ../postponecurrentphase.php?selected=" . "t" . $topicId . "&error=minlimit");
	    exit();
		}

		if ($postoneEndOfCurrentPhaseInDays > 15) {
			header("Location: ../postponecurrentphase.php?selected=" . "t" . $topicId . "&error=maxlimit");
	    exit();
		}

    $topicInfo = $this->getSelectedTopicWithTimeTableInfo((int)$topicId);
		$topicInitiatorName = $topicInfo['user_name'];

		if ($topicInitiatorName === null) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=noinitiator");
	    exit();
	  } elseif ($topicInitiatorName !== $_SESSION["userUid"]) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../postponecurrentphase.php?selected=" . "t" . $topicId . "&error=initnotmatch");
	    exit();
	  }

	  if ($topicInfo['timetable_changed'] == -1) {
      header("Location: ../postponecurrentphase.php?selected=" . "t" . $topicId . "&error=notimetableyet");
	    exit();
    }

	  if ($topicInfo['inv_state'] > 0) {
	  	$currentPhase = 'inv_state';
	  	$currentEndOfPhase = $topicInfo['invitation_phase_closing_date'];
	  } elseif ($topicInfo['req_state'] > 0) {
	  	$currentPhase = 'req_state';
	  	$currentEndOfPhase = $topicInfo['requirements_phase_closing_date'];
	  } elseif ($topicInfo['prop_state'] > 0) {
	  	$currentPhase = 'prop_state';
	  	$currentEndOfPhase = $topicInfo['proposals_phase_closing_date'];
	  } elseif ($topicInfo['groups_state'] > 0) {
	  	$currentPhase = 'groups_state';
	  	$currentEndOfPhase = $topicInfo['groups_phase_closing_date'];
	  } else {
	  	header("Location: ../postponecurrentphase.php?selected=" . "t" . $topicId . "&error=phasesclosed");
	    exit();
	  }

	  if ($declaredPhase != $currentPhase) {
		  header("Location: ../postponecurrentphase.php?selected=" . "t" . $topicId . "&error=declaredphase");
	    exit();
	  }

	  $results = $this->postponeEndOfCurrentPhase((int) $topicId, $postoneEndOfCurrentPhaseInDays, $currentPhase);
	  if ($results == 'currentphaseerror') {
	  	header("Location: ../postponecurrentphase.php?selected=t" . $topicId . "&error=currentphaseerror");
      exit();
	  }
	  header("Location: ../postponecurrentphase.php?selected=t" . $topicId . "&update=success");
    exit();
	}

	public function recipientOfMsg($idOfRecipient, $msgId)
	{
		$this->recipientOfMessage((int)$idOfRecipient, (int)$msgId);
	}

	public function reviewStatusOfReq($approvalStatus, $requirementId, $topicId)
	{
		  $topicInfo = $this->getSelectedTopicInfo((int)$topicId);  // parentid = id of topic that requirement is attached.

		  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
		    header("Location: ../errorpage.php?error=notimetableyet");
		    exit();
		  } elseif ($topicInfo["req_state"] > 0) {  //requirement phase has not been closed.
		    header("Location: ../approverequirement.php?selected=q" . $requirementId . "&selectedt=t" . $topicId . "&error=reqphasenotyet");
		    exit();
		  }

			$tgpcr = $this->getSelectedTgpcr('t', (int)$topicId);
			$topicInitiatorName = $tgpcr['user_name'];

			if ($topicInitiatorName === null) {
				//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
		    header("Location: ../errorpage.php?error=noinitiator");
		    exit();
		  } elseif ($topicInitiatorName !== $_SESSION["userUid"]) {
				//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
		    header("Location: ../approverequirement.php?selected=q" . $requirementId . "&selectedt=t" . $topicId . "&error=initnotmatch");
		    exit();
		  }

		  $tgpcr = $this->getSelectedTgpcr('q', (int)$requirementId);

		  if ($tgpcr == 0) {
		  	header("Location: ../errorpage.php?error=notgpcr");
		    exit();
		  }

		  if ( $tgpcr["approval_status"] !== null) {
		  	header("Location: ../approverequirement.php?selected=q" . $requirementId . "&selectedt=t" . $topicId . "&error=reviewed");
		    exit();
		  }

			$this->updateApprovalStatus($approvalStatus, $requirementId, $topicId);
			header("Location: ../approverequirement.php?selected=q" . $requirementId . "&selectedt=t" . $topicId . "&input=success");
		  exit();
	}
  
  //TO DO: !!!table members_score to be merged with topics_members table!!!
	public function updateMembersScore($selectedCategory, $selectedId, $vote)
	{
		if ($vote == 0) {
			return;
		}

		$tgpcr = $this->getSelectedTgpcr($selectedCategory, (int)$selectedId);

		if ($tgpcr == false || $tgpcr['user_name'] == null) {
			return;
		}

		$authorsId = $tgpcr['user_id'];

		switch ($selectedCategory) {
			case 'r':
			  $this->updateScore((int)$tgpcr['topic_id'], (int)$authorsId, 'score_in_references', $vote);
				break;

			case 'p':
	  		$this->updateScore((int)$tgpcr['topic_id'], (int)$authorsId, 'score_in_proposals', $vote);
			  break;

			case 'g':
	  		$this->updateScore((int)$tgpcr['topic_id'], (int)$authorsId, 'score_in_groups', $vote);
			  break;

			case 's':
		  		$this->updateScore((int)$tgpcr['topic_id'], (int)$authorsId, 'score_in_similarities', $vote);
				  break;
			
			default:
				return;
				break;
		}
	}

	public function setSessionExpCookie()
	{ 
		setcookie("sessionexp", "check", time()+10000, '/', "localhost", false, true);    //Set cookie BEFORE the <html> tag.
		//header("Set-Cookie: sessionexp=check; path=/; domain=localhost; HttpOnly; SameSite=Strict");   //For SameSite=Strict.
	}

	public function checkSessionExpCookie()
	{
    if (!isset($_COOKIE['sessionexp'])) {
    	//session_start();
			session_unset();
      session_destroy();
      setcookie("auth", "", (time()-3600), '/', "localhost", false, true);
		}
	}

    //Changes the password of the users account.
	public function changePwd($password, $newPassword, $newPasswordRepeat)
	{
		if (empty($password) || empty($newPassword) || empty($newPasswordRepeat)) {
		header("Location: ../changepassword.php?error=emptyfields");
	  exit();
	  }

	  $username = $_SESSION["userUid"];

	  $results = $this->getUserCheckPwd($username, $password);

	  if ($results['pwdCheck'] == false) {
			header("Location: ../changepassword.php?error=wrongpwd&uid=".$username);
      exit();
	  }

	  $lenghtOfPassword = mb_strlen($newPassword);

	  if ($lenghtOfPassword < 6) {
	  	header("Location: ../changepassword.php?error=passwordminlength&uid=".$username);
	  	exit();
	  } elseif ($lenghtOfPassword > 20) {
	  	header("Location: ../changepassword.php?error=passwordmaxlength&uid=".$username);
	  	exit();
	  } elseif ($newPassword!==$newPasswordRepeat) {
	  	header("Location: ../changepassword.php?error=passwordcheck&uid=".$username);
	  	exit();
	  }

	  $this->updatePwd($username, $newPassword);
	  header("Location: ../changepassword.php?update=success&uid=".$username);
	  exit();
	}
  
    //
	public function resetCounterOfUnreadMessagesToZero()
	{
		$this->resetCounterOfUnreadMsgsToZero($_SESSION['userId']);
	}

	//
	public function increaseCounterOfUnreadMessages($idOfRecipient)
	{
		$this->increaseCounterOfUnreadMsgs((int)$idOfRecipient);
	}
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
    $groupId = $selectedTgpcrID;
    $sessionVarName = "voteforg" . $groupId;
    if (!isset($_SESSION[$sessionVarName])) {
    	exit("--Error: Alias and secret code could not be generated.");
    }
    $topicId = $_SESSION[$sessionVarName]['topicid'];
    $groupName = $_SESSION[$sessionVarName]['groupname'];
    $alias = $_SESSION[$sessionVarName]['alias'];
    //$vote = $_SESSION[$sessionVarName]['vote'];
    $secretCode = $_SESSION[$sessionVarName]['secretCode'];
  ?>
  <br>
  <div class="votetableinfo" id="votetableinfo">
  	<div class="votetableinfotitle" id="votetableinfotitle"><b>-- Information table --</b></div>
  	<ul>
  		<li>Topic ID: <?php echo $topicId; ?></li>
  		<li>Group ID: <?php echo $groupId; ?></li>
  		<li>Group: <?php echo $groupName; ?></li>
  		<li>Alias: <?php echo $alias; ?></li>
  		<li>Secret code: <?php echo $secretCode; ?></li>
  		<!--<li>Vote: <?php echo $vote; ?></li>-->
  		<li>Vote: <b>Please fill in this cell with what you have just voted (+1, 0 or -1) after copying/pasting this table.</b></li>
  		<li>Message: Thank you for voting. Please copy and keep this table somewhere safely in your device, in order to be able to validate your vote later.</li>
  	</ul>
  </div>
  <br>
  <?php
  unset($_SESSION[$sessionVarName]);
  ?>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

$totalnumOfM = 0;

if (isset($_REQUEST['offset'], $_REQUEST['ascdesc'])) {
  $offset = intval($_REQUEST['offset']);
  $numOfM = 20;
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfM = 20;
  $ascDesc = $_SESSION['ascdesc'];
}

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

$messages = $cdaViewObj->showSentMessagesFromUser((int)$offset, (int)$numOfM, $ascDesc);
$totalnumOfM = $cdaViewObj->totalNumberOfSentMessagesFromUser();

if ($messages == []) {
?>
  <p>All <!--of <?php echo $totalnumOfM; ?> -->of the personal messages that you have sent are displayed!</p>
<?php
  exit();
}

foreach ($messages as $messagesKey => $messagesValue) {
  $category = 'm';
  $tgpcr = $messagesValue;
  require "viewtgpcr.php";
}
?>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 $cdaViewObj = new CdaView();
 $cdaContrObj = new CdaContr();
 $cdaContrObj->setSessionExpCookie();
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
        require_once "viewtopicsnavbar.php";
        $numberOfUnreadMessages = $cdaViewObj->showNumberOfUnreadMessages();
        if ($numberOfUnreadMessages != 0) {
          ?>
            <p><a href="viewreceivedmessages.php" target="_blank">You have got <?php echo $numberOfUnreadMessages ?> new personal message(s).</a></p>
            <br>
          <?php
        }
    } else {
      //
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
      Please notice that cookies must be allowed in order to login (only essential cookies for functionality of the site are used).</p>

  <?php
    include 'mainpagetext.php';
	  }
	?>

</main>

<?php
  require "footer.php";
?>
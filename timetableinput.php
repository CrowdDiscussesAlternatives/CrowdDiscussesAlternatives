<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
      if (!isset($_REQUEST['selected'])) {
        exit("<br><br><b>-- Error: Topic ID is not found!<b>");
      }

      $selectedCateg = 'n';
      require_once "selectednavbar.php";

      $cdaViewObj = new CdaView();
      $tgpcrObj = new Tgpcr();

      $selectedCateg = substr($_REQUEST['selected'], 0, 1);
      $selectedTgpcrID = intval(substr($_REQUEST['selected'], 1));
      $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
      $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

      if ($selectedCategMsg == 'Non-existent category!') {
        exit('-- Error: Category sent to server does not exist!');
      } elseif ($selectedCategMsg != 'topic') {
        exit('-- Error: Category of topic (for time-table creation/update) was expected to be sent to server!');
      }
  ?>

      <p>Selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
  <?php
      require_once "headeroftable.php";

      $category = $selectedCateg;
      require "viewtgpcr.php";
  ?>

      <br>
      <label form="form_create">Specify time-table</label>
      <br>
      <form action="includes/timetableinput-inc.php" method="post" name="form_create" id="form_create">
        <div class="div_inputlabel_timetable">
        <label class="label_for" for="membersinvitationphaseduration">Specify duration of <b>team members invitation and references not in proposals yet</b> phase (in days):</label>
        <input class="input_time_table" type="number" id="membersinvitationphaseduration" name="membersinvitationphaseduration" title="Minimum 3 days, maximum 30 days." placeholder="Suggested: 10 days" required min="3" max="30">
        </div>
        <div class="div_inputlabel_timetable">
        <label class="label_for" for="requirementsphaseduration">Specify duration of <b>requirements and references not in proposals yet</b> phase (in days):</label>
        <input class="input_time_table" type="number" id="requirementsphaseduration" name="requirementsphaseduration" title="Minimum 15 days, maximum 200 days." placeholder="Suggested: 15 to 30 days" required min="15" max="200">
        </div>
         <div class="div_inputlabel_timetable">
        <label class="label_for" for="proposalsphaseduration">Specify duration of <b>proposals and references of proposals</b> phase (in days):</label>
        <input class="input_time_table" type="number" id="proposalsphaseduration" name="proposalsphaseduration" title="Minimum 30 days, maximum 500 days." placeholder="Suggested: 30 to 180 days" required min="30" max="500">
        </div>
        <div class="div_inputlabel_timetable">
        <label class="label_for" for="proposalsgroupsphaseduration">Specify duration of <b>proposals, references and groups of proposals</b> phase (in days):</label>
        <input class="input_time_table" type="number" id="proposalsgroupsphaseduration" name="proposalsgroupsphaseduration" title="Minimum 30 days, maximum 500 days." placeholder="Suggested: 30 to 180 days" required min="30" max="500">
        </div>
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedcategory" name="selectedcategory" value="<?php echo $selectedCateg; ?>">
          <input type="text" id="selectedid" name="selectedid" value="<?php echo $selectedTgpcrID; ?>">
        </div>
        <button type="submit" name="create_submit">OK</button>
      </form>

      <?php
      require "timetableinfotext.php";
      ?>

      <p id="p_create_urlvar"></p>

      <script type="module" src="./timetableinput.js"></script>
      <script type="text/javascript" src="./viewtgpcr.js"></script>
	<?php
      unset($cdaViewObj);
      unset($tgpcrObj);
    } else {
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
      Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>
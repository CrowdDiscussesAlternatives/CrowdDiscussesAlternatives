<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 $cdaViewObj = new CdaView();
 $cdaContrObj = new CdaContr();

 $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

 require "header.php";
?>

<main>

<?php
  if (!isset($_REQUEST['selectedt'])) {
    exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }

  $selectedCateg = 'n';
   require_once "selectednavbar.php";
?>

  <div id="div_toolbardata" style="display: none;">
    <div id="div_numofgroups"><?php echo $_SESSION["numofgroups"] ?></div>
    <div id="div_numofcomments"><?php echo $_SESSION["numofcomments"] ?></div>
    <div id="div_cbshowcomments"><?php echo $_SESSION["cbshowcomments"] ?></div>
    <div id="div_orderby"><?php echo $_SESSION["orderby"] ?></div>
    <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
    <div id="div_selectedt"><?php echo $_REQUEST['selectedt'] ?></div>
  </div>

  <?php
  if (isset($_REQUEST['viewGofselectedP'])) {
    ?>
      <p class="class_tip"><b>Below are displayed the groups that contain the selected proposals.</b></p>
    <?php
  }

  if (isset($_REQUEST['viewg'], $_REQUEST['strs'], $_REQUEST['m'])) {
    if ($_REQUEST['m'] == 'currentmember') {
        $msgM = 'you';
      } else {
        $msgM = 'all team-members';
      }
      $displayedTip = "Below are displayed the groups that some of their comments (created by " . $msgM . ") contain the specified word/phrase: \"" . $_REQUEST['strs'] . "\".";
    ?>
      <p class="class_tip"><b><?php echo $displayedTip; ?></b></p>
    <?php
  }
  ?>

  <p class="class_tip">To select a group you can click on its id.</p>

	<div class="tgpcrAll">
	<?php
    require "headeroftable.php";
  
    require "viewgroups_in2.php";

    if (!isset($_REQUEST['selectedgid']) && !isset($_REQUEST['viewGofselectedP']) && !isset($_REQUEST['viewg'])) {
      $checkRequestVars = true;
    } else {
      $checkRequestVars = false;
    }

    //When isset($_REQUEST['selectedgid']), it shows only selected group.
    if ($orderBy == 'DATE' && $checkRequestVars == true) {
      $categToShow = 'g';
      $tgpcrObj = new Tgpcr();
      $categMsgToShow = $tgpcrObj->categMsg($categToShow);
      $tgpcrInTotalToShow = $totalnumOfG;    //declared in viewgroups_in2.php
      unset($tgpcrObj);
      require 'btnshowmoretgpcr.php';
    } elseif ($orderBy != 'DATE' && $checkRequestVars == true) {
      ?>
        <p class="class_tip">Total number of groups in topic: <?php echo $totalnumOfG; ?></p>
      <?php
    }
  ?>
  </div>
  <br>

  <script type="text/javascript" src="./viewtgpcr.js"></script>
  <script type="text/javascript" src="./viewgroups.js"></script>

<?php
  unset($cdaViewObj);
  unset($cdaContrObj);
?>

</main>

<?php
  require "footer.php";
?>
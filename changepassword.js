/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, updateVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
updateVar = UrlVars.urlVar('update');
infoMessage = document.getElementById('p_changepassword_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'emptyfields') {
    infoMessage.innerHTML = "Please fill in all the fields";
  }
  else if (errorVar == 'wrongpwd') {
    infoMessage.innerHTML =  "The current password is wrong. Please try again.";
  }
  else if (errorVar == 'passwordminlength') {
    infoMessage.innerHTML =  "Length of new password must be at least 6 characters.";
  }
  else if (errorVar == 'passwordmaxlength') {
    infoMessage.innerHTML =  "Length of new password must be no more than 20 characters.";
  }
  else if (errorVar == 'passwordcheck') {
    infoMessage.innerHTML =  "Please write in \"Repeat new password\" field the same as in \"New password\" field.";
  }
  else if (errorVar == 'guest') {
    infoMessage.innerHTML =  "Please note that guest0001 cannot change password.";
  }
}

  if (updateVar != "") {
    if (updateVar == 'success') {
    infoMessage.innerHTML = "You have successfully changed the password.";
    }
  }
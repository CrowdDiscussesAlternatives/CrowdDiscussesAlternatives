<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 require "header.php";
?>

<main>

	<?php
    $cdaContrObj = new CdaContr();
    $cdaContrObj->checkIfLoggedIn(null);

    $cdaViewObj = new CdaView();
    $tgpcrObj = new Tgpcr();

    $selectedCateg = 'n';
    require_once "selectednavbar.php";
  ?>

    <br>
    <label form="form_create">Send a personal message</label>

    <form action="includes/createnewmessage-inc.php" method="post" name="form_create" id="form_create">
      <label class="label_for" for="sendto">To:</label>
      <textarea id="sendto" name="sendto" title="Write the usernames separated with commas." placeholder="Usernames (of recipients) separated with commas..." required></textarea>
      <textarea name="textarea_name" placeholder="Message..." maxlength="300" cols="30" required></textarea> <!-- wrap="hard" -->
      <button type="submit" name="create_submit">OK</button>
    </form>
    <br>

    <p id="p_create_urlvar"></p>

    <script type="module" src="./createnewmessage.js"></script>
    <script type="text/javascript" src="./viewtgpcr.js"></script>
	<?php
    unset($cdaViewObj);
    unset($tgpcrObj);
	?>

</main>

<?php
  require "footer.php";
?>
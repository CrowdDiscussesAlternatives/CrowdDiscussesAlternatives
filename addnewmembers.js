/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let addMemberUrlVars, errorVar, addMemberVar, newMemberVar, oldMemberVar, addMemberMessage;
addMemberUrlVars = new GetUrlVars(window.location.href);
errorVar = addMemberUrlVars.urlVar('error');
addMemberVar = addMemberUrlVars.urlVar('addmember');
newMemberVar = addMemberUrlVars.urlVar('newmember');
oldMemberVar = addMemberUrlVars.urlVar('oldmember');
addMemberMessage = document.getElementById('p_addnewmembers_urlvar');
addMemberMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'invalidname' || errorVar == 'noaddedmember') {
    addMemberMessage.innerHTML = "The name that you entered does not match with any username in Crowd Discusses Alternatives platform!";
  }
  else if (errorVar == 'alreadymember') {
    addMemberMessage.innerHTML =  oldMemberVar + " is already a member of this topic!";
  }
  else if (errorVar == 'nottopic') {
    addMemberMessage.innerHTML =  "Error: Category sent to server is not a topic.";
    document.getElementById('form_addnewmembers').style.display = 'none';
  }
  else if (errorVar == 'initnotmatch') {
    addMemberMessage.innerHTML =  "You are not the initiator of this topic. You can only add members to the topics that you have created.";
    document.getElementById('form_addnewmembers').style.display = 'none';
  } 
  else if (errorVar == 'notimetableyet') {
    addMemberMessage.innerHTML =  "Please notice that firstly you need to specify the time table of the topic, before you can add new members.";
    document.getElementById('form_addnewmembers').style.display = 'none';
  }
  else if (errorVar == 'propphasepassed') {
    addMemberMessage.innerHTML =  "You cannot add new members to the topic after the closing of the third phase.";
    document.getElementById('form_addnewmembers').style.display = 'none';
  }
} 
else if (addMemberVar != "") {
  if (addMemberVar == 'success') {
    addMemberMessage.innerHTML =  "You have successfully added " + newMemberVar + " as a new member in the topic.";
  }
}
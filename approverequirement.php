<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
		   $cdaViewObj = new CdaView();
		   $tgpcrObj = new Tgpcr();

       $selectedCateg = substr($_GET['selected'], 0, 1);  //Requirement.
       $selectedTgpcrID = intval(substr($_GET['selected'], 1));  //ID of requirement.
       $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
       $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

       $parentCateg = substr($_GET['selectedt'], 0, 1);  //Topic of Requirement.
       $parentID = intval(substr($_GET['selectedt'], 1));

       if ($selectedCategMsg == 'Non-existent category!') {
         exit('-- Error: Category sent to server does not exist!');
       } elseif ($selectedCategMsg != 'requirement') {
         exit('-- Error: Category of requirement was expected to be sent to server!');
       } elseif ($parentCateg != 't') {
         exit('-- Error: Category topic of requirement was expected to be sent to server!');
       }
  ?>
      <p>Selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
	<?php
	    require_once "headeroftable.php";

			$category = $selectedCateg;
			require "viewtgpcr.php";

      $tgpcr = $cdaViewObj->showSelectedTgpcr($parentCateg, (int)$parentID);
	?>
      <p>of topic:</p>
  <?php
      $category = $parentCateg;
      require "viewtgpcr.php";
  ?>
      <br>
      <label form="form_approve_reject">Approve or reject requirement of topic</label>

      <form action="includes/approverequirement-inc.php" method="post" name="form_approve_reject" id="form_approve_reject">
        <div class="div_radio">
            <input type="radio" id="approve" name="aprove_reject" value="1">
            <label for="approve">Approve requirement</label><br><br>
            <input type="radio" id="reject" name="aprove_reject" value="-1">
            <label for="reject">Reject requirement</label><br><br>
        </div>
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedcategory" name="selectedcategory" value="<?php echo $selectedCateg; ?>">
          <input type="text" id="selectedid" name="selectedid" value="<?php echo $selectedTgpcrID; ?>">
          <input type="text" id="parentcategory" name="parentcategory" value="<?php echo $parentCateg; ?>">
          <input type="text" id="parentid" name="parentid" value="<?php echo $parentID; ?>">
        </div>
        <button type="submit" name="aprove_reject_submit">Submit</button>
      </form>
      </div>
      
      <p id="p_review_urlvar"></p>
      <br>

      <script type="module" src="./approverequirement.js"></script>
      <script type="text/javascript" src="./viewtgpcr.js"></script>

  <?php
      unset($cdaViewObj);
      unset($tgpcrObj);
    } else {
      //redirect to login
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
     Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
    }
  ?>

</main>

<?php
  require "footer.php";
?>
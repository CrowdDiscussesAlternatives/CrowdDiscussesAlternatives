/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, updateVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
updateVar = UrlVars.urlVar('update');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'minlimit') {
    infoMessage.innerHTML = "You cannot postpone the end of the current phase for less than 3 days.";
  } 
  else if (errorVar == 'maxlimit') {
    infoMessage.innerHTML = "You cannot postpone the end of the current phase for more than 15 days.";
  }
  else if (errorVar == 'initnotmatch') {
    infoMessage.innerHTML = "You are not the initiator of this topic in order to be able to update its time-table.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'notimetableyet') {
    infoMessage.innerHTML = "Please note that you firstly need to specify the time-table of the topic before you can update it.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'phasesclosed') {
    infoMessage.innerHTML = "Please note that all the phases of the topic have been closed. You cannot update the time-table anymore.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'declaredphase') {
    infoMessage.innerHTML = "The current phase seems that has just been closed. Please check the time-table.";
  }
  else if (errorVar == 'currentphaseerror') {
    infoMessage.innerHTML = "An error occurred when the current phase was sent to the server! Please try again.";
  }
}
else if (updateVar != "") {
  if (updateVar == 'success') {
    infoMessage.innerHTML =  "You have successfully updated the time-table.";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('noteforlaterphases').style.display = 'none';
  }
}
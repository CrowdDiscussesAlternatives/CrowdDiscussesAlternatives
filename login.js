/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let LoginUrlVars, uidVar, errorVar, loginVar, loginMessage;
LoginUrlVars = new GetUrlVars(window.location.href);
uidVar = LoginUrlVars.urlVar('uid');

if (uidVar != "") {
    document.forms["form-login"]["uid"].value = uidVar;
}

errorVar = LoginUrlVars.urlVar('error');
loginVar = LoginUrlVars.urlVar('login');
loginMessage = document.getElementById('p-login-urlvar');

loginMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'emptyfields') {
    loginMessage.innerHTML = "Please fill in all the fields";
  }
  else if (errorVar == 'nouser') {
    loginMessage.innerHTML = "Wrong username. Please try again.";
  }
  else if (errorVar == 'wrongpwd') {
    loginMessage.innerHTML =  "Wrong password. Please try again.";
  }
  else if (errorVar == 'unknown') {
    loginMessage.innerHTML =  "Oops, something went wrong. Please reload the page.";
  }
   else if (errorVar == 'stmt-prepare' || errorVar == 'stmt-exec-error') {
    loginMessage.innerHTML =  "Oops, something went wrong! Please try again.";
  }
  else if (errorVar == 'notloggedin') {
    loginMessage.innerHTML =  " <p>You are not logged in yet (or you are logged out).<br><br>" + 
    "Please notice that cookies must be allowed in order to login" + 
    " (only essential cookies for funcionality of the site are used).</p>";
  }
}
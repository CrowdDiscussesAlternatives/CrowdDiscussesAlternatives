/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let SignupUrlVars, uidVar, errorVar, signupVar, SignupMessage;
SignupUrlVars = new GetUrlVars(window.location.href);
uidVar = SignupUrlVars.urlVar('uid');

if (uidVar != "") {
    document.forms["form-signup"]["uid"].value = uidVar;
}

errorVar = SignupUrlVars.urlVar('error');
signupVar = SignupUrlVars.urlVar('signup');
SignupMessage = document.getElementById('p_signup_urlvar');

SignupMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'emptyfields') {
    SignupMessage.innerHTML = "Please fill in all the fields";
  }
  else if (errorVar == 'invaliduid') {
    SignupMessage.innerHTML = "Invalid username. Length of username must be twenty or less characters." + 
    " Valid characters are small letters, capital letters and numbers.";
  }
  else if (errorVar == 'uidmaxlength') {
    SignupMessage.innerHTML =  "Length of username must be twenty or less characters.";
  }
  else if (errorVar == 'passwordminlength') {
    SignupMessage.innerHTML =  "Length of password must be at least 6 characters.";
  }
  else if (errorVar == 'passwordmaxlength') {
    SignupMessage.innerHTML =  "Length of password must be no more than 20 characters.";
  }
  else if (errorVar == 'passwordcheck') {
    SignupMessage.innerHTML =  "Please write in \"Repeat password\" field the same as in \"password\" field.";
  }
  else if (errorVar == 'usertaken') {
    SignupMessage.innerHTML =  "Please choose another username. This username has already been taken.";
  }
   else if (errorVar == 'stmt-prepare' || errorVar == 'stmt-exec-error') {
    SignupMessage.innerHTML =  "Oops, something went wrong! Please try again.";
  }
  else if (errorVar == 'entryrejected') {
    SignupMessage.innerHTML =  "Currently, the limit of the table in the database (for signing-up) has been reached. Please, try again after 1 day.";
  }
  else if (errorVar == 'wrongsecretcode') {
    SignupMessage.innerHTML =  "Error: The secret code for signing up is wrong or invalid.";
  }
  else if (errorVar == 'limit') {
    SignupMessage.innerHTML =  "Unfortunately, the limit of permitted sign-ups for this secret code has been reached.";
  }
} else if (signupVar != "") {
  if (signupVar == 'success') {
    SignupMessage.innerHTML =  "<b>You have successfully signed-up.</b>" 
    + "<br><br><u>Please, send a private message to admin (preferably right after your first login) that will include a 12-word secret recovery phrase that is not easily guessed</u> (for instance, Secret Phrase: The color of my car is the color of the Mediterranean sea), and keep this secret phrase safely in your computer (along with the email address below). Alternatively, you can send your email address to admin via a private message. "
    + "<br><br>If you forget your password, you can send an email at ...(at)....com (replacing (at) with @) witch shall include your username (and your unique secret phrase for verification, in case that you sent a secret phrase to admin). "
    + "An email will be sent back to you with a temporary password (that you can change after logging in). Please be patient with this process, I am tending to read my emails once every week. "
    + "<u><br><br>Finally, please notice that if you do not send a private message with a secret recovery phrase (or your email address), you will not be able to retrieve your account, in case you forget your password.</u><br><br>";
  }
}
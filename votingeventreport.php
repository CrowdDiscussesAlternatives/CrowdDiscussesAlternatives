<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
  session_start();    //session_start(); on the top of the code.
  require_once 'includes/autoloader-inc.php';
  //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded.
  $cdaContrObj = new CdaContr();
  $cdaViewObj = new CdaView();
  $aliasAuthViewObj = new AliasAuthView();

  $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

  require "header.php";
?>

<main>

<?php
  if (!isset($_REQUEST['selectedgid'])) {
    exit("<br><br><b>-- Error: Group ID is not found!<b>");
  }

  $groupId = intval($_GET['selectedgid']);
  $group = $cdaViewObj->showSelectedTgpcr('g', (int)$groupId);

  $aliasAuthViewObj->checkIfEligibleForTempResults((int)$group['topic_id']);

  $results = $aliasAuthViewObj->showAllAliasesVotesAndHashNumbersForGroup((int)$groupId);

  if ($results == [] || $results == null) {
      exit("<br><br><b>-- Please note that no one has voted for this group.<b>");
  }
?>
<br>
<div  class="temporaryresults">
  <h2>Temporary Results of group ID <?php echo $groupId ?></h2>
  <br>
  <div>
      <div>Topic ID: <?php echo $group['topic_id'] ?></div>
      <div>Group ID: <?php echo $groupId ?></div>
      <div>Group: <?php echo $group['group_name'] ?></div>
  </div>
  <br>
  <caption>First table of temporary results:</caption>
  <table>
    <tr>
      <th class="indexofresults">Index</th>
      <th class="aliasofresults">Alias</th>
      <th class="voteofresults">Vote</th>
      <th class="hashnumberofresultstitle">Hash-Number</th>
    </tr>
  </table>
<?php
  $counter = 0;
  $sumOfVotes = 0;
  foreach ($results as $key => $value) {
    $sumOfVotes = $sumOfVotes + $value['vote'];
    $counter++;
?>
  <table>
    <tr>
      <td class="indexofresults"><?php echo $counter ?></td>
      <td class="aliasofresults"><?php echo $value['alias'] ?></td>
      <td class="voteofresults"><?php echo $value['vote'] ?></td>
      <td class="hashnumberofresults"><?php echo $value['hash_number'] ?></td>
    </tr>
  </table>
<?php
  }

  //$results = $aliasAuthViewObj->showAllUsersThatVotedForGroup((int)$groupId);
  $suffledResults = $aliasAuthViewObj->suffleResults($results);
?>
  <table>
    <tr>
      <th class="indexofresults">Index</th>
      <th class="aliasofresults">Alias</th>
      <th class="voteofresults">Vote</th>
      <th class="hashnumberofresultstitle">Hash-Number</th>
    </tr>
  </table>
<br>
<caption>Second table of temporary results: Members that have voted in this voting event, <u>in random order</u>.</caption>
  <table>
    <tr>
      <th class="usernameofresults">User-Name</th>
    </tr>
  </table>
<?php
  foreach ($suffledResults as $key => $value) {
?>
  <table>
    <tr>
      <td class="usernameofresults"><?php echo $value ?></td>
    </tr>
  </table>
<?php
  }
?>
  <table>
    <tr>
      <th class="usernameofresults">User-Name</th>
    </tr>
  </table>
  <br>
  <div>
      <div>Topic ID: <?php echo $group['topic_id'] ?></div>
      <div>Group ID: <?php echo $groupId ?></div>
      <div>Group: <?php echo $group['group_name'] ?></div>
  </div>
  <div>Sum of Votes: <?php echo $sumOfVotes ?></div>
</div>
<br>
<br>
<?php
  unset($cdaContrObj);
  unset($cdaViewObj);
  unset($aliasAuthViewObj);
?>

</main>

<?php
  require "footer.php";
?>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
//session_cache_limiter('nocashe');    //Before session_start(); or use header("Cache-Control: no-cache, must-revalidate");
session_start();
require_once 'autoloader-inc.php';

$cdaContrObj = new CdaContr();
$cdaContrObj->checkIfLoggedIn(null);

	if (isset($_POST["selectedcategory"], $_POST["selectedid"], $_POST["parentcategory"], $_POST["parentid"])) {
		if (!isset($_POST["aprove_reject"])) {
		  header("Location: ../approverequirement.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&selectedt=" . $_POST["parentcategory"] . $_POST["parentid"] . "&error=noreview");
		  exit();
	  }

	  switch ($_POST["aprove_reject"]) {
			case '1':
				$aproveReject = 1;
				break;

				case '-1':
				$aproveReject = -1;
				break;
			
			default:
				$aproveReject = 'error';
				break;
		}

		if ($_POST["selectedcategory"] != 'q') {
	  	header("Location: ../errorpage.php?error=wrongcateg");
	  	exit();
	  } elseif ($_POST["selectedid"] == 0) {
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=noid");
	    exit();
	  }

	  if ($_POST["parentcategory"] != 't') {
	  	header("Location: ../errorpage.php?error=nottopic");
	  	exit();
	  } elseif ($_POST["parentid"] == 0) {  //Topic ID.
			//header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	    header("Location: ../errorpage.php?error=notopic");
	    exit();
	  }

	  $cdaContrObj->reviewStatusOfReq($aproveReject, (int)$_POST["selectedid"], (int)$_POST["parentid"]);
	}
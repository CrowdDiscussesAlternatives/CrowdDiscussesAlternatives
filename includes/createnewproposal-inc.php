<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();

if (isset($_POST['create_submit'], $_POST['selectedtid'], $_POST['editingphaseduration'])) {    //if all variables  in "array" exist and are not null.
	
  require_once 'autoloader-inc.php';

  $cdaContrObj = new CdaContr();
  $cdaViewObj = new CdaView();

  $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$_POST['selectedtid']);

  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
    header("Location: ../errorpage.php?error=notimetableyet");
    exit();
  } elseif ($topicInfo["req_state"] > 0) {  //current date is before requirements phase closing date.
    header("Location: ../createnewproposal.php?selectedt=t" . $_POST['selectedtid'] . "&error=propphasenotyet");
    exit();
  } elseif ($topicInfo["groups_state"] < 30*24*3600) {  //groups_state = (groups phase closing date - current date) in seconds.
    header("Location: ../createnewproposal.php?selectedt=t" . $_POST['selectedtid'] . "&error=propstop");
    exit();    //proposals creation stops 30days before groups phase closing date.
  }elseif (intval($_POST['editingphaseduration']) - 15 < 0) {
    header("Location: ../createnewproposal.php?selectedt=t" . $_POST['selectedtid'] . "&error=phasemin");
    exit();    //minimum of editing phase duration is 15 days.
  } elseif (($topicInfo["groups_state"] - 15*24*3600) < intval($_POST['editingphaseduration']*24*3600)) {
    header("Location: ../createnewproposal.php?selectedt=t" . $_POST['selectedtid'] . "&error=phasemax");
    exit();    //maximum of editing phase duration is 15 days before groups phase ends.
  }

  $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$_POST['selectedtid'], (int)$_SESSION["userId"]);
  if ($checkIfMember === 0) {
    unset($cdaContrObj);
    unset($cdaViewObj);
    header("Location: ../errorpage.php?error=notopic");
    exit();
  } elseif ($checkIfMember === null) {
     header("Location: ../errorpage.php?error=notmember");
     exit();
  }

	$newTextarea = $_POST['textarea_name'];

  $checkIfAlreadyP = $cdaContrObj->checkIfPExists($newTextarea, (int)$_POST['selectedtid']);

  if ($checkIfAlreadyP !== null) {
    header("Location: ../createnewproposal.php?selectedt=t" . $_POST['selectedtid'] . "&error=pexists");
    exit();
  } elseif ($checkIfAlreadyP === 0) {
    header("Location: ../errorpage.php?error=notopic");
    exit();
  }
 
  if ($checkIfAlreadyP === null) {    //If this proposal does not exist yet in topic.
      $cdaContrObj->createProposal($newTextarea, $_SESSION["userId"], $_POST['editingphaseduration'], (int)$_POST['selectedtid']);
  }
  unset($cdaContrObj);
  unset($cdaViewObj);
  
  header("Location: ../createnewproposal.php?selectedt=t" . $_POST['selectedtid'] . "&create=success");
  exit();
}
else{
	header("Location: ../errorpage.php?error=notopic");
	exit();
}
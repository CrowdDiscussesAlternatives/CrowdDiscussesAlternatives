<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2023 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();
require_once 'autoloader-inc.php';

$cdaContrObj = new CdaContr();
$cdaContrObj->checkIfLoggedIn(null);

if (!isset($_POST['create_submit'])) {
  header("Location: ../errorpage.php?error=emptyfields");
  exit();
}

if (!isset($_POST['reference_url'])) {
 if (isset($_POST['selectedpid'])) {
    header("Location: ../createaddreference.php?selectedpid=" . $_POST['selectedpid'] . "&error=emptyurlinput");
  } elseif (isset($_POST['selectedtid'])) {
    header("Location: ../createaddreference.php?selectedtid=" . $_POST['selectedtid'] . "&error=emptyurlinput");
  }
  exit();
}

//Check if username is guest0001. This account is not allowed to create content. It is only for viewing content.
if ($_SESSION["userUid"] == "guest0001") {
  header("Location: ../errorpage.php?error=usernameisguest0001");
  exit();
}

$newReferenceDescription = $_POST['ref_description'];
$newReferenceUrl = trim($_POST['reference_url'], " ");  //Removes whitespaces from the start and the end of the string.

$cdaViewObj = new CdaView();

if (isset($_POST['selectedpid'])) {
    $selectedCateg = 'p';
    $selectedProposalID = intval($_REQUEST['selectedpid']);
    $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedProposalID);
    if ($tgpcr == false || $tgpcr == null || $tgpcr["id"] == null) {
      header("Location: ../errorpage.php?error=noid");
      exit();
    }
    $selectedTopicID = $tgpcr['topic_id'];
    $writerOfProposal = $tgpcr['user_name'];
  } elseif (isset($_POST['selectedtid'])) {
    $selectedCateg = 't';
    $selectedTopicID = intval($_REQUEST['selectedtid']);
    $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTopicID);
    if ($tgpcr == false || $tgpcr == null || $tgpcr["id"] == null) {
      header("Location: ../errorpage.php?error=noid");
      exit();
    }
    $selectedProposalID = NULL;
  }

$topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$selectedTopicID);

if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
  header("Location: ../errorpage.php?error=notimetableyet");
  exit();
} elseif ($topicInfo["inv_state"] > 0) {  //current date is before members invitation phase closing date.
  if (isset($_POST['selectedpid'])) {
    header("Location: ../createaddreference.php?selectedpid=" . $_POST['selectedpid'] . "&error=reqhasenotyet");
  } elseif (isset($_POST['selectedtid'])) {
    header("Location: ../createaddreference.php?selectedtid=" . $_POST['selectedtid'] . "&error=reqhasenotyet");
  }
  exit();
} elseif ($topicInfo["groups_state"] < 0) {  //groups_state = (groups phase closing date - current date) in seconds.
  if (isset($_POST['selectedpid'])) {
    header("Location: ../createaddreference.php?selectedpid=" . $_POST['selectedpid'] . "&error=groupsphasestop");
  } elseif (isset($_POST['selectedtid'])) {
    header("Location: ../createaddreference.php?selectedtid=" . $_POST['selectedtid'] . "&error=groupsphasestop");
  }
  exit();
}

if (isset($_POST['selectedpid'])) {
  if ($writerOfProposal !== $_SESSION["userUid"]) {
    header("Location: ../createaddreference.php?selectedpid=" . $_POST['selectedpid'] . "&error=nowriterofp");
    exit();
  }

  $checkIfRAlreadyInP = $cdaContrObj->checkIfRExistsInP($newReferenceUrl, $selectedProposalID);
  if ($checkIfRAlreadyInP != null && $checkIfRAlreadyInP != 0) {
    header("Location: ../createaddreference.php?selectedpid=" . $_POST['selectedpid'] . "&error=alreadyinp");
    exit();
  }

  $checkIfRAlreadyInT = $cdaContrObj->checkIfRExistsInT($newReferenceUrl, (int)$selectedTopicID);
  if ($checkIfRAlreadyInT == null && $checkIfRAlreadyInT == 0) {
    $referenceId = $cdaContrObj->createRef($newReferenceDescription, $newReferenceUrl, $_SESSION["userId"], (int)$selectedProposalID, (int)$selectedTopicID);
    $cdaContrObj->addRef($referenceId, (int)$selectedProposalID, (int)$selectedTopicID);
    header("Location: ../createaddreference.php?selectedpid=" . $_POST['selectedpid'] . "&create=successinp");
    exit();
  } else {
    $cdaContrObj->addRef($checkIfRAlreadyInT, (int)$selectedProposalID, (int)$selectedTopicID);
    header("Location: ../createaddreference.php?selectedpid=" . $_POST['selectedpid'] . "&refid=" . $checkIfRAlreadyInT . "&create=refaddedtop");
    exit();
  }
}

if (isset($_POST['selectedtid'])) {
  $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$selectedTopicID, (int)$_SESSION["userId"]);
  if ($checkIfMember === 0) {
    header("Location: ../errorpage.php?error=notopic");
    exit();
  } elseif ($checkIfMember === null) {
     header("Location: ../errorpage.php?error=notmember");
     exit();
  }

  $checkIfRAlreadyInT = $cdaContrObj->checkIfRExistsInT($newReferenceUrl, (int)$selectedTopicID);
  if ($checkIfRAlreadyInT != null && $checkIfRAlreadyInT != 0) {
    header("Location: ../createaddreference.php?selectedtid=" . $_POST['selectedtid'] . "&error=alreadyint");
    exit();
  }

    $referenceId = $cdaContrObj->createRef($newReferenceDescription, $newReferenceUrl, $_SESSION["userId"], $selectedProposalID, (int)$selectedTopicID);
    $cdaContrObj->addRef($referenceId, $selectedProposalID, (int)$selectedTopicID);
    header("Location: ../createaddreference.php?selectedtid=" . $_POST['selectedtid'] . "&create=successint");
    exit();
}
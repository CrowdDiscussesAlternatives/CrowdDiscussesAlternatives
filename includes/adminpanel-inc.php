<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
//session_cache_limiter('nocashe');    //Before session_start(); or use header("Cache-Control: no-cache, must-revalidate");
session_start();
require_once 'autoloader-inc.php';

$adminPanelContrObj = new AdminPanelContr();
$cdaContrObj = new CdaContr();
$cdaContrObj->checkIfLoggedIn(null);

if ($_SESSION["userUid"] !== "admin") {
  header("Location: ../viewadminpanel.php?error=notadmin");
  exit();
}

if (!isset($_POST["pwd"])) {
  header("Location: ../viewadminpanel.php?error=nopwd");
  exit();
}

$adminPanelContrObj->checkPasswordForAdministratorPanel($_POST["pwd"]);

if (isset($_POST["modeforsignup_submit"])) {
  $adminPanelContrObj->changeModeForSigningUp($_POST["modeforsignup"]);
}
elseif (isset($_POST["secretcodeforsignup_submit"])) {
  $adminPanelContrObj->changeSecretCodeForSigningUp($_POST["secretCodeForSignup"]);
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();
require_once 'autoloader-inc.php';

$cdaContrObj = new CdaContr();
$cdaContrObj->checkIfLoggedIn(null);
$cdaViewObj = new CdaView();

if (!isset($_POST['create_submit'])) {
  header("Location: ../errorpage.php?error=emptyfields");
  exit();
}

$selectedProposals = explode(",", $_POST['selectedProposals']);
//Checks if ids are integers greater or equal to 1. If not, it redirects to error page.
$selectedProposals = $cdaContrObj->validateIds($selectedProposals, "Location: ../errorpage.php?error=novalidid");
sort($selectedProposals);  //Returns true on success or false on failure.

$selectedProposalsAsString =$cdaContrObj->convertArrayOfIdsToString($selectedProposals, null, ',');

if (!isset($_POST['group_title'])) {
  header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=notitle");
  exit();
}

if ($selectedProposals[0] == "") {
  header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=noproposals");
  exit();
}

$firstSelectedProposal = $cdaViewObj->showSelectedTgpcr("p", (int)$selectedProposals[0]);
$topicId = $firstSelectedProposal["topic_id"];
$checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$topicId, (int)$_SESSION["userId"]);
  if ($checkIfMember === 0) {
    unset($cdaContrObj);
    unset($cdaViewObj);
    unset($tgpcrObj);
    header("Location: ../errorpage.php?error=notopic");
    exit();
  } elseif ($checkIfMember === null) {
  	 unset($cdaContrObj);
     unset($cdaViewObj);
     unset($tgpcrObj);
     header("Location: ../errorpage.php?error=notmember");
     exit();
  }

  $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$topicId);

if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
    header("Location: ../errorpage.php?error=notimetableyet");
    exit();
  } elseif ($topicInfo["prop_state"] > 0) {  //current date is before proposals phase closing date.
    header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=groupsphasenotyet");
    exit();
  } elseif ($topicInfo["groups_state"] < 0) {  //groups_state = (groups phase closing date - current date) in seconds.
    header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=groupsstop");
    exit();    //Groups phase has been closed.
  }

	$groups = $cdaViewObj->showAllGroupsOfTopic((int)$topicId, "DATE", "ASC");

	if ($groups != []) {
  	foreach ($groups as $groupKey => $groupValue) {
      if ($groupValue["group_str"] == $selectedProposalsAsString) {
      	 header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&error=groupexists" . "&groupid=" . $groupValue["id"] . "&topicid=" . $topicId);
         exit(); 
      }
  	}
	}

  $cdaContrObj->createGroup($selectedProposals, $_SESSION["userId"], $_POST['group_title'],(int) $topicId, $selectedProposalsAsString);
  header("Location: ../createnewgroup.php?selectedProposals=" . $selectedProposalsAsString . "&create=success");
  exit(); 
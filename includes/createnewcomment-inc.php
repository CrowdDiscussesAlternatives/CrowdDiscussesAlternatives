<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();

if (isset($_POST['create_submit'], $_POST['selectedid'])) {    //if all variables  in "array" exist and are not null.
	
  require_once 'autoloader-inc.php';

  $cdaViewObj = new cdaView();
  $cdaContrObj = new CdaContr();
  $tgpcrObj = new Tgpcr();

  $tgpcr = $cdaViewObj->showSelectedTgpcr($_POST["selectedcategory"], (int)$_POST["selectedid"]);   //(int)"text" returns 0.
  $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
  if ($checkIfMember === 0) {
    unset($cdaContrObj);
    unset($cdaViewObj);
    unset($tgpcrObj);
    header("Location: ../errorpage.php?error=notopic");
    exit();
  } elseif ($checkIfMember === null) {
      unset($cdaContrObj);
      unset($cdaViewObj);
      unset($tgpcrObj);
      header("Location: ../errorpage.php?error=notmember");
      exit();
  }
  $topicId = $tgpcr['topic_id'];

  $selectedCategNum = $tgpcrObj->categNumInDb($_POST["selectedcategory"]);  //The unique number that corresponds to the category name.
  $newTextarea = $_POST['textarea_name'];
  
  $cdaContrObj->createComment($newTextarea, $_SESSION["userId"], (int)$selectedCategNum, (int)$_POST['selectedid'], $topicId);
  unset($cdaContrObj);
  unset($cdaViewObj);
  unset($tgpcrObj);
  
  header("Location: ../createnewcomment.php?selected=" . $_POST["selectedcategory"] . $_POST['selectedid'] . "&create=success");
  exit();
}
else{
	header("Location: ../createnewcomment.php");
	exit();
}
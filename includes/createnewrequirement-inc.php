<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();

if (isset($_POST['create_submit'], $_POST['selectedtid'])) {    //if all variables  in "array" exist and are not null.
	
  require_once 'autoloader-inc.php';

  $cdaContrObj = new CdaContr();
  $cdaViewObj = new CdaView();

  $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$_POST['selectedtid']);

  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
    header("Location: ../errorpage.php?error=notimetableyet");
    exit();
  } elseif ($topicInfo["inv_state"] > 0) {  //topic in invitation phase.
    header("Location: ../createnewrequirement.php?selectedt=t" . $_POST['selectedtid'] . "&error=reqphasenotyet");
    exit();
  } elseif ($topicInfo["req_state"] < 0) { //requirements phase has been closed.
    header("Location: ../createnewrequirement.php?selectedt=t" . $_POST['selectedtid'] . "&error=reqphasepassed");
    exit();
  }

  $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$_POST['selectedtid'], (int)$_SESSION["userId"]);
  if ($checkIfMember === 0) {
    unset($cdaContrObj);
    unset($cdaViewObj);
    header("Location: ../errorpage.php?error=notopic");
    exit();
  } elseif ($checkIfMember === null) {
     header("Location: ../errorpage.php?error=notmember");
     exit();
  }

	$newTextarea = $_POST['textarea_name'];

  $checkIfAlreadyQ = $cdaContrObj->checkIfQExists($newTextarea, (int)$_POST['selectedtid']);

  if ($checkIfAlreadyQ !== null) {
    header("Location: ../createnewrequirement.php?selectedt=t" . $_POST['selectedtid'] . "&error=qexists");
    exit();
  } elseif ($checkIfAlreadyQ === 0) {
    header("Location: ../errorpage.php?error=notopic");
    exit();
  }
 
  if ($checkIfAlreadyQ === null) {    //If this requirement does not exist yet in topic.
      $cdaContrObj->createRequirement($newTextarea, $_SESSION["userId"], (int)$_POST['selectedtid']);
  }
  unset($cdaContrObj);
  unset($cdaViewObj);
  
  header("Location: ../createnewrequirement.php?selectedt=t" . $_POST['selectedtid'] . "&create=success");
  exit();
}
else{
	header("Location: ../createnewrequirement.php");
	exit();
}
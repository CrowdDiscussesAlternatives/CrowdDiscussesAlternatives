<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();

if (isset($_POST['create_submit'], $_POST['selectedpid'])) {
  require_once 'autoloader-inc.php';

  $cdaContrObj = new CdaContr();
  $cdaViewObj = new CdaView();

  $tgpcr = $cdaViewObj->showSelectedTgpcr('p', (int)$_POST['selectedpid']);

  if ($tgpcr['edit_state'] < 1) {
    header("Location: ../editproposal.php?selected=p" . $_POST['selectedpid'] . "&error=votephase");
    exit();
  }

  if (!isset($_POST['editingphaseduration']) || $_POST['editingphaseduration'] == '') {
    $editingPhaseDuration = $tgpcr['seconds_from_edit_closing_date']/(24*3600);
  } else {
    $editingPhaseDuration = $_POST['editingphaseduration'];
  }

  $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$tgpcr['topic_id']);

  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
    header("Location: ../errorpage.php?error=notimetableyet");
    exit();
  } 

  $maxEditingPhaseDuration = ($topicInfo["groups_state"] - 15*24*3600)/(24*3600);
  $minEditingPhaseDuration = 15 - $tgpcr['seconds_from_date_time']/(24*3600);
  if ($minEditingPhaseDuration < 0) {
   $minEditingPhaseDuration = 0;
  }

  $newTextarea = $_POST['textarea_name'];

  if ($editingPhaseDuration < $minEditingPhaseDuration) {
    header("Location: ../editproposal.php?selected=p" . $_POST['selectedpid'] . "&error=phasemin");
    exit();    //minimum of editing phase duration is 15 days after creation of proposal.
  }

  if ($editingPhaseDuration > $maxEditingPhaseDuration) {
    header("Location: ../editproposal.php?selected=p" . $_POST['selectedpid'] . "&error=phasemax");
    exit();    //maximum of editing phase duration is 15 days before groups phase ends.
  }

  if ($tgpcr['user_id'] !== $_SESSION["userId"]) {
    header("Location: ../editproposal.php?selected=p" . $_POST['selectedpid'] . "&error=notauth");
    exit();
  }

  $checkIfAlreadyP = $cdaContrObj->checkIfPExists($newTextarea, (int)$tgpcr['topic_id']);

  if ($checkIfAlreadyP !== null) {
    header("Location: ../editproposal.php?selected=p" . $_POST['selectedpid'] . "&error=pexists");
    exit();
  } elseif ($checkIfAlreadyP === 0) {
    header("Location: ../errorpage.php?error=notopic");
    exit();
  }
 
  if ($checkIfAlreadyP === null) {    //If this proposal does not exist yet in topic.
      $cdaContrObj->editProposal($newTextarea, $editingPhaseDuration, (int)$_POST['selectedpid']);
  }
  unset($cdaContrObj);
  unset($cdaViewObj);
  
  header("Location: ../editproposal.php?selected=p" . $_POST['selectedpid'] . "&edit=success");
  exit();
}
else{
	header("Location: ../errorpage.php?error=noid");
	exit();
}
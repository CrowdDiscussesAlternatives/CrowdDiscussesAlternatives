<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2023 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
//session_cache_limiter('nocashe');    //Before session_start(); or use header("Cache-Control: no-cache, must-revalidate");
session_start();
require_once 'autoloader-inc.php';

if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
	if (isset($_POST["selectedcategory"], $_POST["selectedid"], $_POST["addmember_submit"])) {
		if (!isset($_POST["input_addmember"])) {
		  header("Location: ../addnewmembers.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=noaddedmember");
		  exit();
	  } elseif ($_POST["selectedcategory"] != 't') {    //!!!Check if category is topic, before addNewMember function!
	  	header("Location: ../addnewmembers.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=nottopic");
	    exit();
	  }

		$cdaContrObj = new CdaContr();
		$cdaViewObj = new CdaView();

		//Check if username is guest0001. This account is not allowed to create content or become a team-member. It is only for viewing content.
    if ($_POST["input_addmember"] == "guest0001") {
      header("Location: ../errorpage.php?error=usernameisguest0001");
      exit();
    }

	  $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$_POST['selectedid']);

	  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
      header("Location: ../addnewmembers.php?selected=t" . $_POST['selectedid'] . "&error=notimetableyet");
      exit();
    } elseif ($topicInfo["prop_state"] < 0) {
	    header("Location: ../addnewmembers.php?selected=t" . $_POST['selectedid'] . "&error=propphasepassed");
	    exit();
	  }
	
		$cdaContrObj->addNewMember((int)$_POST["selectedid"], $_POST["input_addmember"]);
		unset($cdaContrObj);
		unset($cdaViewObj);
		header("Location: ../addnewmembers.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&addmember=success&newmember=" . $_POST["input_addmember"]);
    exit();  
	}
}
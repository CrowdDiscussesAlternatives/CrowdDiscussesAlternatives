<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();
require_once 'autoloader-inc.php';

$cdaContrObj = new CdaContr();
$cdaContrObj->checkIfLoggedIn(null);
$cdaViewObj = new CdaView();

if (!isset($_POST['create_submit'])) {
  header("Location: ../errorpage.php?error=emptyfields");
  exit();
}

if (!isset($_POST['selectedProposals'], $_POST['selectedProposalAid'])) {
  header("Location: ../errorpage.php?error=emptyfields");
  exit();
}

$selectedProposalsAsString = $_POST['selectedProposals'];
$selectedProposalAid = intval($_POST['selectedProposalAid']);  //Most well written proposal from the similar proposals, according to the member that reports the similary.
if ($selectedProposalAid == 0) {
  header("Location: ../createnewsimilarity.php?selectedProposals=" . $selectedProposalsAsString . "&error=nopaid");
  exit();
}

$selectedProposals = explode(",", $_POST['selectedProposals']);
$selectedProposals = $cdaContrObj->validateIds($selectedProposals, "Location: ../errorpage.php?error=novalidid");
$selectedProposalsAsString =$cdaContrObj->convertArrayOfIdsToString($selectedProposals, null, ',');

if (count($selectedProposals) < 2) {
  header("Location: ../createnewsimilarity.php?selectedProposals=" . $selectedProposalsAsString . "&error=fewproposals");
  exit();
}

$firstSelectedProposal = $cdaViewObj->showSelectedTgpcr("p", (int)$selectedProposals[0]);
$topicId = $firstSelectedProposal["topic_id"];
$checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$topicId, (int)$_SESSION["userId"]);
  if ($checkIfMember === 0) {
    unset($cdaContrObj);
    unset($cdaViewObj);
    unset($tgpcrObj);
    header("Location: ../errorpage.php?error=notopic");
    exit();
  } elseif ($checkIfMember === null) {
  	 unset($cdaContrObj);
     unset($cdaViewObj);
     unset($tgpcrObj);
     header("Location: ../errorpage.php?error=notmember");
     exit();
  }

  $i = 0;
  $newSimilarities = [];
  foreach ($selectedProposals as $selectedProposalkey => $selectedProposalvalue) {
    if ($selectedProposalvalue == 0) {
      header("Location: ../errorpage.php?error=noid");
      exit();
    }
    if ($selectedProposalAid != $selectedProposalvalue) {
      $newSimilarities[$i]["pA"] = $selectedProposalAid;
      $newSimilarities[$i]["pB"] = $selectedProposalvalue;
      ++$i;
    }
  }

  $i = 0;
  $existingSimilarities = [];
  $existingUsersReversedSimilarities = [];
  $numberOfNewlyCreatedSimilarities = 0;
  foreach ($newSimilarities as $newSimilaritykey => $newSimilarityvalue) {
    $checkIfSimilarityExists = $cdaContrObj->checkIfSExists($newSimilarityvalue["pA"], $newSimilarityvalue["pB"]);
    $checkIfUsersReversedSimilarityExists = $cdaContrObj->checkIfUsersReversedPairOfSimilarPsExists($topicId, $newSimilarityvalue["pA"], $newSimilarityvalue["pB"], $_SESSION["userId"]);

    if ($checkIfSimilarityExists == null && $checkIfUsersReversedSimilarityExists == null) {
     $cdaContrObj->createSimilarity($_SESSION["userId"], $newSimilarityvalue["pA"], $newSimilarityvalue["pB"], (int)$topicId);
     ++$numberOfNewlyCreatedSimilarities;
    } elseif ($checkIfSimilarityExists != 0) {
     $existingSimilarities[$i] = $checkIfSimilarityExists;
    } elseif ($checkIfUsersReversedSimilarityExists != 0) {
     $existingUsersReversedSimilarities[$i] = $checkIfUsersReversedSimilarityExists;
    }
    ++$i;
  }

  $existingSimilaritiesAsString = $cdaContrObj->convertArrayOfIdsToString($existingSimilarities, null, ',');
  $existingUsersReversedSimilaritiesAsString = $cdaContrObj->convertArrayOfIdsToString($existingUsersReversedSimilarities, null, ',');

  header("Location: ../createnewsimilarity.php?selectedProposals=" . $selectedProposalsAsString . "&existings=" . $existingSimilaritiesAsString . "&create=success&newSNum=" . $numberOfNewlyCreatedSimilarities . "&existingsrev=" . $existingUsersReversedSimilaritiesAsString);
  exit();
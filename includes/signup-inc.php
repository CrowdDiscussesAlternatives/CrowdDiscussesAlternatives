<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
require_once 'autoloader-inc.php';

$adminPanelContrObj = new AdminPanelContr();

$modeForSigningUp = $adminPanelContrObj->checkModeForSigningUp();

if (empty($modeForSigningUp) || $modeForSigningUp == null) {
    exit("<p><br><br><b>-- Error: mode for signing up is not set.</b></p>");
}

if ($modeForSigningUp == "disable_sign_up") {
    exit("<p><br><br><b>-- Please note that currently, the ability of signing up is active only for people that I know and friends of mine. You can still login as a guest, if you want to see the content of this platform.</b></p>");    //Signup is temporarily unavailable.
}

if ($modeForSigningUp == "enable_sign_up_with_secret_code") {
    $adminPanelContrObj->checkSecretCodeForSigningUp($_POST['secretCodeForSignup']);
}

if (isset($_POST['signup-submit'])) {    //if all variables  in "array" exist and are not null.
	
  //require_once 'autoloader-inc.php';

	$username = $_POST['uid'];
	$password = $_POST['pwd'];
	$passwordRepeat = $_POST['pwd-repeat'];

  $cdaContrObj = new CdaContr();

  $cdaContrObj->chechSignupData($username, $password, $passwordRepeat);
  $cdaContrObj->checkUsername($username);
  $cdaContrObj->checkLimitsOfEnteredData('users');

  if ($modeForSigningUp == "enable_sign_up_with_secret_code") {
      $adminPanelContrObj->updateCounterOfSignupsPerSecretCode();
  }
  
  $cdaContrObj->signup($username, $password);
  //$cdaContrObj->checkUsernameSignup($username, $password);

  unset($cdaContrObj);
  unset($adminPanelContrObj);
}
else{
	header("Location: ../signup.php");
	exit();
}
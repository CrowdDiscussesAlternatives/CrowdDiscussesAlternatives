<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();

if (isset($_POST['create_submit'], $_POST['sendto'])) {    //if all variables  in "array" exist and are not null.
	
  require_once 'autoloader-inc.php';

  $cdaContrObj = new CdaContr();
  $cdaViewObj = new CdaView();

  //Check if username is guest0001. This account is not allowed to create content. It is only for viewing content.
  if ($_SESSION["userUid"] == "guest0001") {
    header("Location: ../errorpage.php?error=usernameisguest0001");
    exit();
  }

  $idsOfRecipients = $cdaContrObj->checkUsernamesOfRecipientsOfMsg($_POST['sendto']);

	$newTextarea = $_POST['textarea_name'];

  $msgId = $cdaContrObj->createMessage($_SESSION["userId"], $newTextarea, null);
  
  foreach ($idsOfRecipients as $idsOfRecipientsKey => $idsOfRecipientsValue) {
    $cdaContrObj->recipientOfMsg($idsOfRecipientsValue, $msgId);
    $cdaContrObj->increaseCounterOfUnreadMessages($idsOfRecipientsValue);
  }

  unset($cdaContrObj);
  unset($cdaViewObj);
 
  header("Location: ../createnewmessage.php?create=success");
  exit();
}
else{
	header("Location: ../errorpage.php?error=emptyfields");
	exit();
}
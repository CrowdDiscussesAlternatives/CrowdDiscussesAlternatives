<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
session_start();

if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
  if (isset($_POST["selectedcategory"], $_POST["selectedid"], $_POST["create_submit"])) {
    if (!isset($_POST["membersinvitationphaseduration"], $_POST["requirementsphaseduration"], $_POST["proposalsphaseduration"], $_POST["proposalsgroupsphaseduration"])) {
      header("Location: ../timetableinput.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=missingentries");
      exit();
    } elseif ($_POST["selectedcategory"] != 't') {    //!!!Check if category is topic!
      header("Location: ../timetableinput.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=nottopic");
      exit();
    }
	
    require_once 'autoloader-inc.php';

    $cdaContrObj = new CdaContr();

    $cdaContrObj->createUpdateTimeTableOfTopic((int)$_POST["selectedid"], (int)$_POST["membersinvitationphaseduration"], (int)$_POST["requirementsphaseduration"], (int)$_POST["proposalsphaseduration"], (int)$_POST["proposalsgroupsphaseduration"]);
   
    unset($cdaContrObj);
    unset($cdaViewObj);
    
    header("Location: ../timetableinput.php?selected=" . $_POST["selectedcategory"] . $_POST['selectedid'] . "&input=success");
    exit();
  }
}
else{
	header("Location: ../timetableinput.php");
	exit();
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
//session_cache_limiter('nocashe');    //Before session_start(); or use header("Cache-Control: no-cache, must-revalidate");
session_start();
require_once 'autoloader-inc.php';

$cdaContrObj = new CdaContr();
$cdaContrObj->checkIfLoggedIn(null);

if (!isset($_POST["changepassword_submit"])) {    //isset($_POST["changepassword_submit"]): if all variables in "array" exist and are not null.
  header("Location: ../changepassword.php?error=emptyfields");
  exit();
}

if ($_SESSION["userUid"] == "guest0001") {
  header("Location: ../changepassword.php?error=guest");
  exit();
}

$password = $_POST['pwd'];
$newPassword = $_POST['new_pwd'];
$newPasswordRepeat = $_POST['new_pwd_repeat'];

$cdaContrObj->changePwd($password, $newPassword, $newPasswordRepeat);
unset($cdaContrObj);
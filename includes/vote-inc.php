<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2023 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
//session_cache_limiter('nocashe');    //Before session_start(); or use header("Cache-Control: no-cache, must-revalidate");
session_start();
require_once 'autoloader-inc.php';

if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
	if (isset($_POST["selectedcategory"], $_POST["selectedid"])) {
		if (!isset($_POST["vote"])) {
		  header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=novote");
		  exit();
	  }

	  //Check if username is guest0001. This account is not allowed to create content, become team-member or vote. It is only for viewing content.
    if ($_SESSION["userUid"] == "guest0001") {
      header("Location: ../errorpage.php?error=usernameisguest0001");
      exit();
    }

		switch ($_POST["vote"]) {
			case '1':
				$vote = 1;
				break;

				case '0':
				$vote = 0;
				break;

				case '-1':
				$vote = -1;
				break;
			
			default:
				$vote = 'error';
				break;
		}

		$cdaViewObj = new CdaView();
		$cdaContrObj = new CdaContr();

		switch ($_POST["selectedcategory"]) {
			case 't':    //topic.
				$tgpcr = $cdaViewObj->showSelectedTgpcr($_POST["selectedcategory"], (int)$_POST["selectedid"]);	  //(int)"text" returns 0.
			
				$cdaContrObj->vote((int)$tgpcr['ballot_box_id'], $vote, (int)$_POST["selectedid"], 6);
				unset($cdaContrObj);
        unset($cdaViewObj);
				header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&vote=success");
		    exit();
				break;

		  case 'g':    //group.
				$tgpcr = $cdaViewObj->showSelectedTgpcr($_POST["selectedcategory"], (int)$_POST["selectedid"]);	  //(int)"text" returns 0.
        $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$tgpcr['topic_id']);

			  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
			    header("Location: ../errorpage.php?error=notimetableyet");
			    exit();
			  } elseif ($topicInfo["prop_state"] > 0) {  //topic in proposals phase.
			    header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=groupsphasenotyet");
			    exit();
			  } elseif ($topicInfo["groups_state"] < 0) { //groups phase has been closed.
			    header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=groupsphasepassed");
			    exit();
			  }

        $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
        if ($checkIfMember === 0) {
        	unset($cdaContrObj);
          unset($cdaViewObj);
        	header("Location: ../errorpage.php?error=notopic");
	        exit();
	      } elseif ($checkIfMember === null) {
	      	  unset($cdaContrObj);
            unset($cdaViewObj);
	          header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=tofgroupmembernotmatch");
	         exit();
        }

        $cdaContrObj->vote((int)$tgpcr['ballot_box_id'], $vote, (int)$tgpcr['topic_id'], 2);
				$cdaContrObj->updateMembersScore($_POST["selectedcategory"], (int)$_POST["selectedid"], $vote);
				//For sending an alias and a secret-code to the user, when he/she is voting for a group. These are used later for validating manually the vote (when the report of the temporary results of the voting event is published).
				$aliasAuthContrObj = new AliasAuthContr();
        $aliasAuthContrObj->createVoteTableInfo($vote, $tgpcr);
        unset($aliasAuthContrObj);
				unset($cdaContrObj);
        unset($cdaViewObj);
				header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&vote=success");
		    exit();		
				break;

			case 'p':    //proposal.
				$tgpcr = $cdaViewObj->showSelectedTgpcr($_POST["selectedcategory"], (int)$_POST["selectedid"]);	  //(int)"text" returns 0.
        
        $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$tgpcr['topic_id']);

			  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
			    header("Location: ../errorpage.php?error=notimetableyet");
			    exit();
			  } elseif ($topicInfo["groups_state"] < 0) { //groups phase has been closed.
			    header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=groupsphasepassed");
			    exit();
			  }

        $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
        if ($checkIfMember === 0) {
        	unset($cdaContrObj);
          unset($cdaViewObj);
        	header("Location: ../errorpage.php?error=notopic");
	        exit();
	      } elseif ($checkIfMember === null) {
			      //header("Cache-Control: no-cache, must-revalidate");  // or session_cache_limiter('nocashe'); before session_start();.
	          header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=tofproposalmembernotmatch");
	         exit();
	        /*}*/
        } elseif ($tgpcr['edit_state'] >= 1) {
					unset($cdaContrObj);
          unset($cdaViewObj);
			  	header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=editingphase");
		      exit();
			  }
			
				$cdaContrObj->vote((int)$tgpcr['ballot_box_id'], $vote, (int)$tgpcr['topic_id'], 3);
				$cdaContrObj->updateMembersScore($_POST["selectedcategory"], (int)$_POST["selectedid"], $vote);
				unset($cdaContrObj);
        unset($cdaViewObj);
				header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&vote=success");
		    exit();
				break;

			case 'r':    //reference.
				$tgpcr = $cdaViewObj->showSelectedTgpcr($_POST["selectedcategory"], (int)$_POST["selectedid"]);	  //(int)"text" returns 0.

				$checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
        if ($checkIfMember === 0) {
        	unset($cdaContrObj);
          unset($cdaViewObj);
        	header("Location: ../errorpage.php?error=notopic");
	        exit();
	      } elseif ($checkIfMember === null) {
	      	  unset($cdaContrObj);
            unset($cdaViewObj);
	          header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=tofrefmembernotmatch");
	         exit();
        }
			
				$cdaContrObj->vote((int)$tgpcr['ballot_box_id'], $vote, NULL, 5);
				$cdaContrObj->updateMembersScore($_POST["selectedcategory"], (int)$_POST["selectedid"], $vote);

				unset($cdaContrObj);
        unset($cdaViewObj);
				header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&vote=success");
		    exit();
				break;

			case 'q':    //requirement.
				$tgpcr = $cdaViewObj->showSelectedTgpcr($_POST["selectedcategory"], (int)$_POST["selectedid"]);	  //(int)"text" returns 0.
        $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$tgpcr['topic_id']);

			  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
			    header("Location: ../errorpage.php?error=notimetableyet");
			    exit();
			  } elseif ($topicInfo["inv_state"] > 0) {  //topic in invitation phase.
			    header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=reqphasenotyet");
			    exit();
			  } elseif ($topicInfo["req_state"] < 0) { //requirements phase has been closed.
			    header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=reqphasepassed");
			    exit();
			  }

        $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
        if ($checkIfMember === 0) {
        	unset($cdaContrObj);
          unset($cdaViewObj);
        	header("Location: ../errorpage.php?error=notopic");
	        exit();
	      } elseif ($checkIfMember === null) {
	      	  unset($cdaContrObj);
            unset($cdaViewObj);
	          header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=tofrequirementmembernotmatch");
	         exit();
        } elseif ($tgpcr['approval_status'] !== null) {
        	unset($cdaContrObj);
          unset($cdaViewObj);
	        header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=requirementnotpending");
	        exit();
        }
        $cdaContrObj->vote((int)$tgpcr['ballot_box_id'], $vote, (int)$tgpcr['topic_id'], 1);
				unset($cdaContrObj);
        unset($cdaViewObj);
				header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&vote=success");
		    exit();		
				break;

			case 's':    //similarity.
				$tgpcr = $cdaViewObj->showSelectedTgpcr($_POST["selectedcategory"], (int)$_POST["selectedid"]);	  //(int)"text" returns 0.
       
        $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$tgpcr['topic_id'], (int)$_SESSION["userId"]);
        if ($checkIfMember === 0) {
        	unset($cdaContrObj);
          unset($cdaViewObj);
        	header("Location: ../errorpage.php?error=notopic");
	        exit();
	      } elseif ($checkIfMember === null) {
	      	  unset($cdaContrObj);
            unset($cdaViewObj);
	          header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=tofgroupmembernotmatch");
	         exit();
        }

        $checkIfUserVotedForSOfRevPs = $cdaContrObj->checkIfUserHasVotedForAReversedPairOfSimilarProposals((int)$tgpcr['topic_id'], $tgpcr['proposal_a_id'], $tgpcr['proposal_b_id']);

        if ($checkIfUserVotedForSOfRevPs != null) {
        	unset($cdaContrObj);
          unset($cdaViewObj);
	        header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&error=UVotedForSOfRevPs" . "&revS=" . $checkIfUserVotedForSOfRevPs);
	         exit();
        }

        $cdaContrObj->vote((int)$tgpcr['ballot_box_id'], $vote, (int)$tgpcr['topic_id'], 7);
        //TO DO: !!!table members_score to be merged with topics_members table!!!
				$cdaContrObj->updateMembersScore($_POST["selectedcategory"], (int)$_POST["selectedid"], $vote);
				unset($cdaContrObj);
        unset($cdaViewObj);
				header("Location: ../vote.php?selected=" . $_POST["selectedcategory"] . $_POST["selectedid"] . "&vote=success");
		    exit();		
				break;
			
			default:
				# code...//TO DO: SEND ERROR!!!
				break;
		}
	}
}
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var offsetOfP; // they are on global scope and accessed easily. it is better to use closure.
offsetOfP = 0;

function btnShow()
{ 
    let numOfP, numOfComments, cbShowComments, numOfReferences, cbShowReferences, orderBy, ascDesc, selectedT;    //P: Proposal.
    numOfP =  Number(document.getElementById('div_numofproposals').innerHTML);
    numOfComments = Number(document.getElementById('div_numofcomments').innerHTML);
    cbShowComments = document.getElementById('div_cbshowcomments').innerHTML;
    numOfReferences = Number(document.getElementById('div_numofreferences').innerHTML);
    cbShowReferences = document.getElementById('div_cbshowreferences').innerHTML;
    orderBy = document.getElementById('div_orderby').innerHTML;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    offsetOfP += numOfP;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreP, newDivP, buttonShowMoreP;
            newDivP = document.createElement("div");
            newDivP.setAttribute('id', ('div_proposals_' + offsetOfP.toString()));
            divShowMoreP = document.getElementById('showp');
            buttonShowMoreP = document.getElementById('btn_showp');
            divShowMoreP.insertBefore(newDivP, buttonShowMoreP);
            newDivP.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4, hrefString5, hrefString6;
    hrefString1 = "offset=" + offsetOfP.toString();
    hrefString2 = "&numofproposals=" + numOfP.toString();
    hrefString3 = "&numofcomments=" + numOfComments.toString() + "&numofreferences=" + numOfReferences.toString();
    hrefString4 = "&cbshowcomments=" + cbShowComments + "&cbshowreferences=" + cbShowReferences;
    hrefString5 = "&orderby=" + orderBy;
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString7 = "&selectedt=" + selectedT;
    hrefString = hrefString1 + hrefString2 + hrefString3 + hrefString4 + hrefString5 + hrefString6 + hrefString7;
    xhttp.open("post", "viewproposals_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}

function groupProposals()
{    //selectedProposalsForGroupingOrSimilarities is declared in viewtgpcr.js.
    if (typeof selectedProposalsForGroupingOrSimilarities === "undefined") {
        return;
    }
    
    window.open("createnewgroup.php?" + "selectedProposals=" + selectedProposalsForGroupingOrSimilarities.toString());
}

function similarProposals()
{    //selectedProposalsForGroupingOrSimilarities is declared in viewtgpcr.js.
    if (typeof selectedProposalsForGroupingOrSimilarities === "undefined") {
        return;
    }
    
    window.open("createnewsimilarity.php?" + "selectedProposals=" + selectedProposalsForGroupingOrSimilarities.toString());
}

function viewProposalsOfMembersWithHighestScoreInCateg()
{
    let selectedT, numOfMembers, selectedCategForScore;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    numOfMembers = document.getElementById('select_numofmembers').value;
    selectedCategForScore = document.getElementById('select_selectedcategory').value;
    window.open("viewproposals.php?" + "selectedt=" + selectedT.toString() + "&scoreincateg=" + selectedCategForScore.toString() + "&numofmemb=" + numOfMembers.toString());
}

function viewProposalsWithSpecificIds()
{
    let selectedT, selectedProposalsIdsAsString;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    selectedProposalsIdsAsString = document.getElementById('input_proposalsids').value;
    window.open("viewproposals.php?" + "selectedt=" + selectedT.toString() + "&selectedp=" + selectedProposalsIdsAsString);
}

function viewGroupsThatContainSelectedProposals()
{   
     //selectedProposalsForGroupingOrSimilarities is declared in viewtgpcr.js.
    if (typeof selectedProposalsForGroupingOrSimilarities === "undefined") {
        return;
    }

    let selectedT, selectedProposalsIdsAsString;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    window.open("viewgroups.php?" + "selectedt=" + selectedT.toString() + "&viewGofselectedP=" + selectedProposalsForGroupingOrSimilarities.toString());
}
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
		   $cdaViewObj = new CdaView();
		   $tgpcrObj = new Tgpcr();

       $selectedCateg = substr($_GET['selected'], 0, 1);
       $selectedTgpcrID = intval(substr($_GET['selected'], 1));
       $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
       $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);
  ?>
      <p>Selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
	<?php
	    require_once "headeroftable.php";

			$category = $selectedCateg;
			require "viewtgpcr.php";
?>
      </div>
      <br>
      <label form="form_addnewmembers">Add new members of topic.</label>

      <form action="includes/addnewmembers-inc.php" method="post" name="form_addnewmembers" id="form_addnewmembers">
        <input type="text" name="input_addmember" placeholder="Add a new member..." title="Username" required></input>
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedcategory" name="selectedcategory" value="<?php echo $selectedCateg; ?>">
          <input type="text" id="selectedid" name="selectedid" value="<?php echo $selectedTgpcrID; ?>">
        </div>
        <button type="submit" name="addmember_submit">OK</button>
      </form>
      <br>

      <p id="p_addnewmembers_urlvar"></p>

      <script type="module" src="./addnewmembers.js"></script>
      <script type="text/javascript" src="./viewtgpcr.js"></script>

	<?php
	    unset($cdaViewObj);
	    unset($tgpcrObj);
    } else {
      //redirect to login
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
     Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>

<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
       require_once "viewtopicsnavbar.php";
  ?>

      <div id="div_toolbardata" style="display: none;">
        <div id="div_numoftopics"><?php echo $_SESSION["numoftopics"] ?></div>
        <div id="div_numofproposals"><?php echo $_SESSION["numofproposals"] ?></div>
        <div id="div_cbshowproposals"><?php echo $_SESSION["cbshowproposals"] ?></div>
        <div id="div_orderby"><?php echo $_SESSION["orderby"] ?></div>
        <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
      </div>

      <p id="tip_select">To select a topic or proposal etc. you can click on its id.</p>

			<div class="tgpcrAll">
			<?php
        require "headeroftable.php";
      
        require "viewtopics_in2.php";

        if ($orderBy == 'DATE') {
          $categToShow = 't';
          $tgpcrObj = new Tgpcr();
          $categMsgToShow = $tgpcrObj->categMsg($categToShow);
          $tgpcrInTotalToShow = $totalnumOfTopics;    //declared in viewtopics_in2.php
          unset($tgpcrObj);
          require 'btnshowmoretgpcr.php';
        } elseif ($orderBy != 'DATE') {
          ?>
            <p id="tip_select">Total number of topics: <?php echo $totalnumOfTopics; ?></p>
          <?php
        }
      ?>
      </div>
      <br>

      <script type="text/javascript" src="./viewtgpcr.js"></script>
      <script type="text/javascript" src="./viewtopics.js"></script>

	<?php
      unset($cdaViewObj);    //Declared in viewtipics_in2.php
    } else {
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
     Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>
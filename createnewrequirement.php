<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
      if (!isset($_REQUEST['selectedt'])) {
        exit("<br><br><b>-- Error: Topic ID is not found!<b>");
      }

      $selectedCateg = 'n';
      require_once "selectednavbar.php";

      $cdaViewObj = new CdaView();
      $tgpcrObj = new Tgpcr();

      $selectedCateg = substr($_REQUEST['selectedt'], 0, 1);
      $selectedTgpcrID = intval(substr($_REQUEST['selectedt'], 1));
      $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
      $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

      if ($selectedCategMsg == 'Non-existent category!') {
        exit('-- Error: Category sent to server does not exist!');
      } elseif ($selectedCategMsg != 'topic') {
        exit('-- Error: Category of topic (for new requirement creation) was expected to be sent to server!');
      }

      $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$selectedTgpcrID);

      if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
        exit("<br><br>-- Note: The time table of the topic has not been created yet. You can contact with the initiator of the topic via a personal message.");
      } elseif ($topicInfo["inv_state"] > 0) {
        exit("<br><br>-- Note: The topic is still in the \"team members invitation\" phase. You can view the time table for more info.");
      } elseif ($topicInfo["req_state"] < 0) {
        exit("<br><br>-- Note: The requirements phase has been closed. You can view the time table for more info.");
      }
  ?>

      <br>
      <label form="form_create">Create a new requirement</label>

      <form action="includes/createnewrequirement-inc.php" method="post" name="form_create" id="form_create">
        <textarea name="textarea_name" placeholder="Requirement..." maxlength="700" cols="30" required></textarea>
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedtid" name="selectedtid" value="<?php echo  $selectedTgpcrID; ?>">
        </div>
        <button type="submit" name="create_submit">OK</button>
      </form>
      <br>

      <p>of selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
  <?php
      require_once "headeroftable.php";

      $category = $selectedCateg;
      require "viewtgpcr.php";
  ?>

      <p id="p_create_urlvar"></p>

      <script type="module" src="./createnewrequirement.js"></script>
      <script type="text/javascript" src="./viewtgpcr.js"></script>
	<?php
      unset($cdaViewObj);
      unset($tgpcrObj);
    } else {
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
      Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
function calculateGroupEvaluation()
{
	let weightFactors, scores, checkForError, i, lenghtOfArray;
	weightFactors = document.getElementsByClassName("input_weightfactor");
	scores = document.getElementsByClassName("input_score");
	displayTotalScore = document.getElementById("div_totalscoredisplay");
	displayTotalScore.innerHTML = "";
	lenghtOfArray = weightFactors.length;

	if (lenghtOfArray != scores.length) {
      displayTotalScore.innerHTML = "An error in calculations have occurred (length of array).";
      return;
	}

	for (i = 0; i < lenghtOfArray; i++) {
	  if (weightFactors[i].value == "" || scores[i].value == "") {
        displayTotalScore.innerHTML = "Please not that all the fields must be filled!";
        return;
	  }

	  if (weightFactors[i].value < 0 || weightFactors[i].value > 1) {
        displayTotalScore.innerHTML = "Weight factors must have a value between 0 and 1!";
        return;
	  }

	  if (scores[i].value < 0 || scores[i].value > 10) {
        displayTotalScore.innerHTML = "Scores must have a value between 0 and 10!";
        return;
	  }
	}
    
    totalScore = 0;
	for (i = 0; i < lenghtOfArray; i++) {
	  totalScore += weightFactors[i].value * scores[i].value;
	}

	displayTotalScore.innerHTML = totalScore;
}
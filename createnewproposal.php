<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
      if (!isset($_REQUEST['selectedt'])) {
        exit("<br><br><b>-- Error: Topic ID is not found!<b>");
      }

      $selectedCateg = 'n';
      require_once "selectednavbar.php";
      //require_once "requirementsnavbar.php";

      $cdaViewObj = new CdaView();
      $tgpcrObj = new Tgpcr();

      $selectedCateg = substr($_REQUEST['selectedt'], 0, 1);
      $selectedTgpcrID = intval(substr($_REQUEST['selectedt'], 1));
      $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
      $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

      if ($selectedCategMsg == 'Non-existent category!') {
        exit('-- Error: Category sent to server does not exist!');
      } elseif ($selectedCategMsg != 'topic') {
        exit('-- Error: Category of topic (for new proposal creation) was expected to be sent to server!');
      }

      $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$selectedTgpcrID); //id of topic.

      if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
        exit("<br><br>-- Note: The time table of the topic has not been created yet. You can contact with the initiator of the topic via a personal message.");
      } elseif ($topicInfo["req_state"] > 0) {  //current date is before requirements phase closing date.
        exit("<br><br>-- Note: \"proposals\" phase (third phase) has not been started yet." . 
          " You can view the time table for more info.");
      } elseif ($topicInfo["groups_state"] < 30*24*3600) {  //groups_state = (groups phase closing date - current date) in seconds.
        exit("<br><br>-- Note: The topic is 30 days before the closing of the \"groups\" phase (fourth phase)." . 
          " You can view the time table for more info.");    //proposals creation stops 30days before groups phase closing date.
      }

      $maxEditingPhaseDuration = ($topicInfo["groups_state"] - 15*24*3600)/(24*3600);
  ?>

      <br>
      <label form="form_create">Create a new proposal</label>

      <form action="includes/createnewproposal-inc.php" method="post" name="form_create" id="form_create">
        <textarea name="textarea_name" placeholder="Proposal..." maxlength="700" cols="30" required></textarea> <!-- wrap="hard" -->
        <div class="div_inputlabel_timetable">
        <label class="label_for" for="editingphaseduration">Specify duration of <b>editing phase of proposal</b> (in days):</label>
        <input class="input_time_table" type="number" id="editingphaseduration" name="editingphaseduration" title="Minimum 15 days, maximum 15 days before fourth phase ends." placeholder="Suggested: 15 days" required min="15" max="<?php echo  $maxEditingPhaseDuration; ?>">
        </div>
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedtid" name="selectedtid" value="<?php echo  $selectedTgpcrID; ?>">
        </div>
        <button type="submit" name="create_submit">OK</button>
        <p>Tip: You can write &ltbr&gt if you want to insert a line break (please note that no other html tag is allowed).</p>
      </form>
      <br>

      <p>of selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
  <?php
      require_once "headeroftable.php";

      $category = $selectedCateg;
      require "viewtgpcr.php";
  ?>

      <p id="p_create_urlvar"></p>

      <script type="module" src="./createnewproposal.js"></script>
      <script type="text/javascript" src="./viewtgpcr.js"></script>
	<?php
      unset($cdaViewObj);
      unset($tgpcrObj);
    } else {
      //redirect to login
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
      Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>

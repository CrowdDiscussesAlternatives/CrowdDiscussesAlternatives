/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var offsetOfM; // they are on global scope and accessed easily. it is better to use closure.
offsetOfM = 0;

function btnShow()
{ 
    let numOfM, ascDesc;    //m: message.
    numOfM = 20;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    offsetOfM += numOfM;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreM, newDivM, buttonShowMoreM;
            newDivM = document.createElement("div");
            newDivM.setAttribute('id', ('div_messages_' + offsetOfM.toString()));
            divShowMoreM = document.getElementById('showm');
            buttonShowMoreM = document.getElementById('btn_showm');
            divShowMoreM.insertBefore(newDivM, buttonShowMoreM);
            newDivM.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4, hrefString5, hrefString6;
    hrefString1 = "offset=" + offsetOfM.toString();
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString = hrefString1 + hrefString6;
    xhttp.open("post", "viewsentmessages_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}

function viewRecipients(event)
{
    let categSelected, categIdSelected, eventCurrentTarget, ascDesc;
    eventCurrentTarget = event.currentTarget;
    categSelected = eventCurrentTarget.parentNode.id;
    categIdSelected = categSelected.substr(1, categSelected.length);
    eventCurrentTarget.style.color = '#800000';
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    var xhttpForRecipients = new XMLHttpRequest();
    xhttpForRecipients.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divRecipients, newDivRecipients;
            newDivRecipients = document.createElement("div");
            newDivRecipients.setAttribute('id', ('div_recipients_of_' + categIdSelected));
            divRecipients = document.getElementById(categSelected);
            newDivRecipients.innerHTML = this.responseText;
            divRecipients.parentNode.insertBefore(newDivRecipients, divRecipients.nextSibling);
        }
    };
    let hrefString;
    hrefString = "msg_id=" + categIdSelected + "&ascdesc=" + ascDesc;
    xhttpForRecipients.open("post", "viewrecipientsofmessages.php", true);
    xhttpForRecipients.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttpForRecipients.setRequestHeader("Cache-Control", "no-cache");
    xhttpForRecipients.send(hrefString);
}
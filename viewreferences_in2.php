<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

$totalnumOfR = 0;

if (isset($_REQUEST['offset'], $_REQUEST['numofreferences'], $_REQUEST["cbshowcomments"], $_REQUEST["numofcomments"], $_REQUEST['orderby'], $_REQUEST['ascdesc'])) {
  $offset = intval($_REQUEST['offset']);
  $numOfR = intval($_REQUEST['numofreferences']);
  $cbShowComments = $_REQUEST['cbshowcomments'];
  $numOfComments = intval($_REQUEST['numofcomments']);
  $orderBy = mb_strtoupper($_REQUEST['orderby']);
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfR = intval($_SESSION['numofreferences']);
  $cbShowComments = $_SESSION['cbshowcomments'];
  $numOfComments = intval($_SESSION['numofcomments']);
  $orderBy = $_SESSION['orderby'];
  $ascDesc = $_SESSION['ascdesc'];
}

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

if (isset($_REQUEST['selectedtid'])) {
  $topicId = $_REQUEST['selectedtid'];
  $references = $cdaViewObj->showReferencesInTopic((int)$topicId, (int)$offset, (int)$numOfR, $orderBy, $ascDesc);
  $totalnumOfR = $cdaViewObj->totalNumberOfReferencesInTopic((int)$topicId);
} elseif (isset($_REQUEST['selectedpid'])) {
  $proposalId = $_REQUEST['selectedpid'];
  $references = $cdaViewObj->showReferencesInProposal((int)$proposalId, (int)$offset, (int)$numOfR, $orderBy, $ascDesc);
  $totalnumOfR = $cdaViewObj->totalNumberOfReferencesInProposal((int)$proposalId);
} else{
  $references = $cdaViewObj->showReferences((int)$offset, (int)$numOfR, $orderBy, $ascDesc);  //references in CDA.
  $totalnumOfR = $cdaViewObj->totalNumberOfReferences();
}

if ($references == []) {
?>
  <p>All of <?php echo $totalnumOfR; ?> references are displayed!</p>
<?php
  exit();
}

foreach ($references as $RKey => $ReferenceValue) {
  $category = 'r';
  $tgpcr = $ReferenceValue;
  require "viewtgpcr.php";

  if ($cbShowComments == 'true') {
    $comments = $cdaViewObj->showComments('5' , $ReferenceValue['id'], 0, (int)$numOfComments, $orderBy, $ascDesc); //! $orderBy will be used in the future if there will be voting for comments. Now it is set to comments_in_cda.id. !
    if (isset($comments) || $comments!=[]) {
      foreach ($comments as $commentKey => $commentValue) {
          $category = 'c';
          $tgpcr = $commentValue;
          require "viewtgpcr.php";
      }
    }
  }
}
?>
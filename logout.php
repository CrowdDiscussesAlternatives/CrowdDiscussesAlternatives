<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
  session_start();
  session_unset();
  session_destroy();
  setcookie("auth", "", (time()-3600), '/', "localhost", false, true);
  require "header.php";
?>

<br>
<p><b>You have successfully logged out.</b></p>
<p>IMPORTANT NOTE: Please send a private message to admin that will include a 12-word secret recovery phrase that is not easily guessed<!-- (for instance: Secret Phrase: The color of my car is the color of the Mediterranean sea)-->, and keep this secret phrase safely in your computer (along with the email address below). <u>This step needs to be done only once!</u> Alternatively, you can send your email address to admin via a private message. I will answer to your private message in order for you to know that I have read it. <br><br>If you forget your password, you can send an email at ...(at)....com (replacing (at) with @) witch shall include your username (and your unique secret phrase for verification, in case that you sent a secret phrase to admin). An email will be sent back to you with a temporary password (that you can change after logging in). Please be patient with this process, I am tending to read my emails once every week. <br><br><u>Finally, please notice that if you do not send a private message with a secret recovery phrase (or your email address), you will not be able to retrieve your account, in case you forget your password.</u></p>
<br><br>

<?php
  require "footer.php";
?>
<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
      
  ?>
      <br>
		  <div id="div_toolbar" class="div_showtgpcr">
		  	<label form="form_toolbar">Toolbar</label>
		  	
	      <form action="toolbarresponse.php" method="post" name="form_toolbar" id="form_toolbar">
		      <div id=div_toolbar_select>
			  	  <p>Topics to show: </p>
					  <select name="numoftopics" class="select_showtgpcr">
					    <option value="5">5</option>
					    <option value="10" selected="selected">10</option>
					    <option value="20">20</option>
					    <option value="50">50</option>
					    <option value="100">100</option>
					  </select>
					  <p>Groups to show: </p>
					  <select name="numofgroups" class="select_showtgpcr">
					    <option value="5">5</option>
					    <option value="10" selected="selected">10</option>
					    <option value="20">20</option>
					    <option value="50">50</option>
					    <option value="100">100</option>
					  </select>
					  <p>Proposals to show: </p>
					  <select name="numofproposals" class="select_showtgpcr">
					    <option value="5">5</option>
					    <option value="10" selected="selected">10</option>
					    <option value="20">20</option>
					    <option value="50">50</option>
					    <option value="100">100</option>
					  </select>
					  <p>Comments to show: </p>
					  <select name="numofcomments" class="select_showtgpcr">
					    <option value="5">5</option>
					    <option value="10" selected="selected">10</option>
					    <option value="20">20</option>
					    <option value="50">50</option>
					    <option value="100">100</option>
					    <option value="200">200</option>
					    <option value="500">500</option>
					    <option value="1000">1000</option>
					  </select>
					   <p>References to show: </p>
					  <select name="numofreferences" class="select_showtgpcr">
					    <option value="5">5</option>
					    <option value="10" selected="selected">10</option>
					    <option value="20">20</option>
					    <option value="50">50</option>
					    <option value="100">100</option>
					  </select>
					</div>
					<div class="div_checkbox">
						<input type="checkbox" id="cb_showproposals" name="cb_showproposals" value="true">
					  <label for="cb_showproposals"> Show proposals</label><br>
				  </div>
				  <div class="div_checkbox">
					  <input type="checkbox" id="cb_showcomments" name="cb_showcomments" value="true">
					  <label for="cb_showcomments"> Show comments</label><br>
					</div>
				  <div class="div_checkbox">
					  <input type="checkbox" id="cb_showreferences" name="cb_showreferences" value="true">
					  <label for="cb_showreferences"> Show references</label><br>
					</div>
					<div class="div_radio">
						<input type="radio" id="date" name="orderby" value="DATE">
					  <label for="date">Sort by date</label><br>
					  <input type="radio" id="sumvotes" name="orderby" value="VOTES_SUM">
					  <label for="sumvotes">Sort by sum of votes</label><br>
					  <input type="radio" id="numvotes" name="orderby" value="VOTES_NUM">
					  <label for="numvotes">Sort by number of votes</label>
					</div>
					<div class="div_radio">
						<input type="radio" id="desc" name="ascdesc" value="DESC">
					  <label for="desc">Descending</label><br>
					  <input type="radio" id="asc" name="ascdesc" value="ASC">
					  <label for="asc">Ascending</label><br>
					</div>
					<div id="div_toolbar_btn">
	        <button type="submit" name="toolbar_submit">OK</button>
	        </div>
	      </form>
			</div>
			<br>

  <?php
    } else {
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
     Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>
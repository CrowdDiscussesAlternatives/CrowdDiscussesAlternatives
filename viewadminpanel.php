<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 //$cdaViewObj = new CdaView();
 $cdaContrObj = new CdaContr();

 $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

 require "header.php";
?>

<main>

<br>
<label>Administrator's panel</label>

<p id="p_adminpanel_urlvar"></p>

<br>
<label form="form_modeforsignup">Change the mode for signing up:</label>

<form action="includes/adminpanel-inc.php" method="post" id="form_modeforsignup">
  <input type="Password" id="Password_for_admin_panel_1" name="pwd" placeholder="Password for the admin panel..." required="required">
  <div class="div_radio">
    <input type="radio" id="disable_sign_up" name="modeforsignup" value="disable_sign_up">
    <label for="disable_sign_up">Disable sign up.</label><br><br>
    <input type="radio" id="enable_sign_up" name="modeforsignup" value="enable_sign_up">
    <label for="enable_sign_up">Enable sign up.</label><br><br>
    <input type="radio" id="enable_sign_up_with_secret_code" name="modeforsignup" value="enable_sign_up_with_secret_code">
    <label for="enable_sign_up_with_secret_code">Enable sign up requiring a secret code.</label>
  </div>
  <button type="submit" name="modeforsignup_submit">Change mode</button>
</form>
<br>

<br>
<label form="form_secretcodeforsignup">Change the secret code that a user must enter before signing up (if it is requested):</label>

<form action="includes/adminpanel-inc.php" method="post" id="form_secretcodeforsignup">
  <input type="Password" id="Password_for_admin_panel_2" name="pwd" placeholder="Password for the admin panel..." required="required">
  <input type="Password" id="secretCodeForSignup" name="secretCodeForSignup" placeholder="New secret code..." required="required" minlength="8" maxlength="18" title="Secret code must be at least 8 characters long and no more than 18.">
  <button type="submit" name="secretcodeforsignup_submit">Change secret code</button>
</form>
<br>
<br>

<script type="module" src="./viewadminpanel.js"></script>

<?php
  //exit(password_hash("randompasswordforpanel", PASSWORD_DEFAULT));    //TO DO: DELETE row.

  //unset($cdaViewObj);
  unset($cdaContrObj);
?>

</main>

<?php
  require "footer.php";
?>
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
/*
import { GetUrlVars } from './modules/geturlvars.js';

var ViewTopicsUrlVars, numOfTopics, numofproposals, cbshowproposals;
ViewTopicsUrlVars = new GetUrlVars(window.location.href);
numOfTopics = ViewTopicsUrlVars.urlVar('numOfTopics');
numofproposals = ViewTopicsUrlVars.urlVar('numofproposals');
cbshowproposals = ViewTopicsUrlVars.urlVar('cbshowproposals');
*/

var offsetOfTopic; // they are on global scope and accessed easily. it is better to use closure.
offsetOfTopic = 0;

function btnShow()
{ 
    let numOfTopics, numOfProposals, cbShowProposals, orderBy, ascDesc;
    numOfTopics = Number(document.getElementById('div_numoftopics').innerHTML);
    numOfProposals = Number(document.getElementById('div_numofproposals').innerHTML);
    cbShowProposals = document.getElementById('div_cbshowproposals').innerHTML;
    orderBy = document.getElementById('div_orderby').innerHTML;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    offsetOfTopic += numOfTopics;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreTopics, newDivTopics, buttonShowMoreTopics;
            newDivTopics = document.createElement("div");
            newDivTopics.setAttribute('id', ('div_topics_' + offsetOfTopic.toString()));
            divShowMoreTopics = document.getElementById('showt');
            buttonShowMoreTopics = document.getElementById('btn_showt');
            divShowMoreTopics.insertBefore(newDivTopics, buttonShowMoreTopics);
            newDivTopics.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4;
    hrefString1 = "offset=" + offsetOfTopic.toString();
    hrefString2 = "&numoftopics=" + numOfTopics.toString();
    hrefString3 = "&numofproposals=" + numOfProposals.toString();
    hrefString4 = "&cbshowproposals=" + cbShowProposals;
    hrefString5 = "&orderby=" + orderBy;
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString = hrefString1 + hrefString2 + hrefString3 + hrefString4 + hrefString5 + hrefString6;
    xhttp.open("post", "viewtopics_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}
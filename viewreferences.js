/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var offsetOfR; // they are on global scope and accessed easily. it is better to use closure.
offsetOfR = 0;

function btnShow()
{ 
    let numOfR, numOfComments, cbShowComments, orderBy, ascDesc, selectedTId, selectedPId,elementDivSelectedTId, elementDivSelectedPId, hrefString7;    //R: Reference.
    numOfR = Number(document.getElementById('div_numofreferences').innerHTML);;
    numOfComments = Number(document.getElementById('div_numofcomments').innerHTML);
    cbShowComments = document.getElementById('div_cbshowcomments').innerHTML;
    orderBy = document.getElementById('div_orderby').innerHTML;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    elementDivSelectedTId = document.getElementById('div_selectedtid'); // if div_selectedtid does not exist it returns null.
    elementDivSelectedPId = document.getElementById('div_selectedpid');
    if (elementDivSelectedTId != null) {
        selectedTId = elementDivSelectedTId.innerHTML;  //selected topicId.
        hrefString7 = "&selectedtid=" + selectedTId;
    } else if (elementDivSelectedPId != null) {
        selectedPId = elementDivSelectedPId.innerHTML;  //selected proposalId.
        hrefString7 = "&selectedpid=" + selectedPId;
    } else {
        selectedTId = '';
        selectedPId = '';
        hrefString7 = "";
    }
    offsetOfR += numOfR;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreR, newDivR, buttonShowMoreR;
            newDivR = document.createElement("div");
            newDivR.setAttribute('id', ('div_references_' + offsetOfR.toString()));
            divShowMoreR = document.getElementById('showr');
            buttonShowMoreR = document.getElementById('btn_showr');
            divShowMoreR.insertBefore(newDivR, buttonShowMoreR);
            newDivR.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4, hrefString5, hrefString6;
    hrefString1 = "offset=" + offsetOfR.toString();
    hrefString2 = "&numofreferences=" + numOfR.toString();
    hrefString3 = "&numofcomments=" + numOfComments.toString();
    hrefString4 = "&cbshowcomments=" + cbShowComments;
    hrefString5 = "&orderby=" + orderBy;
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString = hrefString1 + hrefString2 + hrefString3 + hrefString4 + hrefString5 + hrefString6 + hrefString7;
    xhttp.open("post", "viewreferences_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}
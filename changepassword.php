<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 $cdaContrObj = new CdaContr();

 $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

 require "header.php";
?>

<main>
<br>
<p id="p_changepassword_urlvar"></p>

<br>
<label form="form_changepassword">Change your password:</label>

<form action="includes/changepassword-inc.php" method="post" id="form_changepassword">
  <!--<input type="text" name="uid" placeholder="Username..." maxlength="20" required="required">-->
  <input type="Password" name="pwd" placeholder="Current password..." required="required" minlength="6" maxlength="20">
  <input type="Password" name="new_pwd" placeholder="New password..." required="required" minlength="6" maxlength="20" title="Password must be at least 6 characters long and no more than 20.">
  <input type="Password" name="new_pwd_repeat" placeholder="Repeat new password..." required="required" minlength="6" maxlength="20">
  <button type="submit" name="changepassword_submit">Change password</button>
</form>
<br>
<br>

<script type="module" src="./changepassword.js"></script>

<?php

  unset($cdaContrObj);
?>

</main>

<?php
  require "footer.php";
?>
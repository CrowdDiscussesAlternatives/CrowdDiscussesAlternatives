<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 require "header.php";
?>

<main>

	<?php
    $cdaContrObj = new CdaContr();
    $cdaContrObj->checkIfLoggedIn(null);

    if (!(isset($_REQUEST['selectedpid']) || isset($_REQUEST['selectedtid']))) {
      exit("<br><br><b>-- Error: ID is not found!<b>");
    }

    $selectedCateg = 'n';
    require_once "selectednavbar.php";

    $cdaViewObj = new CdaView();
    $tgpcrObj = new Tgpcr();
    
    if (isset($_REQUEST['selectedpid'])) {
      $selectedCateg = 'p';
      $selectedTgpcrID = intval($_REQUEST['selectedpid']);
      $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
      $selectedTopicID = $tgpcr['topic_id'];
    } elseif (isset($_REQUEST['selectedtid'])) {
      $selectedCateg = 't';
      $selectedTgpcrID = intval($_REQUEST['selectedtid']);
      $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
      $selectedTopicID = $selectedTgpcrID;
    }
    $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

    $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$selectedTopicID);

    if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
      require "closehtmltag.php";
      exit("<br><br>-- Note: The time table of the topic has not been created yet. You can contact with the initiator of the topic via a personal message.");
    } elseif ($topicInfo["inv_state"] > 0) {  //current date is before members invitation phase closing date.
      require "closehtmltag.php";
      exit("<br><br>-- Note: The \"requirements\" phase (second phase) has not been started yet." . 
        " You can view the time table for more info.");
    } elseif ($topicInfo["groups_state"] < 0) {  //groups_state = (groups phase closing date - current date) in seconds.
      require "closehtmltag.php";
      exit("<br><br>-- Note: The \"groups\" phase (fourth phase) of the topic has been closed." . 
        " You can view the time table for more info.");
    }
    
?>

    <br>
    <label form="form_create">Add a new reference</label>

    <form action="includes/createaddreference-inc.php" method="post" name="form_create" id="form_create">
      <textarea name="ref_description" placeholder="Reference title or description..." maxlength="300" cols="30" required></textarea>
      <input class="input_url" type="url" id="reference_url" name="reference_url" placeholder="Reference link (URL)..." maxlength="2000" cols="30" required>
      <div id="selecteddata" style="display: none;">
        <?php
        if (isset($_REQUEST['selectedpid'])) {
        ?>
          <input type="text" id="selectedpid" name="selectedpid" value="<?php echo  $selectedTgpcrID; ?>">
        <?php
        } elseif (isset($_REQUEST['selectedtid'])) {
        ?>
          <input type="text" id="selectedtid" name="selectedtid" value="<?php echo  $selectedTgpcrID; ?>">
        <?php
        }
        ?>
      </div>
      <button type="submit" name="create_submit">OK</button>
    </form>
    <br>

    <p>to selected <?php echo $selectedCategMsg; ?>:</p>

    <div class="tgpcrAll">
<?php
    require_once "headeroftable.php";

    $category = $selectedCateg;
    require "viewtgpcr.php";
?>

    <p id="p_create_urlvar"></p>

    <script type="module" src="./createaddreference.js"></script>
    <script type="text/javascript" src="./viewtgpcr.js"></script>
<?php
    unset($cdaViewObj);
    unset($tgpcrObj);
?>

</main>

<?php
  require "footer.php";
?>

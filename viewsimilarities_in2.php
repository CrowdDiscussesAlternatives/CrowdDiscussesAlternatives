<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

if (!isset($cdaContrObj)) {
  $cdaContrObj = new CdaContr();
}

$totalnumOfS = 0;

if (isset($_REQUEST['offset'], $_REQUEST['numofsimilarities'], $_REQUEST['orderby'], $_REQUEST['ascdesc'])) {
  if (!isset($_REQUEST['selectedt'])) {
   exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }
  $offset = intval($_REQUEST['offset']);
  $numOfS = intval($_REQUEST['numofsimilarities']);
  $orderBy = mb_strtoupper($_REQUEST['orderby']);
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfS = intval($_SESSION['numofsimilarities']);
  $orderBy = $_SESSION['orderby'];
  $ascDesc = $_SESSION['ascdesc'];
}

$topicId = intval(substr($_REQUEST['selectedt'], 1));
if ($topicId == 0 || $topicId == null) {
  exit("<br><br>-- Error: Topic ID is not found!");
}

if (isset($_REQUEST['selectedsid'])) {  //When isset($_REQUEST['selectedsid']), it shows only selected similarity.
  $similarity = $cdaViewObj->showSelectedTgpcr('s', (int)$_REQUEST['selectedsid']);   //(int)"text" returns 0.
  $similarities = [$similarity];
  $totalnumOfS = 1;
} elseif (isset($_REQUEST['c_m'])) {
  $similarities = $cdaViewObj->showSimilaritiesReportedByCurrentMember((int)$topicId, $orderBy, $ascDesc);
  $totalnumOfS = count($similarities);
} elseif (isset($_REQUEST['v1by'])) {
  $similarities = $cdaViewObj->showSimilaritiesThatUserVoted1((int)$topicId);
  $totalnumOfS = count($similarities);
} else {
  $similarities = $cdaViewObj->showSimilarities((int)$topicId, (int)$offset, (int)$numOfS, $orderBy, $ascDesc);
  $totalnumOfS = $cdaViewObj->totalNumberOfSimilarities((int)$topicId);
}

if ($similarities == []) {
?>
  <p>All of <?php echo $totalnumOfS; ?> similarities of proposals are displayed!</p>
<?php
  exit();
}

foreach ($similarities as $similarityKey => $similarityValue) {
  $category = 's';
  $tgpcr = $similarityValue;
  require "viewtgpcr.php";

  $tgpcr =$cdaViewObj->showSelectedTgpcr('p', (int)$similarityValue['proposal_a_id']);
  $category = 'p';
  //$tgpcr = $proposalAId;
  require "viewtgpcr.php";

  $tgpcr =$cdaViewObj->showSelectedTgpcr('p', (int)$similarityValue['proposal_b_id']);
  $category = 'p';
  //$tgpcr = $proposalBId;
  require "viewtgpcr.php";
}
?>
<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2025 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<!--
<br><br>
<h3>!!! Warning: This website is under maintenance !!!</h3>
<br><br>
-->
<p>
You can signup/login or login <u>as a guest using the username guest0001 and password guest0001.</u> Please note that this account (with username guest0001) is only for viewing content, i.e. you cannot create content, become a team-member, or vote as a guest.
</p>
<p>
<u>Please note </u>that currently, the ability of signing up is active only for people that I know and friends of mine. You can still login as a guest, if you want to see the content of this platform.
</p>
<br>
<br>
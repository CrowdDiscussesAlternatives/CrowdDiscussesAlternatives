/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, inputVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
inputVar = UrlVars.urlVar('input');
infoMessage = document.getElementById('p_review_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'noreview') {
    infoMessage.innerHTML = "You must select one of the two options (approve or reject) in order to review the requirement!";
    document.getElementById('form_approve_reject').style.display = 'none';
  }
  else if (errorVar == 'reqphasenotyet') {
    infoMessage.innerHTML = "The requirements phase has not been closed yet in order to be able to review them." + 
    " You can view the time table for more info.";
    document.getElementById('form_approve_reject').style.display = 'none';
  } 
  else if (errorVar == 'initnotmatch') {
    infoMessage.innerHTML = "You are not the initiator of the topic that the requirement belongs in order to review it.";
    document.getElementById('form_approve_reject').style.display = 'none';
  }
  else if (errorVar == 'reviewed') {
    infoMessage.innerHTML = "You have already reviewed this requirement of the topic.";
    document.getElementById('form_approve_reject').style.display = 'none';
  }
}
else if (inputVar != "") {
  if (inputVar == 'success') {
    infoMessage.innerHTML =  "You have successfully reviewed this requirement of the topic.";
    document.getElementById('form_approve_reject').style.display = 'none';
  }
}
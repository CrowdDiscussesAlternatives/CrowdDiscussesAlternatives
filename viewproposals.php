<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 $cdaViewObj = new CdaView();
 $cdaContrObj = new CdaContr();

 $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

 require "header.php";
?>

<main>

	<?php
  if (!isset($_REQUEST['selectedt'])) {
    exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }

  require_once "viewproposalsnavbar.php";
  ?>

  <div id="div_toolbardata" style="display: none;">
    <div id="div_numofproposals"><?php echo $_SESSION["numofproposals"] ?></div>
    <div id="div_numofcomments"><?php echo $_SESSION["numofcomments"] ?></div>
    <div id="div_cbshowcomments"><?php echo $_SESSION["cbshowcomments"] ?></div>
    <div id="div_numofreferences"><?php echo $_SESSION["numofreferences"] ?></div>
    <div id="div_cbshowreferences"><?php echo $_SESSION["cbshowreferences"] ?></div>
    <div id="div_orderby"><?php echo $_SESSION["orderby"] ?></div>
    <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
    <div id="div_selectedt"><?php echo $_REQUEST['selectedt'] ?></div>
  </div>

  <?php
  if (isset($_REQUEST['selectedm'])) {
    if ($_REQUEST['selectedm'] != '') {
    ?>
      <p class="class_tip">Proposals of selected team-members are displayed.</p>
    <?php
    } else {
      ?>
      <p class="class_tip"><b>Please notice that you have not selected team-members in order to view their proposals!</b></p>
    <?php
    }
  } 

  if (isset($_REQUEST['scoreincateg'], $_REQUEST['numofmemb'])) {
    ?>
      <p class="class_tip"><b>Below are displayed the proposals of the first <?php echo $_REQUEST['numofmemb']; ?> team-member(s) whose <?php echo $_REQUEST['scoreincateg']; ?> are the most voted in topic.</b></p>
    <?php
  }

  if (isset($_REQUEST['selectedp'])) {
    ?>
      <p class="class_tip"><b>Proposals of selected IDs are displayed.</b></p>
    <?php
  }

  if (isset($_REQUEST['lcp'])) {
    ?>
      <p class="class_tip"><b>Below are displayed the latest commented proposals.</b></p>
    <?php
  }

  if (isset($_REQUEST['viewp'], $_REQUEST['strs'], $_REQUEST['m'])) {
    if ($_REQUEST['m'] == 'currentmember') {
        $msgM = 'you';
      } else {
        $msgM = 'all team-members';
      }
      $displayedTip = "Below are displayed the proposals that some of their comments (created by " . $msgM . ") contain the specified word/phrase: \"" . $_REQUEST['strs'] . "\".";
    ?>
      <p class="class_tip"><b><?php echo $displayedTip; ?></b></p>
    <?php
  }

  if (isset($_REQUEST['v1by'])) {
    ?>
      <p class="class_tip"><b>Below are displayed the proposals that you have voted +1.</b></p>
    <?php
  }

  if (isset($_REQUEST['viewp_excls_v1by'])) {
    ?>
      <p class="class_tip"><b>Below are displayed the proposals of the topic, excluding the proposals that are second in order inside the similarities that you have voted +1.</b> Maximum number of displayed proposals: 1000.</p>
    <?php
  }
  ?>

  <p class="class_tip">To select a proposal you can click on its id. You can also group multiple proposals by clicing on each of their text and then pressing:</p>
  <button type="button" onclick="groupProposals()">Select</button>
  <p class="paragraphafterbutton"> proposals for grouping.</p>
  <br>
  <p class="class_tip">You can report multiple proposals as similar by clicing on each of their text and then pressing:</p>
  <button type="button" onclick="similarProposals()">Select</button>
  <p class="paragraphafterbutton"> proposals for reporting them as similar.</p>
  <br><br>
  <button type="button" onclick="viewGroupsThatContainSelectedProposals()">View</button>
  <p class="paragraphafterbutton"> groups that contain selected proposals. You can select multiple proposals by clicing on each of their text (content).</p>
  <br><br>
  <button type="button" onclick="viewProposalsWithSpecificIds()">View</button>
  <p class="paragraphafterbutton"> proposals with ids: </p>
  <input type="text" name="input_proposalsids" id="input_proposalsids" class="input_ids_entries" placeholder="Enter IDs of proposals separated with commas (e.g. 3,7,21,5)..." title="Enter IDs of proposals separated with commas (e.g. 3,7,21,5)"></input>
  <p class="paragraphafterbutton"> (separate ids with commas e.g. 3,7,21,5) </p> 
  <br><br>
  <button type="button" onclick="viewProposalsOfMembersWithHighestScoreInCateg()">View</button>
  <p class="paragraphafterbutton"> proposals of the first </p> 
  <select id="select_numofmembers" name="select_numofmembers" class="select_numofmemb">
    <option value="1">1</option>
    <option value="3" selected="selected">3</option>
    <option value="5">5</option>
    <option value="10">10</option>
    <option value="20">20</option>
  </select>
  <p class="paragraphafterbutton">team-members that have written the most voted </p>
  <select id="select_selectedcategory" name="select_selectedcategory" class="select_selectedcateg"> 
    <option value="groups">groups</option>
    <option value="proposals">proposals</option>
    <option value="references" selected="selected">references</option>
    <option value="similarities">similarities</option>
  </select>
  <p class="paragraphafterbutton">in the topic.</p>

	<div class="tgpcrAll">
	<?php
  require "headeroftable.php";

  require "viewproposals_in2.php";

  if (!isset($_REQUEST['lcp']) && !isset($_REQUEST['selectedm']) && !isset($_REQUEST['scoreincateg']) && !isset($_REQUEST['selectedp']) && !isset($_REQUEST['viewp']) && !isset($_REQUEST['v1by']) && !isset($_REQUEST['viewp_excls_v1by'])) {
    $checkRequestVars = true;
  } else {
    $checkRequestVars = false;
  }

  if ($orderBy == 'DATE' && $checkRequestVars == true) {  //When isset($_REQUEST['lcp']) proposals are sorted by the most recently commented.
    $categToShow = 'p';
    $tgpcrObj = new Tgpcr();
    $categMsgToShow = $tgpcrObj->categMsg($categToShow);
    $tgpcrInTotalToShow = $totalnumOfP;    //declared in viewproposals_in2.php
    unset($tgpcrObj);
    require 'btnshowmoretgpcr.php';
  } elseif ($orderBy != 'DATE' && $checkRequestVars == true) {
    ?>
      <p class="class_tip">Total number of proposals in topic: <?php echo $totalnumOfP; ?></p>
    <?php
  }
  ?>
  </div>
  <br>

  <script type="text/javascript" src="./viewtgpcr.js"></script>
  <script type="text/javascript" src="./viewproposals.js"></script>

	<?php
  unset($cdaViewObj);    //Declared in viewproposals_in2.php
  unset($cdaContrObj);
	?>

</main>

<?php
  require "footer.php";
?>
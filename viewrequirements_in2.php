<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

$totalnumOfQ = 0;

if (isset($_REQUEST['offset'], $_REQUEST["cbshowcomments"], $_REQUEST["numofcomments"], $_REQUEST['orderby'], $_REQUEST['ascdesc'])) {
  if (!isset($_REQUEST['selectedt'])) {
   exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }
  $offset = intval($_REQUEST['offset']);
  $numOfQ = 10;
  $cbShowComments = $_REQUEST['cbshowcomments'];
  $numOfComments = intval($_REQUEST['numofcomments']);
  $orderBy = mb_strtoupper($_REQUEST['orderby']);
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfQ = 10;
  $cbShowComments = $_SESSION['cbshowcomments'];
  $numOfComments = intval($_SESSION['numofcomments']);
  $orderBy = $_SESSION['orderby'];
  $ascDesc = $_SESSION['ascdesc'];
}

if (!isset($cdaViewObj)) {
  $cdaViewObj = new CdaView();
}

$topicId = intval(substr($_REQUEST['selectedt'], 1));
if ($topicId == 0 || $topicId == null) {
  exit("<br><br>-- Error: Topic ID is not found!");
}

if (isset($_REQUEST['var'])) {
  $requirements = $cdaViewObj->showApprovedRequirements((int)$topicId, (int)$offset, (int)$numOfQ, $orderBy, $ascDesc);
  $totalnumOfQ = $cdaViewObj->totalNumberOfApprovedRequirements((int)$topicId);
} else {
  $requirements = $cdaViewObj->showRequirements((int)$topicId, (int)$offset, (int)$numOfQ, $orderBy, $ascDesc);
  $totalnumOfQ = $cdaViewObj->totalNumberOfRequirements((int)$topicId);
}

if ($requirements == []) {
?>
  <p>All of <?php echo $totalnumOfQ; ?> requirements are displayed!</p>
<?php
  exit();
}

foreach ($requirements as $QKey => $RequirementValue) {
  $category = 'q';
  $tgpcr = $RequirementValue;
  require "viewtgpcr.php";

  if ($cbShowComments == 'true') {
    $comments = $cdaViewObj->showComments('1' , $RequirementValue['id'], 0, (int)$numOfComments, $orderBy, $ascDesc);  //! $orderBy will be used in the future if there will be voting for comments. Now it is set to comments_in_cda.id. !
    if (isset($comments) || $comments!=[]) {
      foreach ($comments as $commentKey => $commentValue) {
          $category = 'c';
          $tgpcr = $commentValue;
          require "viewtgpcr.php";
      }
    }
  }
}
?>
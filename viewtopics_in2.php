<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
if (!isset($sessionStarted)) {
  session_start();    //session_start(); on the top of the code.
}
require_once 'includes/autoloader-inc.php';

$totalnumOfTopics = 0;

if (isset($_REQUEST['offset'], $_REQUEST["numoftopics"], $_REQUEST["cbshowproposals"], $_REQUEST["numofproposals"], $_REQUEST['orderby'], $_REQUEST['ascdesc'])) {
  $offset = intval($_REQUEST['offset']);
  $numOfTopics = intval($_REQUEST['numoftopics']);
  $cbShowProposals = $_REQUEST['cbshowproposals'];
  $numOfProposals = intval($_REQUEST['numofproposals']);
  $orderBy = mb_strtoupper($_REQUEST['orderby']);
  $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
} else {
  $offset = 0;
  $numOfTopics = intval($_SESSION['numoftopics']);
  $cbShowProposals = $_SESSION['cbshowproposals'];
  $numOfProposals = intval($_SESSION['numofproposals']);
  $orderBy = $_SESSION['orderby'];
  $ascDesc = $_SESSION['ascdesc'];
}

$cdaViewObj = new CdaView();
$topics = $cdaViewObj->showTopics((int)$offset, (int)$numOfTopics, $orderBy, $ascDesc);
$totalnumOfTopics = $cdaViewObj->totalNumberOfTopics();

if ($topics == []) {
?>
  <p>All of <?php echo $totalnumOfTopics; ?> topics are displayed!</p>
<?php
  exit();
}

foreach ($topics as $topicKey => $topicValue) {
  $category = 't';
  $tgpcr = $topicValue;
  require "viewtgpcr.php";

  if ($cbShowProposals == 'true') {
    $proposals = $cdaViewObj->showProposals($topicValue['id'], 0, (int)$numOfProposals, $orderBy, $ascDesc);
    if (isset($proposals) || $proposals!=[]) {
      foreach ($proposals as $proposalKey => $proposalValue) {
          $category = 'p';
          $tgpcr = $proposalValue;
          require "viewtgpcr.php";
      }
    }
  }
}
?>
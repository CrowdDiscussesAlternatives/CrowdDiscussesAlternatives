<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

	<?php
    if (isset($_SESSION['userId'], $_SESSION['auth'] ,$_COOKIE['auth']) && $_COOKIE['auth'] == $_SESSION['auth']) {
		   $cdaViewObj = new CdaView();
		   $tgpcrObj = new Tgpcr();

       $selectedCateg = substr($_GET['selected'], 0, 1);
       $selectedTgpcrID = intval(substr($_GET['selected'], 1));
       $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
       $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

       if ($selectedCategMsg == 'Non-existent category!') {
         exit('-- Error: Category sent to server does not exist!');
       }
  ?>
      <p>Selected <?php echo $selectedCategMsg; ?>:</p>

      <div class="tgpcrAll">
	<?php
	    require_once "headeroftable.php";

			$category = $selectedCateg;
			require "viewtgpcr.php";

      switch ($selectedCateg) {
        case 't':
          $infoText = "<p>Vote 1 if you agree that <b>this topic is important</b>, -1 if you disagree or 0 if you are neutral.<br><br> Remember that you cannot change your vote later. You can always choose not to vote if you do not feel well-informed yet.</p>";
          break;

        case 'r':
          $infoText = "<p>Vote 1 if you agree that <b>this reference is important and accurate</b>, -1 if you think that it is neither important nor aqurate or 0 if you evaluate this reference something in between 1 and -1.<br><br> Remember that you cannot change your vote later. You can always choose not to vote if you do not feel well-informed yet.</p>";
          break;

        case 's':
          $infoText = "<p>Vote 1 if you agree that these two proposals are similar, -1 if you disagree or 0 if you are neutral. Note that the first proposal is considered the most well written from the two.<br><br> Remember that you cannot change your vote later. You can always choose not to vote if you do not feel well-informed yet.</p>";
          break;
        
        default:
          $infoText = "<p>Vote 1 if you agree with this " . $selectedCategMsg . ", -1 if you disagree or 0 if you are neutral.<br><br> You can read the comments/references (when available) before you make your mind. Remember that you cannot change your vote later. You can always choose not to vote if you do not feel well-informed yet.</p>";
          break;
      }

      echo $infoText;
	?>
       
      <label form="form-vote">Vote</label>

      <form action="includes/vote-inc.php" method="post" name="form-vote" id="form-vote">
        <div class="div_radio">
            <input type="radio" id="agree" name="vote" value="1">
            <label for="agree"> +1, I agree</label><br><br>
            <input type="radio" id="neutral" name="vote" value="0">
            <label for="neutral">0, I neither agree or disagree</label><br><br>
            <input type="radio" id="disagree" name="vote" value="-1">
            <label for="disagree">-1, I dissagree</label>
        </div>
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedcategory" name="selectedcategory" value="<?php echo $selectedCateg; ?>">
          <input type="text" id="selectedid" name="selectedid" value="<?php echo $selectedTgpcrID; ?>">
        </div>
        <button type="submit" name="vote-submit">Vote</button>
      </form>
      </div>
      <br>
      <p id="p_vote_urlvar"></p>

  <?php
      //For sending an alias and a secret-code to the user, when he/she is voting for a group. These are used later for validating manually the vote (when the report of the temporary results of the voting event is published).
      if ($selectedCateg == 'g' && isset($_GET["vote"]) && $_GET["vote"] == "success") {
        require 'votetableinfo.php';
        /*?>
          <script type="text/javascript" src="./votewithalias.js"></script>
        <?php*/
      }
  ?>
      <script type="module" src="./vote.js"></script>
      <script type="text/javascript" src="./viewtgpcr.js"></script>

	<?php
	    unset($cdaViewObj);
	    unset($tgpcrObj);
    } else {
      //redirect to login
  ?>

      <p>You are not logged in yet (or you are logged out).<br><br>
     Please notice that cookies must be allowed in order to login (only essential cookies for funcionality of the site are used).</p>

  <?php
	  }
	?>

</main>

<?php
  require "footer.php";
?>
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
function viewProposalsTheirCommentsContainSelectedStrs()
{
    let selectedT, selectedMembers, strs;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    selectedMembers = document.getElementById('select_selectedmembersforviewp').value;
    strs = document.getElementById('input_strsforviewp').value;
    window.open("viewproposals.php?" + "selectedt=" + selectedT.toString() + "&viewp=true&strs=" + strs.toString() + "&m=" + selectedMembers);
}

function viewGroupsTheirCommentsContainSelectedStrs()
{
    let selectedT, selectedMembers, strs;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    selectedMembers = document.getElementById('select_selectedmembersforviewg').value;
    strs = document.getElementById('input_strsforviewg').value;
    window.open("viewgroups.php?" + "selectedt=" + selectedT.toString() + "&viewg=true&strs=" + strs.toString() + "&m=" + selectedMembers);
}
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var nav;
var selectedProposalsForGroupingOrSimilarities;
selectedProposalsForGroupingOrSimilarities = [];

var tableHeader; 
tableHeader = document.getElementById("table_header");

nav = document.getElementById("nav");
if (nav != null) {
  nav.addEventListener("mouseover", hideTableHeader);
  nav.addEventListener("mouseout", showTableHeader);
}

function hideTableHeader()
{   
    if (tableHeader != null) {
      tableHeader.style.display = 'none';
    }
}

function showTableHeader()
{
    if (tableHeader != null) {
      tableHeader.style.display = 'inline-block';
    }
}

function selectTgpcr(event)
{
    let categIdSelected, categIdParent, eventCurrentTarget;
    eventCurrentTarget = event.currentTarget;
    categIdSelected = eventCurrentTarget.innerHTML;
    eventCurrentTarget.style.color = '#800000';
    categIdParent = eventCurrentTarget.parentNode.lastElementChild.innerHTML;
    //event.stopPropagation();
    window.open("selected.php?" + "selected=" + categIdSelected.toString() + "&parent=" + categIdParent.toString());
}

function selectProposals(event)
{
    let pagePathname;
    pagePathname = window.location.pathname;
    if (!pagePathname.includes("viewproposals.php")) {
      return;
    }

    let proposalIdSelected, eventCurrentTarget;
    eventCurrentTarget = event.currentTarget;
    proposalIdSelected = eventCurrentTarget.parentNode.firstElementChild.innerHTML.slice(1);
    if (!selectedProposalsForGroupingOrSimilarities.includes(proposalIdSelected)) {
      selectedProposalsForGroupingOrSimilarities.push(proposalIdSelected);
      eventCurrentTarget.style.color = '#DC143C';
    }
}
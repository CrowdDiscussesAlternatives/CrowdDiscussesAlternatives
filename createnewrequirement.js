/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, createVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
createVar = UrlVars.urlVar('create');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'qexists') {
    infoMessage.innerHTML = "This requirement already exists!";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'reqphasenotyet') {
    infoMessage.innerHTML = "The topic is still in the \"team members invitation\" phase." + 
    " You can view the time table for more info.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'reqphasepassed') {
    infoMessage.innerHTML = "The requirements phase has been closed. You can view the time table for more info.";
    document.getElementById('form_create').style.display = 'none';
  }
} 
else if (createVar != "") {
  if (createVar == 'success') {
    infoMessage.innerHTML =  "You have successfully created the requirement.";
    document.getElementById('form_create').style.display = 'none';
  }
}
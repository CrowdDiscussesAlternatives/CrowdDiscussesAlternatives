<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

<?php
  $cdaViewObj = new CdaView();
  $cdaContrObj = new CdaContr();

  $cdaContrObj->checkIfLoggedIn(null);
/*
  if (!isset($_REQUEST['selectedt'])) {
    exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }
*/
  $selectedCateg = 'n';
   require_once "selectednavbar.php";
?>

  <div id="div_toolbardata" style="display: none;">
    <div id="div_numofreferences"><?php echo $_SESSION["numofreferences"] ?></div>
    <div id="div_numofcomments"><?php echo $_SESSION["numofcomments"] ?></div>
    <div id="div_cbshowcomments"><?php echo $_SESSION["cbshowcomments"] ?></div>
    <div id="div_orderby"><?php echo $_SESSION["orderby"] ?></div>
    <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
<?php
    if (isset($_REQUEST['selectedtid'])) {
?>
      <div id="div_selectedtid"><?php echo $_REQUEST['selectedtid'] ?></div>
<?php
    } elseif (isset($_REQUEST['selectedpid'])) {
?>
      <div id="div_selectedpid"><?php echo $_REQUEST['selectedpid'] ?></div>
<?php
    }
?>
  </div>


	<div class="tgpcrAll">
	<?php    
    if (isset($_REQUEST['selectedtid'])) {
      ?>
        <p>References of topic:</p>
      <?php
      require "headeroftable.php";
      $category = 't';
      $tgpcr = $cdaViewObj->showSelectedTgpcr('t', (int)$_REQUEST['selectedtid']);
      require "viewtgpcr.php";
    } elseif (isset($_REQUEST['selectedpid'])) {
      ?>
        <p>References of proposal:</p>
      <?php
      require "headeroftable.php";
      $category = 'p';
      $tgpcr = $cdaViewObj->showSelectedTgpcr('p', (int)$_REQUEST['selectedpid']);
      require "viewtgpcr.php";
    } else {
      ?>
        <p>References of Crowd Discusses Alternatives:</p>
      <?php
        require "headeroftable.php";
    }
    ?>
      <p id="tip_select">To select a reference you can click on its id.</p>
    <?php

    require "viewreferences_in2.php";

    if ($orderBy == 'DATE') {
      $categToShow = 'r';
      $tgpcrObj = new Tgpcr();
      $categMsgToShow = $tgpcrObj->categMsg($categToShow);
      $tgpcrInTotalToShow = $totalnumOfR;    //declared in viewreferences_in2.php
      unset($tgpcrObj);
      require 'btnshowmoretgpcr.php';
    } elseif ($orderBy != 'DATE' && isset($_REQUEST['selectedtid'])) {
      ?>
        <p id="tip_select">Total number of references in topic: <?php echo $totalnumOfR; ?></p>
      <?php
    } elseif ($orderBy != 'DATE' && isset($_REQUEST['selectedpid'])) {
      ?>
        <p id="tip_select">Total number of references in proposal: <?php echo $totalnumOfR; ?></p>
      <?php
    } else {
      ?>
        <p id="tip_select">Total number of references: <?php echo $totalnumOfR; ?></p>
      <?php
    }
  ?>
  </div>
  <br>

  <script type="text/javascript" src="./viewtgpcr.js"></script>
  <script type="text/javascript" src="./viewreferences.js"></script>

<?php
  unset($cdaViewObj);
  unset($cdaContrObj);
?>

</main>

<?php
  require "footer.php";
?>
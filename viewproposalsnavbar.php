<!--
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
-->
<nav id="nav">
<?php 
  require_once 'includes/autoloader-inc.php';
?>
    <ul>Proposals
      <div class="dropdown_content">
	      <li><a class="nav_a" href="viewproposals.php?selectedt=<?php echo $_REQUEST['selectedt']; ?>&v1by=true" target="_blank">View proposals voted +1 by you</a></li>
        <li><a class="nav_a" href="viewproposals.php?selectedt=<?php echo $_REQUEST['selectedt']; ?>&viewp_excls_v1by=true" title="View proposals excluding the 2nd proposals from similarities that you have voted +1" target="_blank">View proposals excluding similar</a></li>
	    </div>
	  </ul>
  <ul>Personal-messages
    <div class="dropdown_content">
      <li><a class="nav_a" href="viewsentmessages.php" target="_blank">View sent messages</a></li>
      <li><a class="nav_a" href="viewreceivedmessages.php" target="_blank">View received messages</a></li>
      <li><a class="nav_a" href="createnewmessage.php" target="_blank">Write a new message</a></li>
    </div>
  </ul>
  <ul>Options
    <div class="dropdown_content">
      <li><a class="nav_a" href="toolbar.php" target="_blank">Toolbar</a></li>
      <li><a class="nav_a" href="changepassword.php" target="_blank">Change password</a></li>
      <li><a class="nav_a" href="aboutmainpage.php" target="_blank">About</a></li>
    </div>
  </ul>
</nav>
<br>
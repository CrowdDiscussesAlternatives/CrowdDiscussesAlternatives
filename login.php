<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
  session_start();
  require "header.php";
?>

  <main>
  	<div>
  		<section>
      <br>

        <?php
           if (isset($_SESSION['userId'])) {
        ?>

             <p>You are already logged-in!</p>
		  	
        <?php
  	       } else { 
  	    ?>

  			    <label form="form-login">Login</label>

            <form action="includes/login-inc.php" method="post" id="form-login">
		  				<input type="text" name="uid" placeholder="Username..." required="required">
		  				<input type="Password" name="pwd" placeholder="Password..." required="required">
		  				<button type="submit" name="login-submit">Login</button>
		  			</form>
		        
		        <p id="p-login-urlvar"></p>

  	    <?php
  	       } 
  	    ?>	

  			<script type="module" src="./login.js"></script>

  		</section>
  	</div>
  </main>

<?php
  require "footer.php";
?>
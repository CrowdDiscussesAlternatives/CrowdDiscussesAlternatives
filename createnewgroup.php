<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 require "header.php"; 
?>

<main>

<?php
    $cdaContrObj = new CdaContr();
    $cdaContrObj->checkIfLoggedIn(null);

    $cdaViewObj = new CdaView();
    $tgpcrObj = new Tgpcr();

     if (!(isset($_REQUEST['selectedProposals']))) {
      exit("<br><br><b>-- Error: selected proposals array is not found!<b>");
    }

    $selectedProposals = explode(",", $_REQUEST['selectedProposals']);
    $selectedProposalsAsString = $_REQUEST['selectedProposals'];

    if ($selectedProposals[0] == "") {
    	exit("<br><br><b>-- Error: No proposals are selected!<b>");
    }

    $firstSelectedProposal = $cdaViewObj->showSelectedTgpcr("p", (int)$selectedProposals[0]);
    $topicId = $firstSelectedProposal["topic_id"];
    $checkIfMember = $cdaContrObj->checkIfMemberInTopic((int)$topicId, (int)$_SESSION["userId"]);
  if ($checkIfMember === 0) {
    unset($cdaContrObj);
    unset($cdaViewObj);
    unset($tgpcrObj);
    exit("<br><br><b>-- Error: Topic id not found!<b>");
  } elseif ($checkIfMember === null) {
  	 unset($cdaContrObj);
     unset($cdaViewObj);
     unset($tgpcrObj);
     exit("<br><br><b>-- Note: You are not a member of this topic! " . 
     	"You can contact with the initiator of this topic via a personal message.<b>");
  }

  $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$topicId);

  if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
    exit("<br><br>-- Note: The time table of the topic has not been created yet. You can contact with the initiator of the topic via a personal message.");
  } elseif ($topicInfo["prop_state"] > 0) {  //current date is before proposals phase closing date.
    exit("<br><br>-- Note: \"groups\" phase (fourth phase) has not been started yet." . 
      " You can view the time table for more info.");
  } elseif ($topicInfo["groups_state"] < 0) {  //groups_state = (groups phase closing date - current date) in seconds.
    exit("<br><br>-- Note: The \"groups\" phase (fourth phase) has been closed." . 
      " You can view the time table for more info.");
  }

    $selectedCateg = 'n';
    require_once "selectednavbar.php";
 ?>

    <label>Group selected proposals</label>

    <p id="p_create_urlvar"></p>

    <div class="tgpcrAll" id="div_selected_proposals">
    <?php
      require_once "headeroftable.php";
	    foreach ($selectedProposals as $proposalsKey => $proposalsValue) {
	    	$category = 'p';
        $tgpcr = $cdaViewObj->showSelectedTgpcr('p', (int)$proposalsValue);  //proposalsValue: proposalId.
        require "viewtgpcr.php";
    	  if ($tgpcr['edit_state'] >= 1) {
    	  	?> </div> <?php
    	  	require "closehtmltag.php";
    	  	exit("<br><br>-- Note: You cannot group a proposal that it is in editing phase."); /*(see proposal <a href=\"selected.php?selected=p" . $proposalsValue['id'] . "&parent=t" . $proposalsValue['topic_id'] . "_n0_n0" . "\">p" . $proposalsValue['id'] . "</a>).");*/
    	  }
	    }
    ?>
    </div>

    <form action="includes/createnewgroup-inc.php" method="post" name="form_create" id="form_create">
      <textarea name="group_title" placeholder="Group title..." maxlength="200" cols="30" required></textarea>
      <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedProposals" name="selectedProposals" value="<?php echo $selectedProposalsAsString; ?>">
      </div>
      <button type="submit" name="create_submit">OK</button>
    </form>
    <br>

    <script type="module" src="./createnewgroup.js"></script>
    <script type="text/javascript" src="./viewtgpcr.js"></script>
<?php
    unset($cdaContrObj);
    unset($cdaViewObj);
    unset($tgpcrObj);
?>

</main>

<?php
  require "footer.php";
?>
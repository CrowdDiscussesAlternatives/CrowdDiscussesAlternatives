<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';

  $cdaViewObj = new CdaView();
  $cdaContrObj = new CdaContr();

  $cdaContrObj->checkIfLoggedIn(null);

  if (!isset($_REQUEST['msg_id'])) {
  	exit('<br><b>-- Error: ID is not found!</b>');
  }

  if (isset($_REQUEST['ascdesc'])) {
    $ascDesc = mb_strtoupper($_REQUEST['ascdesc']);
  } else {
    $ascDesc = $_SESSION['ascdesc'];
  }

  $recipients = $cdaViewObj->showAllRecipientsOfMessage((int)$_REQUEST['msg_id'], $ascDesc);

  foreach ($recipients as $recipientsKey => $recipientsValue) {
	  $category = 'u';
	  $tgpcr = $recipientsValue;
	  $topicId = 0; //TO DO: users and members should be seperated in viewtgpcr.php.
	  require "viewtgpcr.php";
  }

  unset($cdaViewObj);
  unset($cdaContrObj);
  ?>
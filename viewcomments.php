<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 $cdaViewObj = new CdaView();
 $cdaContrObj = new CdaContr();

 $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

 require "header.php";
?>

<main>

	<?php
    if (!isset($_REQUEST['selectedt'])) {
      exit("<br><br><b>-- Error: Topic ID is not found!<b>");
    }

    $selectedCateg = 'n';
     require_once "selectednavbar.php";
  ?>

  <div id="div_toolbardata" style="display: none;">
    <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
    <div id="div_selectedt"><?php echo $_REQUEST['selectedt'] ?></div>
  </div>

  <?php
    if (isset($_REQUEST['strs'], $_REQUEST['c_in_categ'], $_REQUEST['selectedmember'])) {
      if ($_REQUEST['selectedmember'] == $_SESSION['userUid']) {
        $msgM = 'you';
      } elseif ($_REQUEST['selectedmember'] == 'any team-member') {
        $msgM = 'any team-member';
      } else {
        $msgM = 'user ' . $_REQUEST['selectedmember'];
      }

      if ($_REQUEST['c_in_categ'] == 'proposals' || $_REQUEST['c_in_categ'] == 'p') {
        $msgPOrG = 'proposals';
      } elseif ($_REQUEST['c_in_categ'] == 'groups' || $_REQUEST['c_in_categ'] == 'g') {
        $msgPOrG = 'groups';
      } else {
        exit("<br><br><b>-- Error: Wrong Category!</b>");
      }

      $displayedTip = "Below are displayed comments (of " . $msgPOrG . ") that contain the specified word/phrase: " . $_REQUEST['strs'] . "."
       . " The search was performed in comments created by " . $msgM . '.';
      ?>
        <p class="class_tip"><b><?php echo $displayedTip; ?></b></p>
      <?php
    }
    $limitOfRows = '1000';
  ?>

    <p class="class_tip">To select a comment you can click on its id. Maximum number of displayed comments: <?php echo $limitOfRows; ?></p>
    <?php
    if (!isset($_REQUEST['strs'], $_REQUEST['c_in_categ'], $_REQUEST['selectedmember'])) {
    ?>
    <form action="viewcomments.php" method="post" name="form_viewCommentsContainSelectedStrs" id="form_viewCommentsContainSelectedStrs" class="form_inline" target="_blank">
      <button type="submit" name="button_submitform">View</button>
      <p class="paragraphafterbutton"> comments of </p> 
      <select id="c_in_categ" name="c_in_categ" class="select_in_form"> 
        <option value="g">groups</option>
        <option value="p" selected="selected">proposals</option>
      </select>
      <p class="paragraphafterbutton">created by </p>
      <input list="datalist_selectedmembers" name="selectedmember" id="selectedmember" placeholder="(enter a username)" title="Enter a username or the phrase any team-member">
        <datalist id="datalist_selectedmembers">
          <option value="<?php echo $_SESSION['userUid']; ?>">
          <option value="any team-member">
        </datalist> 
      <p class="paragraphafterbutton">and that contain: </p> 
      <input type="text" name="strs" id="strs" class="input_strs_entries" placeholder="Enter a word or phrase that a comment must contain (enclosed in %% e.g. %word%)..." title="Enter a word or phrase that a comment must contain (enclosed in %% e.g. %word%)"></input>
      <div id="selecteddata" style="display: none;">
        <input type="text" id="selectedt" name="selectedt" value="<?php echo  $_REQUEST['selectedt']; ?>">
      </div>
    </form>
    <!-- TO DO: TO BE ALSO IN A FORM -->
      <p class="paragraphafterbutton"></p>
      <br><br>
      <button type="button" onclick="viewProposalsTheirCommentsContainSelectedStrs()">View</button>
      <p class="paragraphafterbutton"> proposals that some of their comments created by </p> 
      <select id="select_selectedmembersforviewp" name="select_selectedmembersforviewp" class="select_selectedmembers"> 
        <option value="currentmember">you</option>
        <option value="allmembers" selected="selected">any team-member</option>
      </select>
      <p class="paragraphafterbutton">contain: </p> 
      <input type="text" name="input_strsforviewp" id="input_strsforviewp" class="input_strs_entries" placeholder="Enter a word or phrase that a comment must contain (enclosed in %% e.g. %word%)..." title="Enter a word or phrase that a comment must contain (enclosed in %% e.g. %word%)"></input>
      <p class="paragraphafterbutton"></p>
      <br><br>
      <button type="button" onclick="viewGroupsTheirCommentsContainSelectedStrs()">View</button>
      <p class="paragraphafterbutton"> groups that some of their comments created by </p> 
      <select id="select_selectedmembersforviewg" name="select_selectedmembersforviewg" class="select_selectedmembers"> 
        <option value="currentmember">you</option>
        <option value="allmembers" selected="selected">any team-member</option>
      </select>
      <p class="paragraphafterbutton">contain: </p> 
      <input type="text" name="input_strsforviewg" id="input_strsforviewg" class="input_strs_entries" placeholder="Enter a word or phrase that a comment must contain (enclosed in %% e.g. %word%)..." title="Enter a word or phrase that a comment must contain (enclosed in %% e.g. %word%)"></input>
      <p class="paragraphafterbutton"></p>
      <p class="class_tip">If you want to search for a comment that contains a word/phrase e.g. "pros and cons" enclose the phrase inside "%%" e.g. "%pros and cons%". If you want the comment to start with this phrase, you can enter "pros and cons%". Note that if the word/phrase contains characters that are not letter, number, backslash nor space, you should type "\" before each of them (e.g. "100\%" instead of "100%").<!-- For more details you can read <a href="">help</a> page.--></p>
    <?php
    }
    ?>

  	<div class="tgpcrAll">
	<?php
    if (isset($_REQUEST['strs'], $_REQUEST['c_in_categ'], $_REQUEST['selectedmember'])) {
      require "headeroftable.php";
    
      require "viewcomments_in2.php";
    }
  ?>
  </div>
  <br>

  <script type="text/javascript" src="./viewtgpcr.js"></script>
  <script type="text/javascript" src="./viewcomments.js"></script>

	<?php
    unset($cdaViewObj);    //Declared in viewproposals_in2.php
    unset($cdaContrObj);
	?>

</main>

<?php
  require "footer.php";
?>
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, inputVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
inputVar = UrlVars.urlVar('input');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  document.getElementById('div_time_table_info').style.display = 'none'; 
  if (errorVar == 'nottopic') {
    infoMessage.innerHTML = "Error: Category sent to server is not a topic.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'missingentries') {
    infoMessage.innerHTML = "Please fill in all the fields of the time-table.";
  } 
  else if (errorVar == 'invitationphaseoutoflimit') {
    infoMessage.innerHTML = "Some of the values that you entered in the fields of time-table were out of limit." + 
    " You can read the limits if you move the cursor of the mouse close to each field.";
  }
  else if (errorVar == 'requirementsphaseoutoflimit') {
    infoMessage.innerHTML = "Some of the values that you entered in the fields of time-table were out of limit." + 
    " You can read the limits if you move the cursor of the mouse close to each field.";
  }
  else if (errorVar == 'proposalsphaseoutoflimit') {
    infoMessage.innerHTML = "Some of the values that you entered in the fields of time-table were out of limit." + 
    " You can read the limits if you move the cursor of the mouse close to each field.";
  }
  else if (errorVar == 'groupsphaseoutoflimit') {
    infoMessage.innerHTML = "Some of the values that you entered in the fields of time-table were out of limit." + 
    " You can read the limits if you move the cursor of the mouse close to each field.";
  }
  else if (errorVar == 'initnotmatch') {
    infoMessage.innerHTML =  "Please notice that you are not the initiator of this topic in order to create its time-table.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'update') {
    infoMessage.innerHTML =  "Please notice that you can create the time-table of the topic only once." + 
    " However, you can prolong (postpone the end of) its current phase.";
    document.getElementById('form_create').style.display = 'none';
  }
}
else if (inputVar != "") {
  if (inputVar == 'success') {
    infoMessage.innerHTML =  "You have successfully entered durations for each phase of the topic.";
    document.getElementById('form_create').style.display = 'none';
    document.getElementById('div_time_table_info').style.display = 'none'; 
  }
}
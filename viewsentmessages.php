<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

  <?php
    $cdaViewObj = new CdaView();
    $cdaContrObj = new CdaContr();

    $cdaContrObj->checkIfLoggedIn(null);
  
    $selectedCateg = 'n';
    require_once "selectednavbar.php";
  ?>

    <div id="div_toolbardata" style="display: none;">
      <!-- <div id="div_numofcomments"><?php echo $_SESSION["numofcomments"] ?></div>
      <div id="div_cbshowcomments"><?php echo $_SESSION["cbshowcomments"] ?></div>
      <div id="div_orderby"><?php echo $_SESSION["orderby"] ?></div> -->
      <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
      <!-- <div id="div_selectedt"><?php echo $_REQUEST['selectedt'] ?></div> -->
    </div>

    <p class="class_tip">You can click on a message to see its recipients.</p>

    <p class="class_tip">Sent messages:</p>

    <div class="tgpcrAll">
    <?php
      require "headeroftableofmessages.php";
    
      require "viewsentmessages_in2.php";

      $categToShow = 'm';
      $tgpcrObj = new Tgpcr();
      $categMsgToShow = $tgpcrObj->categMsg($categToShow);
      $tgpcrInTotalToShow = $totalnumOfM;
      unset($tgpcrObj);
      require 'btnshowmoretgpcr.php';

    ?>
    </div>
    <br>

    <script type="text/javascript" src="./viewtgpcr.js"></script>
    <script type="text/javascript" src="./viewsentmessages.js"></script>

  <?php
    unset($cdaViewObj);    //Declared in viewrequirements_in2.php
    unset($cdaContrObj);
  ?>

</main>

<?php
  require "footer.php";
?>
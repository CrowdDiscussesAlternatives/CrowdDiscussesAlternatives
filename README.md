# CrowdDiscussesAlternatives

Crowd Discusses Alternatives is an open-source web application that helps people discuss with each other, construct alternative solutions and distinguish the most popular.

Contact info: crowd_discusses_alternatives(at)protonmail.com (replace (at) with @).

License: GNU AFFERO GENERAL PUBLIC LICENSE Version 3 or (at your option) any later version (read file LICENSE).
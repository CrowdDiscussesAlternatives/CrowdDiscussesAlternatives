<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 $cdaViewObj = new CdaView();
 $cdaContrObj = new CdaContr();

 $cdaContrObj->checkIfLoggedIn("Location: login.php?error=notloggedin");

 require "header.php";
?>

<main>

<?php
  if (!isset($_REQUEST['selected'])) {
    exit("<br><br><b>-- Error: ID is not found!<b>");
  }

   $selectedCateg = 'n';
   require_once "selectednavbar.php";

   $idOfSelected = intval(substr($_REQUEST['selected'], 1));
   $categOfSelected = substr($_REQUEST['selected'], 0, 1);

   $tgpcrObj = new Tgpcr();
   $categMsgToShow = $tgpcrObj->categMsg($categOfSelected);
?>

  <div id="div_toolbardata" style="display: none;">
    <div id="div_numofcomments"><?php echo $_SESSION["numofcomments"] ?></div>
    <div id="div_orderby"><?php echo $_SESSION["orderby"] ?></div>
    <div id="div_ascdesc"><?php echo $_SESSION["ascdesc"] ?></div>
    <div id="div_selected"><?php echo $_REQUEST['selected'] ?></div>
  </div>

	<div class="tgpcrAll">
	
  <p>Comments of <?php echo $categMsgToShow; ?>:</p>
  <?php
    require "headeroftable.php";
    $category = $categOfSelected;
    $tgpcr = $cdaViewObj->showSelectedTgpcr($categOfSelected, (int)$idOfSelected);
    require "viewtgpcr.php";
  ?>
    <p class="class_tip">To select a comment you can click on its id.</p>
  <?php
  
    require "viewcommentsofselected_in2.php";

    $categToShow = 'c';
    //$tgpcrObj = new Tgpcr();  //declared also in viewcommentsofselected_in2.php
    $categMsgToShow = $tgpcrObj->categMsg($categToShow);
    $tgpcrInTotalToShow = $totalnumOfC;    //declared in viewcommentsofselected_in2.php
    unset($tgpcrObj);
    require 'btnshowmoretgpcr.php';
  ?>
  </div>
  <br>

  <script type="text/javascript" src="./viewtgpcr.js"></script>
  <script type="text/javascript" src="./viewcommentsofselected.js"></script>

<?php
  unset($cdaViewObj);
  unset($cdaContrObj);
?>

</main>

<?php
  require "footer.php";
?>
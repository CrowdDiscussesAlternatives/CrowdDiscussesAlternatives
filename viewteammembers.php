<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2024 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 $sessionStarted = true;
 require_once 'includes/autoloader-inc.php';
 //ini_set('max_execution_time', 300);    // in order to avoid Fatal error: Maximum execution time of 30 seconds exceeded .
 require "header.php";
?>

<main>

<?php
  $cdaViewObj = new CdaView();
  $cdaContrObj = new CdaContr();

  $cdaContrObj->checkIfLoggedIn(null);

  if (!isset($_REQUEST['selectedt'])) {
    exit("<br><br><b>-- Error: Topic ID is not found!<b>");
  }

  $selectedCateg = 'n';
  require_once "selectednavbar.php";

  $topicId = intval(substr($_REQUEST['selectedt'], 1));
  $totalnumOfTM = $cdaViewObj->totalNumberOfTeamMembers((int)$topicId);
?>

  <div id="div_toolbardata" style="display: none;">
    <div id="div_selectedt"><?php echo $_REQUEST['selectedt'] ?></div>
  </div>

	<div class="tgpcrAll">
    <p>There are <?php echo $totalnumOfTM; ?> team-members in topic:</p>
    <?php
    $category = 't';
    $tgpcr = $cdaViewObj->showSelectedTgpcr('t', (int)$topicId);
    require "viewtgpcr.php";
    ?>
      <p id="tip_select">You can select multiple team-members by clicing on their usernames and then pressing:</p>
      <button type="button" onclick="viewMembersProposals()">Select</button>
      <p class="paragraphafterbutton"> team-members for viewing their proposals.</p>
    <?php

    if (isset($_REQUEST['rank_m_by_u_vs'])) {
      ?>
      <p id="tip_select">Team-members are ranked by the sum of your votes on the references of each one. The order is from the largest sum to the smallest. Team-members that you have not voted for at least one of their references are not displayed.</p>
      <?php
    }

    require "viewteammembers_in2.php";
  ?>
  </div>
  <br>

  <script type="text/javascript" src="./viewtgpcr.js"></script>
  <script type="text/javascript" src="./viewteammembers.js"></script>

<?php
  unset($cdaViewObj);
  unset($cdaContrObj);
?>

</main>

<?php
  require "footer.php";
?>
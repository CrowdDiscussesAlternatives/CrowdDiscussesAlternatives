/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var offsetOfC; // they are on global scope and accessed easily. it is better to use closure.
offsetOfC = 0;

function btnShow()
{
    let numOfC, orderBy, ascDesc, selected;    //C: Comment.
    numOfC =  Number(document.getElementById('div_numofcomments').innerHTML);
    orderBy = document.getElementById('div_orderby').innerHTML;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    selected = document.getElementById('div_selected').innerHTML;
    offsetOfC += numOfC;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreC, newDivC, buttonShowMoreC;
            newDivC = document.createElement("div");
            newDivC.setAttribute('id', ('div_comments_' + offsetOfC.toString()));
            divShowMoreC = document.getElementById('showc');
            buttonShowMoreC = document.getElementById('btn_showc');
            divShowMoreC.insertBefore(newDivC, buttonShowMoreC);
            newDivC.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4, hrefString5, hrefString6;
    hrefString1 = "offset=" + offsetOfC.toString();
    hrefString2 = "&numofcomments=" + numOfC.toString();
    hrefString5 = "&orderby=" + orderBy;
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString7 = "&selected=" + selected;
    hrefString = hrefString1 + hrefString2 + hrefString5 + hrefString6 + hrefString7;
    xhttp.open("post", "viewcommentsofselected_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}
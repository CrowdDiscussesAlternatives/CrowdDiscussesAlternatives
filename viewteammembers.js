/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var selectedTeamMembers;
selectedTeamMembers = [];

function selectTeamMembers(event)
{
    let pagePathname;
    pagePathname = window.location.pathname;
    if (!pagePathname.includes("viewteammembers.php")) {
      return;
    }

    let memberIdSelected, eventCurrentTarget;
    eventCurrentTarget = event.currentTarget;
    memberIdSelected = eventCurrentTarget.parentNode.firstElementChild.innerHTML.slice(1);
    if (!selectedTeamMembers.includes(memberIdSelected)) {
      selectedTeamMembers.push(memberIdSelected);
      eventCurrentTarget.style.color = '#DC143C';
    }
}

function viewMembersProposals()
{  
    let topicId; 
    if (typeof selectedTeamMembers === "undefined") {
        return;
    }

    topicId = document.getElementById('div_selectedt').innerHTML.slice(1);
    
    window.open("viewproposals.php?" + "selectedt=t" + topicId.toString() + "&selectedm=" + selectedTeamMembers.toString());
}
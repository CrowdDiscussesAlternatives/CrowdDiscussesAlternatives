/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
import { GetUrlVars } from './modules/geturlvars.js';

let UrlVars, errorVar, editVar, infoMessage;
UrlVars = new GetUrlVars(window.location.href);
errorVar = UrlVars.urlVar('error');
editVar = UrlVars.urlVar('edit');
infoMessage = document.getElementById('p_create_urlvar');
infoMessage.innerHTML = " ";

if (errorVar != "") {
  if (errorVar == 'pexists') {
    infoMessage.innerHTML = "This proposal already exists!";
    document.getElementById('form_create').style.display = 'none';
  } 
  else if (errorVar == 'nop') {
    infoMessage.innerHTML = "You cannot create an empty proposal!";
  }
  else if (errorVar == 'notauth') {
    infoMessage.innerHTML = "You cannot edit this proposal because you are not the author.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'votephase') {
    infoMessage.innerHTML = "This proposal is not in editing phase.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'phasemin') {
    infoMessage.innerHTML = "The duration of the editing phase of the proposal must be at least 15 days after its creation.";
    document.getElementById('form_create').style.display = 'none';
  }
  else if (errorVar == 'phasemax') {
    infoMessage.innerHTML = "The editing phase of the proposal must end 15 days before the \"groups\" phase (fourth phase) ends.";
    document.getElementById('form_create').style.display = 'none';
  }
} 
else if (editVar != "") {
  if (editVar == 'success') {
    infoMessage.innerHTML =  "You have successfully updated the proposal.";
    document.getElementById('form_create').style.display = 'none';
  }
}
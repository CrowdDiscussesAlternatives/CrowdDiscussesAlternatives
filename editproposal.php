<?php
/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
 session_start();    //session_start(); on the top of the code.
 require_once 'includes/autoloader-inc.php';
 require "header.php";
?>

<main>

	<?php
    $cdaContrObj = new CdaContr();
    $cdaContrObj->checkIfLoggedIn(null);

    $cdaViewObj = new CdaView();
    $tgpcrObj = new Tgpcr();

    if (!(isset($_REQUEST['selected']))) {
      exit("<br><br><b>-- Error: ID of selected proposal is not found!<b>");
    }

    $selectedCateg = 'n';
    require_once "selectednavbar.php";

    $selectedCateg = substr($_REQUEST['selected'], 0, 1);
    $selectedTgpcrID = intval(substr($_REQUEST['selected'], 1));
    $tgpcr = $cdaViewObj->showSelectedTgpcr($selectedCateg, (int)$selectedTgpcrID);
    $selectedCategMsg = $tgpcrObj->categMsg($selectedCateg);

    if ($selectedCategMsg == 'Non-existent category!') {
      exit('-- Error: Category sent to server does not exist!');
    } elseif ($selectedCategMsg != 'proposal') {
      exit('-- Error: Category of proposal (for editing) was expected to be sent to server!');
    }

    if (!(isset($_REQUEST['edit']) && $_REQUEST['edit'] == 'success')) {
      if ($tgpcr['edit_state'] < 1) {
       exit('-- Error: Proposal is not in editing phase!');
      }

      $topicInfo = $cdaViewObj->showSelectedTopicInfo((int)$tgpcr['topic_id']); //id of topic.

      if ($topicInfo === null || $topicInfo["timetable_changed"] == -1) {
        exit("<br><br>-- Note: The time table of the topic has not been created yet. You can contact with the initiator of the topic via a personal message.");
      } 

      $maxEditingPhaseDuration = ($topicInfo["groups_state"] - 15*24*3600)/(24*3600);
      $currentEditingPhaseDuration = round($tgpcr['seconds_from_edit_closing_date']/(24*3600), 1);
      $minEditingPhaseDuration = 15 - $tgpcr['seconds_from_date_time']/(24*3600);

      if ($maxEditingPhaseDuration < 0) {
        exit('-- Error: Note that the editing phase of the proposal must end at least 15 days before fourth phase ends!');
      }elseif ($minEditingPhaseDuration < 0) {
        $minEditingPhaseDuration = 0;
      }
    }
?>
    <p>Selected <?php echo $selectedCategMsg; ?>:</p>

    <div class="tgpcrAll">
<?php
    require_once "headeroftable.php";

    $category = $selectedCateg;
    require "viewtgpcr.php";

    if (!(isset($_REQUEST['edit']) && $_REQUEST['edit'] == 'success')) {
?>    
      <br>
      <label form="form_create">Edit selected proposal</label>

      <form action="includes/editproposal-inc.php" method="post" name="form_create" id="form_create">
        <textarea name="textarea_name" id="textarea_name" placeholder="Improved proposal..." maxlength="700" cols="30" required></textarea> <!-- wrap="hard" -->
        <div class="div_inputlabel_timetable">
        <label class="label_for" for="editingphaseduration">Specify duration of <b>editing phase of proposal</b> (in days):</label>
        <input class="input_time_table" type="number" step="0.000000000000001" id="editingphaseduration" name="editingphaseduration" title="Minimum 15 days after creation of proposal, maximum 15 days before fourth phase ends." placeholder="Current: ~<?php echo $currentEditingPhaseDuration; ?> days" min="<?php echo  $minEditingPhaseDuration; ?>" max="<?php echo  $maxEditingPhaseDuration; ?>">
        <p>Note that if you leave this field empty, the current duration will remain the same.</p>
        </div>
        <div id="selecteddata" style="display: none;">
          <input type="text" id="selectedpid" name="selectedpid" value="<?php echo $selectedTgpcrID; ?>">
        </div>
        <button type="submit" name="create_submit">OK</button>
      </form>
      <br>
    <?php
    }
    ?>
    <p>of selected topic:</p>

<?php
    $tgpcr = $cdaViewObj->showSelectedTgpcr('t', (int)$tgpcr['topic_id']);
    $category = 't';
    require "viewtgpcr.php";
?>
    </div>
    <p id="p_create_urlvar"></p>

    <script type="module" src="./editproposal.js"></script>
    <script type="text/javascript" src="./viewtgpcr.js"></script>
<?php
    unset($cdaViewObj);
    unset($tgpcrObj);
?>

</main>

<?php
  require "footer.php";
?>
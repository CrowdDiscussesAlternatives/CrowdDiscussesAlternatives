/*
    Crowd Discusses Alternatives is a web application for more organized discussions that help people create alternative solutions, evaluate and rank them.

    Copyright 2021-2022 Stavros Kalognomos

    This file is part of Crowd Discusses Alternatives.

    Crowd Discusses Alternatives is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    Crowd Discusses Alternatives is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License along with Crowd Discusses Alternatives. If not, see <https://www.gnu.org/licenses/>. 
*/
var offsetOfQ; // they are on global scope and accessed easily. it is better to use closure.
offsetOfQ = 0;

function btnShow()
{ 
    let numOfQ, numOfComments, cbShowComments, orderBy, ascDesc, selectedT;    //Q: Requirement.
    numOfQ = 10;
    numOfComments = Number(document.getElementById('div_numofcomments').innerHTML);
    cbShowComments = document.getElementById('div_cbshowcomments').innerHTML;
    orderBy = document.getElementById('div_orderby').innerHTML;
    ascDesc = document.getElementById('div_ascdesc').innerHTML;
    selectedT = document.getElementById('div_selectedt').innerHTML;    //selected topicId.
    offsetOfQ += numOfQ;
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let divShowMoreQ, newDivQ, buttonShowMoreQ;
            newDivQ = document.createElement("div");
            newDivQ.setAttribute('id', ('div_requirements_' + offsetOfQ.toString()));
            divShowMoreQ = document.getElementById('showq');
            buttonShowMoreQ = document.getElementById('btn_showq');
            divShowMoreQ.insertBefore(newDivQ, buttonShowMoreQ);
            newDivQ.innerHTML = this.responseText;
        }
    };
    let hrefString, hrefString1,  hrefString2, hrefString3, hrefString4, hrefString5, hrefString6;
    hrefString1 = "offset=" + offsetOfQ.toString();
    hrefString3 = "&numofcomments=" + numOfComments.toString();
    hrefString4 = "&cbshowcomments=" + cbShowComments;
    hrefString5 = "&orderby=" + orderBy;
    hrefString6 = "&ascdesc=" + ascDesc;
    hrefString7 = "&selectedt=" + selectedT;
    if (window.location.search.includes("&var=")) {
      hrefString8 = "&var=true";
    } else {
      hrefString8 = "";
    }
    hrefString = hrefString1 + hrefString3 + hrefString4 + hrefString5 + hrefString6 + hrefString7 + hrefString8;
    xhttp.open("post", "viewrequirements_in2.php", true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader("Cache-Control", "no-cache");
    xhttp.send(hrefString);
}